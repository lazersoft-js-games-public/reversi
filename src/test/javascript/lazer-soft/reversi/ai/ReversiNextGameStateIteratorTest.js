describe("ReversiNextGameStateIterator", function() {
    it("iterates through all next available game states", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);

    	var iterator = new LS_REVERSI.ReversiNextGameStateIterator(gameState, moveHelper, constants);
    	
    	var nextGameStates = new Array();
    	expect(iterator.hasNext()).toBeTruthy();
    	nextGameStates[0] = iterator.next();
    	expect(iterator.hasNext()).toBeTruthy();
    	nextGameStates[1] = iterator.next();
    	expect(iterator.hasNext()).toBeTruthy();
    	nextGameStates[2] = iterator.next();
    	expect(iterator.hasNext()).toBeTruthy();
    	nextGameStates[3] = iterator.next();
    	expect(iterator.hasNext()).toBeFalsy();
    	//TODO assert nextGameStates is ok
    });
});