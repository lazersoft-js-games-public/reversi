describe("GameStateHistory", function() {
    it("goes back one, going to previous game state and advances from there", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	
    	var gameState0 = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	var gameState1 = new LS_REVERSI.ReversiGameState(2, constants, moveHelper);
    	var gameState2 = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	var gameState3 = new LS_REVERSI.ReversiGameState(2, constants, moveHelper);
    	var gameState4 = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	var gameState5 = new LS_REVERSI.ReversiGameState(2, constants, moveHelper);
    	
    	var history = new LS_REVERSI.GameStateHistory();
    	expect(history.getSize()).toBe(0);
    	expect(history.getCurrentLocation()).toBe(-1);
    	
    	history.advanceFromCurrent(gameState0);
    	expect(history.getSize()).toBe(1);
    	expect(history.getCurrentLocation()).toBe(0);
    	
    	history.advanceFromCurrent(gameState1);
    	expect(history.getSize()).toBe(2);
    	expect(history.getCurrentLocation()).toBe(1);
    	
    	history.advanceFromCurrent(gameState2);
    	expect(history.getSize()).toBe(3);
    	expect(history.getCurrentLocation()).toBe(2);
    	
    	history.advanceFromCurrent(gameState3);
    	expect(history.getSize()).toBe(4);
    	expect(history.getCurrentLocation()).toBe(3);
    	
    	expect(history.getCurrent()).toEqual(gameState3);
    	
    	history.goBackOne();
    	expect(history.getSize()).toBe(4);
    	expect(history.getCurrentLocation()).toBe(2);
    	
    	expect(history.getCurrent()).toEqual(gameState2);
    	
    	history.advanceFromCurrent(gameState4);
    	expect(history.getSize()).toBe(4);
    	expect(history.getCurrentLocation()).toBe(3);
    	
    	expect(history.getCurrent()).toEqual(gameState4);
    });
    
    it("when something is changed in earlier history, the rest of history from that point on is erased", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	
    	var gameState0 = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	var gameState1 = new LS_REVERSI.ReversiGameState(2, constants, moveHelper);
    	var gameState2 = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	var gameState3 = new LS_REVERSI.ReversiGameState(2, constants, moveHelper);
    	var gameState4 = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	var gameState5 = new LS_REVERSI.ReversiGameState(2, constants, moveHelper);
    	
    	var history = new LS_REVERSI.GameStateHistory();
    	expect(history.getSize()).toBe(0);
    	expect(history.getCurrentLocation()).toBe(-1);
    	
    	history.advanceFromCurrent(gameState0);
    	//[0]
    	expect(history.getSize()).toBe(1);
    	expect(history.getCurrentLocation()).toBe(0);
    	
    	history.advanceFromCurrent(gameState1);
    	//0[1]
    	expect(history.getSize()).toBe(2);
    	expect(history.getCurrentLocation()).toBe(1);
    	
    	history.advanceFromCurrent(gameState2);
    	//01[2]
    	expect(history.getSize()).toBe(3);
    	expect(history.getCurrentLocation()).toBe(2);
    	
    	history.advanceFromCurrent(gameState3);
    	//012[3]
    	expect(history.getSize()).toBe(4);
    	expect(history.getCurrentLocation()).toBe(3);
    	expect(history.getCurrent()).toEqual(gameState3);
    	
    	history.goBackOne();
    	//01[2]3
    	expect(history.getSize()).toBe(4);
    	expect(history.getCurrentLocation()).toBe(2);    	
    	expect(history.getCurrent()).toEqual(gameState2);
    	
    	history.goBackOne();
    	//0[1]23
    	expect(history.getSize()).toBe(4);
    	expect(history.getCurrentLocation()).toBe(1);    	
    	expect(history.getCurrent()).toEqual(gameState1);
    	
    	history.advanceFromCurrent(gameState4);
    	//01[4]
    	expect(history.getSize()).toBe(3);
    	expect(history.getCurrentLocation()).toBe(2);    	
    	expect(history.getCurrent()).toEqual(gameState4);
    	
    	var historyEntries = history.getEntries();
    	expect(historyEntries.length).toBe(3);
    	expect(historyEntries[0]).toEqual(gameState0);
    	expect(historyEntries[1]).toEqual(gameState1);
    	expect(historyEntries[2]).toEqual(gameState4);
    });
    
    it("goes back and forth through history, as long as nothing is altered in the past", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	
    	var gameState0 = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	var gameState1 = new LS_REVERSI.ReversiGameState(2, constants, moveHelper);
    	var gameState2 = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	var gameState3 = new LS_REVERSI.ReversiGameState(2, constants, moveHelper);
    	var gameState4 = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	var gameState5 = new LS_REVERSI.ReversiGameState(2, constants, moveHelper);
    	
    	var history = new LS_REVERSI.GameStateHistory();
    	expect(history.getSize()).toBe(0);
    	expect(history.getCurrentLocation()).toBe(-1);
    	
    	history.advanceFromCurrent(gameState0);
    	expect(history.getSize()).toBe(1);
    	expect(history.getCurrentLocation()).toBe(0);
    	
    	history.advanceFromCurrent(gameState1);
    	expect(history.getSize()).toBe(2);
    	expect(history.getCurrentLocation()).toBe(1);
    	
    	history.advanceFromCurrent(gameState2);
    	expect(history.getSize()).toBe(3);
    	expect(history.getCurrentLocation()).toBe(2);
    	
    	history.advanceFromCurrent(gameState3);
    	expect(history.getSize()).toBe(4);
    	expect(history.getCurrentLocation()).toBe(3);
    	
    	expect(history.getCurrent()).toEqual(gameState3);
    	
    	history.goBackOne();
    	expect(history.getSize()).toBe(4);
    	expect(history.getCurrentLocation()).toBe(2);
    	
    	expect(history.getCurrent()).toEqual(gameState2);
    	
    	history.goForwardOne();
    	expect(history.getSize()).toBe(4);
    	expect(history.getCurrentLocation()).toBe(3);
    	
    	expect(history.getCurrent()).toEqual(gameState3);
    	
    	history.advanceFromCurrent(gameState4);
    	expect(history.getSize()).toBe(5);
    	expect(history.getCurrentLocation()).toBe(4);
    	
    	expect(history.getCurrent()).toEqual(gameState4);
    });
});