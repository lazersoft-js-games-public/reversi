describe("NextMoveIterator", function() {
    it("contains all next moves from starting position, iterated by coordinates", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	var moveIterator = new LS_REVERSI.NextMoveIterator(gameState, moveHelper, constants, false);
    	expect(moveIterator.hasNext()).toBeTruthy();
    	var nextMoves = new Array();
    	
    	nextMoves[0] = moveIterator.next();
    	expect(moveIterator.hasNext()).toBeTruthy();
    	nextMoves[1] = moveIterator.next();
    	expect(moveIterator.hasNext()).toBeTruthy();
    	nextMoves[2] = moveIterator.next();
    	expect(moveIterator.hasNext()).toBeTruthy();
    	nextMoves[3] = moveIterator.next();
    	expect(moveIterator.hasNext()).toBeFalsy();
    });
    
    it("contains all next moves from starting position, iterated by interestingness", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	var moveIterator = new LS_REVERSI.NextMoveIterator(gameState, moveHelper, constants, true);
    	expect(moveIterator.hasNext()).toBeTruthy();
    	var nextMoves = new Array();
    	
    	nextMoves[0] = moveIterator.next();
    	expect(moveIterator.hasNext()).toBeTruthy();
    	nextMoves[1] = moveIterator.next();
    	expect(moveIterator.hasNext()).toBeTruthy();
    	nextMoves[2] = moveIterator.next();
    	expect(moveIterator.hasNext()).toBeTruthy();
    	nextMoves[3] = moveIterator.next();
    	expect(moveIterator.hasNext()).toBeFalsy();
    });
    
    it("returns corner move before midde-of-board-move when set to explore interesting moves first", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	gameState.forceAllTokens(constants.tokenTypeBlack);
    	gameState.forceTokenAt(constants.tokenTypeEmpty, 7, 7);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 6, 7);
    	gameState.forceTokenAt(constants.tokenTypeEmpty, 4, 2);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 5, 2);    	
    	
    	var moveIterator = new LS_REVERSI.NextMoveIterator(gameState, moveHelper, constants, true);
    	
    	expect(moveIterator.hasNext()).toBeTruthy();
    	
    	var move1 = moveIterator.next();
    	expect(move1.position.x).toBe(7);
    	expect(move1.position.y).toBe(7);
    	
    	var move2 = moveIterator.next();
    	expect(move2.position.x).toBe(4);
    	expect(move2.position.y).toBe(2);
    	
    	expect(moveIterator.hasNext()).toBeFalsy();
    });
    
    it("returns midde-of-board-move before corner move when set to explore by XY coords", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	gameState.forceAllTokens(constants.tokenTypeBlack);
    	gameState.forceTokenAt(constants.tokenTypeEmpty, 7, 7);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 6, 7);
    	gameState.forceTokenAt(constants.tokenTypeEmpty, 4, 2);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 5, 2);    	
    	
    	var moveIterator = new LS_REVERSI.NextMoveIterator(gameState, moveHelper, constants, false);
    	
    	expect(moveIterator.hasNext()).toBeTruthy();
    	
    	var move1 = moveIterator.next();
    	expect(move1.position.x).toBe(4);
    	expect(move1.position.y).toBe(2);
    	
    	var move2 = moveIterator.next();
    	expect(move2.position.x).toBe(7);
    	expect(move2.position.y).toBe(7);
    	
    	expect(moveIterator.hasNext()).toBeFalsy();
    });
});