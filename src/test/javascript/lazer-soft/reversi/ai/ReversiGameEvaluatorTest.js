describe("ReversiGameEvaluator", function() {
    it("scores start position to 0 for white (with default intermediate state evaluation function)", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var mockEvaluateIntermediateState = function(playerToWin, gameState, gameStateSummary) {
			return 0;
		};
    	var evaluator = new LS_REVERSI.ReversiGameEvaluator(2, 3, constants, moveHelper, mockEvaluateIntermediateState);
    	
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	
    	var score = evaluator.evaluate(gameState);
    	
    	expect(score).toBe(0);
    });
    
    it("scores start position to 0 for black (with default intermediate state evaluation function)", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var mockEvaluateIntermediateState = function(playerToWin, gameState, gameStateSummary) {
			return 0;
		};
    	var evaluator = new LS_REVERSI.ReversiGameEvaluator(1, 3, constants, moveHelper, mockEvaluateIntermediateState);
    	
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	
    	var score = evaluator.evaluate(gameState);
    	
    	expect(score).toBe(0);
    });
    
    it("calls custom intermediate state evaluation function", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var customIntermediateEvaluator = function(playerToWin, gameState, gameStateSummary) {
    		return 123;
    	};
    	var evaluator = new LS_REVERSI.ReversiGameEvaluator(1, 3, constants, moveHelper, customIntermediateEvaluator);
    	
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	
    	var score = evaluator.evaluate(gameState);
    	
    	expect(score).toBe(123);
    });
    
    it("generates gameStateSummary with correct corner count", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var actualGameStateSummary;
    	var customIntermediateEvaluator = function(playerToWin, gameState, gameStateSummary) {
    		actualGameStateSummary = gameStateSummary;
    		return 1;
    	};
    	var evaluator = new LS_REVERSI.ReversiGameEvaluator(1, 3, constants, moveHelper, customIntermediateEvaluator);
    	
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 0, 0);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 7, 0);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 0, 7);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 7, 7);
    	
    	evaluator.evaluate(gameState);
    	
    	expect(actualGameStateSummary.takenCornersBlack).toBe(3);
    	expect(actualGameStateSummary.takenCornersWhite).toBe(1);
    	expect(actualGameStateSummary.cornerExposingTokensWhite).toBe(0);
    	expect(actualGameStateSummary.cornerExposingTokensBlack).toBe(0);
    	expect(actualGameStateSummary.takenBordersWhite).toBe(0);
    	expect(actualGameStateSummary.takenBordersBlack).toBe(0);
    	expect(actualGameStateSummary.borderExposingTokensWhite).toBe(0);
    	expect(actualGameStateSummary.borderExposingTokensBlack).toBe(0);
    	expect(actualGameStateSummary.tokenCountBlack).toBe(5);
    	expect(actualGameStateSummary.tokenCountWhite).toBe(3);
    });
    
    it("generates gameStateSummary with correct corner exposing count", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var actualGameStateSummary;
    	var customIntermediateEvaluator = function(playerToWin, gameState, gameStateSummary) {
    		actualGameStateSummary = gameStateSummary;
    		return 1;
    	};
    	var evaluator = new LS_REVERSI.ReversiGameEvaluator(1, 3, constants, moveHelper,  customIntermediateEvaluator);
    	
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 1, 1);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 1, 0);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 6, 6);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 6, 7);
    	
    	gameState.forceTokenAt(constants.tokenTypeBlack, 0, 7);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 0, 6);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 1, 6);
    	
    	evaluator.evaluate(gameState);
    	
    	expect(actualGameStateSummary.takenCornersBlack).toBe(1);
    	expect(actualGameStateSummary.takenCornersWhite).toBe(0);
    	expect(actualGameStateSummary.cornerExposingTokensWhite).toBe(1);
    	expect(actualGameStateSummary.cornerExposingTokensBlack).toBe(3);
    	expect(actualGameStateSummary.takenBordersWhite).toBe(0);
    	expect(actualGameStateSummary.takenBordersBlack).toBe(0);
    	expect(actualGameStateSummary.borderExposingTokensWhite).toBe(0);
    	expect(actualGameStateSummary.borderExposingTokensBlack).toBe(0);
    	expect(actualGameStateSummary.tokenCountBlack).toBe(8);
    	expect(actualGameStateSummary.tokenCountWhite).toBe(3);
    });
    
    it("generates gameStateSummary with correct border (exposing) count for black", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var actualGameStateSummary;
    	var customIntermediateEvaluator = function(playerToWin, gameState, gameStateSummary) {
    		actualGameStateSummary = gameStateSummary;
    		return 1;
    	};
    	var evaluator = new LS_REVERSI.ReversiGameEvaluator(1, 3, constants, moveHelper, customIntermediateEvaluator);
    	
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 1, 1);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 1, 0);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 6, 6);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 6, 7);
    	
    	gameState.forceTokenAt(constants.tokenTypeBlack, 0, 7);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 0, 6);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 1, 6);
    	
    	gameState.forceTokenAt(constants.tokenTypeBlack, 0, 3);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 1, 3);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 1, 5);
    	
    	evaluator.evaluate(gameState);
    	
    	expect(actualGameStateSummary.takenCornersBlack).toBe(1);
    	expect(actualGameStateSummary.takenCornersWhite).toBe(0);
    	expect(actualGameStateSummary.cornerExposingTokensWhite).toBe(1);
    	expect(actualGameStateSummary.cornerExposingTokensBlack).toBe(3);
    	expect(actualGameStateSummary.takenBordersWhite).toBe(0);
    	expect(actualGameStateSummary.takenBordersBlack).toBe(1);
    	expect(actualGameStateSummary.borderExposingTokensWhite).toBe(0);
    	expect(actualGameStateSummary.borderExposingTokensBlack).toBe(1);
    	expect(actualGameStateSummary.tokenCountBlack).toBe(11);
    	expect(actualGameStateSummary.tokenCountWhite).toBe(3);
    });
    
    it("generates gameStateSummary with correct border (exposing) count for white", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var actualGameStateSummary;
    	var customIntermediateEvaluator = function(playerToWin, gameState, gameStateSummary) {
    		actualGameStateSummary = gameStateSummary;
    		return 1;
    	};
    	var evaluator = new LS_REVERSI.ReversiGameEvaluator(1, 3, constants, moveHelper, customIntermediateEvaluator);
    	
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	//corner (exposing):
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 1);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 0);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 6, 6);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 6, 7);    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 0, 7);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 0, 6);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 6);
    	
    	//border (exposing):
    	gameState.forceTokenAt(constants.tokenTypeWhite, 0, 3);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 3);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 5);
    	
    	evaluator.evaluate(gameState);
    	
    	expect(actualGameStateSummary.takenCornersWhite).toBe(1);
    	expect(actualGameStateSummary.takenCornersBlack).toBe(0);
    	expect(actualGameStateSummary.cornerExposingTokensWhite).toBe(4);
    	expect(actualGameStateSummary.cornerExposingTokensBlack).toBe(0);
    	expect(actualGameStateSummary.takenBordersWhite).toBe(1);
    	expect(actualGameStateSummary.takenBordersBlack).toBe(0);
    	expect(actualGameStateSummary.borderExposingTokensWhite).toBe(1);
    	expect(actualGameStateSummary.borderExposingTokensBlack).toBe(0);
    	expect(actualGameStateSummary.tokenCountWhite).toBe(12);
    	expect(actualGameStateSummary.tokenCountBlack).toBe(2);
    });
    
    it("evaluates end game win", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	//player to win set to 1 (black)
    	var evaluator = new LS_REVERSI.ReversiGameEvaluator(1, 3, constants, moveHelper);
    	    	
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	gameState.forceAllTokensBlack();
    	gameState.forceTokenAt(constants.tokenTypeWhite, 0, 0);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 1);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 2);
    	gameState.forceTokenAt(constants.tokenTypeEmpty, 1, 4);
    	gameState.forcePlayerToMove(2);
    	
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var lastMove = moveHelper.toMove(2, constants.positionsByCoordinates[1][4], gameState);
    	gameState.applyMove(2, lastMove);
    	
    	expect(gameState.isGameEnded()).toBeTruthy();
    	
    	var score = evaluator.evaluate(gameState);
    	//expect black score to be bigger than white at end of game (black wins aka player who should win, wins)
    	expect(evaluator.getTokenCount(constants.tokenTypeWhite, gameState)).toBe(5);
    	expect(evaluator.getTokenCount(constants.tokenTypeBlack, gameState)).toBe(59);
    	//expect max score as game has ended and player who should win has won
    	expect(score).toBe(Number.POSITIVE_INFINITY);
    });
    
    it("evaluates end game loss", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	//player to win set to 2 (white)
    	var evaluator = new LS_REVERSI.ReversiGameEvaluator(2, 3, constants, moveHelper);
    	
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	gameState.forceAllTokensBlack();
    	gameState.forceTokenAt(constants.tokenTypeWhite, 0, 0);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 1);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 2);
    	gameState.forceTokenAt(constants.tokenTypeEmpty, 1, 4);
    	gameState.forcePlayerToMove(2);
    	
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var lastMove = moveHelper.toMove(2, constants.positionsByCoordinates[1][4], gameState);
    	gameState.applyMove(2, lastMove);
    	
    	expect(gameState.isGameEnded()).toBeTruthy();
    	
    	var score = evaluator.evaluate(gameState);
    	//expect black score to be bigger than white at end of game (black wins aka player who should win, loses)
    	expect(evaluator.getTokenCount(constants.tokenTypeWhite, gameState)).toBe(5);
    	expect(evaluator.getTokenCount(constants.tokenTypeBlack, gameState)).toBe(59);
    	//expect min score as game has ended and player who should win has lost
    	expect(score).toBe(Number.NEGATIVE_INFINITY);
    });
    
    it("evaluates end game draw", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	//player to win set to 2 (white)
    	var evaluator = new LS_REVERSI.ReversiGameEvaluator(2, 3, constants, moveHelper);
    	
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	gameState.forceAllTokensBlack();
    	for(var x = 0; x < 8; x++) {
    		for(var y = 0; y < 4; y++) {
    			gameState.forceTokenAt(constants.tokenTypeWhite, x, y);
    		}
    	}
    	gameState.gameEnded = true;
    	expect(gameState.isGameEnded()).toBeTruthy();
    	
    	var score = evaluator.evaluate(gameState);
    	expect(evaluator.getTokenCount(constants.tokenTypeWhite, gameState)).toBe(32);
    	expect(evaluator.getTokenCount(constants.tokenTypeBlack, gameState)).toBe(32);
    	expect(score).toBe(0);
    });
    
    it("uses custom evaluation depth function", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var gameState1 = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	gameState2 = gameState1.clone();
    	gameState2.applyMoveFromPosition(1, constants.positionsByCoordinates[3][2]);
    	
    	var getEvaluationDepth = function(gameState) {
    		if(gameState.getTokenCount() == 4) {
    			return 13;
    		} else {
    			return 14;
    		}
    	}
    	
    	var evaluator = new LS_REVERSI.ReversiGameEvaluator(2, getEvaluationDepth, constants, moveHelper);
    	expect(evaluator.getEvaluationDepth(gameState1)).toBe(13);
    	expect(evaluator.getEvaluationDepth(gameState2)).toBe(14);
    });
    
    it("generates game state tree for alternating players", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	var mockEvaluateIntermediateState = function(playerToWin, gameState, gameStateSummary) { 
    		return 1;
    	}
    	var evaluator = new LS_REVERSI.ReversiGameEvaluator(2, 4, constants, moveHelper, mockEvaluateIntermediateState);
    	var tree = new LS_AI.Tree(evaluator, new LS_AI.CallStack());
    	
    	var node = tree.grow(gameState);
    	
    	expect(node.childNodes.length).toBe(4);
    	expect(node.childNodes[0].gameState.getPlayerToMove()).toBe(2);
    	expect(node.childNodes[1].gameState.getPlayerToMove()).toBe(2);
    	expect(node.childNodes[2].gameState.getPlayerToMove()).toBe(2);
    	expect(node.childNodes[3].gameState.getPlayerToMove()).toBe(2);
    	
    	var grandchildNodes = new Array();
    	while(node.childNodes.length != 0) {
    		var childNode = node.childNodes.pop();
    		while(childNode.childNodes.length != 0) {
        		var grandchildNode = childNode.childNodes.pop();
        		grandchildNodes.push(grandchildNode);
        	}
    	}
    	
    	var assertCount = 0;
    	while(grandchildNodes.length != 0) {
    		var grandchildNode = grandchildNodes.pop();
    		expect(grandchildNode.gameState.getPlayerToMove()).toBe(1);
    		assertCount++;
    	}
    	expect(assertCount).toBe(12);
    });
    
    it("generates game state tree for consecutive moves by same player", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	gameState.forceAllTokensWhite();
    	gameState.forceEmptyTokenAt(0, 2);
    	gameState.forceEmptyTokenAt(6, 5);
    	gameState.forceEmptyTokenAt(7, 0);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 0, 3);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 6, 6);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 6, 3);
    	gameState.forcePlayerToMove(2);
    	gameState.applyMoveFromPosition(2, constants.positionsByCoordinates[6][4]);
    	
    	var evaluator = new LS_REVERSI.ReversiGameEvaluator(2, 4, constants, moveHelper);
    	var tree = new LS_AI.Tree(evaluator, new LS_AI.CallStack());
    	
    	var node = tree.grow(gameState);
    	
    	expect(node.childNodes.length).toBe(2);
    	expect(node.childNodes[0].gameState.getPlayerToMove()).toBe(2);
    	expect(node.childNodes[1].gameState.getPlayerToMove()).toBe(2);
    	
    	expect(node.childNodes[0].childNodes.length).toBe(1);
    	expect(node.childNodes[1].childNodes.length).toBe(1);
    	//game has ended:
    	expect(node.childNodes[0].childNodes[0].gameState.getPlayerToMove()).toBeUndefined();
    	expect(node.childNodes[1].childNodes[0].gameState.getPlayerToMove()).toBeUndefined();
    });
});
    