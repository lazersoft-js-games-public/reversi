describe("AIConfigurationToReversiGameEvaluatorConverterTest", function() {
    it("generates game evaluator from parameterized AI config", function() {
    	var aiConfig = new LS_REVERSI.AIConfiguration("myConf", 10, false, "parameterized",
		    	{
    				evaluationDepth: 4,
		    		tokenScore: {
		    			takenCorner: 0,
		    		   	cornerExposing: 0,
		    		   	takenBorder: 0,
		    		   	borderExposing: 0,
		    		   	count: 1
		    		}
		    	});
    	
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	
    	var converter = new LS_REVERSI.AIConfigurationToReversiGameEvaluatorConverter(constants, moveHelper);
    	var evaluator = converter.convert(1, aiConfig);
    	
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	expect(evaluator.evaluate(gameState)).toBe(0);
    	gameState.applyMoveFromPosition(1, constants.positionsByCoordinates[2][3]);
    	expect(evaluator.evaluate(gameState)).toBe(4 - 1);
    });
    
    it("generates game evaluator from scripted AI config", function() {
    	var evaluateIntermediateState = function(gameState) {
			return 123;
		};
		var getEvaluationDepth = function(gameState) {
			return 2;
		};
    	var aiConfig = new LS_REVERSI.AIConfiguration("myConf", 10, false, "scripted", undefined, 
    			evaluateIntermediateState,
    			getEvaluationDepth);
    	
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	
    	var converter = new LS_REVERSI.AIConfigurationToReversiGameEvaluatorConverter(constants, moveHelper);
    	var evaluator = converter.convert(1, aiConfig);
    	
    	expect(evaluator.evaluateIntermediateState).toBe(evaluateIntermediateState);
    	expect(evaluator.getEvaluationDepth).toBe(getEvaluationDepth);
    });
});