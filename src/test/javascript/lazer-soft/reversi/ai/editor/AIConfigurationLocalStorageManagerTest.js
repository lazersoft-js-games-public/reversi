describe("AIConfigurationLocalStorageManager", function() {
    it("serializes/deserializes parameterized AI configuration", function() {
    	var storageManager = new LS_REVERSI.MockStorageManager("dummy");
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	
    	var manager = new LS_REVERSI.AIConfigurationLocalStorageManager(storageManager, constants);
    	
    	var aiConfig = new LS_REVERSI.AIConfiguration("myConf", 10, false, "parameterized",
    	{
    		evaluationDepth: 4,
    		tokenScore: {
    			takenCorner: 10,
    		   	cornerExposing: -8,
    		   	takenBorder: 5,
    		   	borderExposing: -3,
    		   	count: -2
    		}
    	});
    	
    	var serializedConf = manager.serialize(aiConfig);
    	var deserializedConf = manager.deserialize(serializedConf);
    	expect(deserializedConf).toEqual(aiConfig);
    });
    
    it("serializes/deserializes scripted AI configuration", function() {
    	var storageManager = new LS_REVERSI.MockStorageManager("dummy");
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	
    	var manager = new LS_REVERSI.AIConfigurationLocalStorageManager(storageManager, constants);
    	
    	var aiConfig = new LS_REVERSI.AIConfiguration("myConf", 1, false, "scripted", undefined, 
    			function(playerToWin, gameState, gameStateSummary) {
    				var a = 1;
    				var b = 2;
    				return a + b + playerToWin;
    			},
    			function(gameState) {
    				var c = 11;
    				var d = 12;
    				return c + d + gameState;
    			});
    	
    	var serializedConf = manager.serialize(aiConfig);
    	var deserializedConf = manager.deserialize(serializedConf);
    	
    	expect(aiConfig.name).toBe(deserializedConf.name);
    	expect(aiConfig.evaluateIntermediateState(11)).toBe(deserializedConf.evaluateIntermediateState(11));
    	expect(aiConfig.getEvaluationDepth(11)).toBe(deserializedConf.getEvaluationDepth(11));
    });
});