describe("ReversiGameState", function() {
    it("starts valid Reversi game", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, new LS_REVERSI.MoveHelper(constants));
    	var emptyTokenAssertionCount = 0;
    	for(var x = 0; x < 8; x++) {
    		for(var y = 0; y < 8; y++) {
        		if( (x == 3 || x == 4) && (y == 3 || y == 4)) {
        			continue;
        		} else {
        			expect(gameState.getTokenTypeAt(x, y)).toBe(constants.tokenTypeEmpty);
        			emptyTokenAssertionCount++;
        		}
        	}
    	}
    	expect(emptyTokenAssertionCount).toBe(60);
    	expect(gameState.getTokenTypeAt(3, 3)).toBe(constants.tokenTypeWhite);
    	expect(gameState.getTokenTypeAt(4, 3)).toBe(constants.tokenTypeBlack);
    	expect(gameState.getTokenTypeAt(3, 4)).toBe(constants.tokenTypeBlack);
    	expect(gameState.getTokenTypeAt(4, 4)).toBe(constants.tokenTypeWhite);
    	expect(gameState.playerToMove).toBe(1);
    	expect(gameState.isGameEnded()).toBeFalsy();
    });
    
    it("transitions to correct state after 1 move", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	var gameStateClone = gameState.clone();
    	gameState.applyMoveFromPosition(1, constants.positionsByCoordinates[3][2]);
    	var emptyTokenAssertionCount = 0;
    	for(var x = 0; x < 8; x++) {
    		for(var y = 0; y < 8; y++) {
        		if( (x == 3 || x == 4) && (y == 3 || y == 4)) {
        			continue;
        		} if (x == 3  && y == 2) {
        			continue;
        		} else {
        			expect(gameState.getTokenTypeAt(x, y)).toBe(constants.tokenTypeEmpty);
        			emptyTokenAssertionCount++;
        		}
        	}
    	}
    	expect(emptyTokenAssertionCount).toBe(59);
    	expect(gameState.getTokenTypeAt(3, 2)).toBe(constants.tokenTypeBlack);
    	expect(gameState.getTokenTypeAt(3, 3)).toBe(constants.tokenTypeBlack);
    	expect(gameState.getTokenTypeAt(4, 3)).toBe(constants.tokenTypeBlack);
    	expect(gameState.getTokenTypeAt(3, 4)).toBe(constants.tokenTypeBlack);
    	expect(gameState.getTokenTypeAt(4, 4)).toBe(constants.tokenTypeWhite);
    	expect(gameState.playerToMove).toBe(2);
    	expect(gameState.isGameEnded()).toBeFalsy();
    	expect(gameState.getLastMove()).toEqual(moveHelper.toMove(1, constants.positionsByCoordinates[3][2], gameStateClone));
    });
    
    it("transitions to correct state after 2 moves", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	gameState.applyMoveFromPosition(1, constants.positionsByCoordinates[3][2]);
    	var gameStateClone = gameState.clone();
    	gameState.applyMoveFromPosition(2, constants.positionsByCoordinates[2][4]);
    	
    	var emptyTokenAssertionCount = 0;
    	for(var x = 0; x < 8; x++) {
    		for(var y = 0; y < 8; y++) {
        		if( (x == 3 || x == 4) && (y == 3 || y == 4)) {
        			continue;
        		} if (x == 3  && y == 2) {
        			continue;
        		} if(x == 2  && y == 4) {
        			continue;
        		} else {
        			expect(gameState.getTokenTypeAt(x, y)).toBe(constants.tokenTypeEmpty);
        			emptyTokenAssertionCount++;
        		}
        	}
    	}
    	expect(emptyTokenAssertionCount).toBe(58);
    	expect(gameState.getTokenTypeAt(3, 2)).toBe(constants.tokenTypeBlack);
    	expect(gameState.getTokenTypeAt(3, 3)).toBe(constants.tokenTypeBlack);
    	expect(gameState.getTokenTypeAt(4, 3)).toBe(constants.tokenTypeBlack);
    	expect(gameState.getTokenTypeAt(2, 4)).toBe(constants.tokenTypeWhite);
    	expect(gameState.getTokenTypeAt(3, 4)).toBe(constants.tokenTypeWhite);
    	expect(gameState.getTokenTypeAt(4, 4)).toBe(constants.tokenTypeWhite);
    	expect(gameState.playerToMove).toBe(1);
    	expect(gameState.isGameEnded()).toBeFalsy();
    	expect(gameState.getLastMove()).toEqual(moveHelper.toMove(2, constants.positionsByCoordinates[2][4], gameStateClone));
    });
    
    it("forces token to black", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	
    	gameState.forceTokenAt(constants.tokenTypeBlack, 1, 1);
    	expect(gameState.getTokenTypeAt(1, 1)).toEqual(constants.tokenTypeBlack);
    	
    	gameState.forceTokenAt(constants.tokenTypeBlack, 6, 2);
    	expect(gameState.getTokenTypeAt(6, 2)).toEqual(constants.tokenTypeBlack);
    	
    	gameState.forceTokenAt(constants.tokenTypeBlack, 1, 6);
    	expect(gameState.getTokenTypeAt(1, 6)).toEqual(constants.tokenTypeBlack);
    	
    	gameState.forceTokenAt(constants.tokenTypeBlack, 5, 6);
    	expect(gameState.getTokenTypeAt(5, 6)).toEqual(constants.tokenTypeBlack);
    });
    
    it("forces token to white", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 1);
    	expect(gameState.getTokenTypeAt(1, 1)).toEqual(constants.tokenTypeWhite);
    	expect(gameState.getTokenCount()).toBe(5);
    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 6, 2);
    	expect(gameState.getTokenTypeAt(6, 2)).toEqual(constants.tokenTypeWhite);
    	expect(gameState.getTokenCount()).toBe(6);
    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 6);
    	expect(gameState.getTokenTypeAt(1, 6)).toEqual(constants.tokenTypeWhite);
    	expect(gameState.getTokenCount()).toBe(7);
    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 6);
    	expect(gameState.getTokenTypeAt(1, 6)).toEqual(constants.tokenTypeWhite);
    	expect(gameState.getTokenCount()).toBe(7);
    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 5, 6);
    	expect(gameState.getTokenTypeAt(5, 6)).toEqual(constants.tokenTypeWhite);
    	expect(gameState.getTokenCount()).toBe(8);
    });
    
    it("forces token to empty", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 1);
    	expect(gameState.getTokenTypeAt(1, 1)).toEqual(constants.tokenTypeWhite);    	
    	gameState.forceTokenAt(constants.tokenTypeBlack, 6, 2);
    	expect(gameState.getTokenTypeAt(6, 2)).toEqual(constants.tokenTypeBlack);    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 6);
    	expect(gameState.getTokenTypeAt(1, 6)).toEqual(constants.tokenTypeWhite);    	
    	gameState.forceTokenAt(constants.tokenTypeBlack, 5, 6);
    	expect(gameState.getTokenTypeAt(5, 6)).toEqual(constants.tokenTypeBlack);
    	gameState.forceTokenAt(constants.tokenTypeBlack, 5, 6);
    	expect(gameState.getTokenTypeAt(5, 6)).toEqual(constants.tokenTypeBlack);
    	
    	gameState.forceTokenAt(constants.tokenTypeEmpty, 1, 1);
    	expect(gameState.getTokenTypeAt(1, 1)).toEqual(constants.tokenTypeEmpty); 
    	expect(gameState.getTokenCount()).toBe(7);
    	gameState.forceTokenAt(constants.tokenTypeEmpty, 6, 2);
    	expect(gameState.getTokenTypeAt(6, 2)).toEqual(constants.tokenTypeEmpty);
    	expect(gameState.getTokenCount()).toBe(6);
    	gameState.forceTokenAt(constants.tokenTypeEmpty, 1, 6);
    	expect(gameState.getTokenTypeAt(1, 6)).toEqual(constants.tokenTypeEmpty);
    	expect(gameState.getTokenCount()).toBe(5);
    	gameState.forceTokenAt(constants.tokenTypeEmpty, 5, 6);
    	expect(gameState.getTokenTypeAt(5, 6)).toEqual(constants.tokenTypeEmpty);
    	expect(gameState.getTokenCount()).toBe(4);
    	gameState.forceTokenAt(constants.tokenTypeEmpty, 5, 6);
    	expect(gameState.getTokenTypeAt(5, 6)).toEqual(constants.tokenTypeEmpty);
    	expect(gameState.getTokenCount()).toBe(4);
    });
    
    it("finalizes game ok", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
    	var moveHelper = new LS_REVERSI.MoveHelper(constants);
    	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	
    	gameState.forceAllTokensBlack();
    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 1);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 2, 1);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 3, 1);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 4, 1);
    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 2);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 3, 2);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 6, 2);
    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 3);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 5, 3);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 6, 3);
    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 4);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 2, 4);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 3, 4);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 5, 4);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 6, 4);
    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 1, 5);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 2, 5);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 3, 5);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 4, 5);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 6, 5);
    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 2, 6);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 3, 6);
    	gameState.forceEmptyTokenAt(4, 6);
    	gameState.forceEmptyTokenAt(5, 6);
    	
    	gameState.forceTokenAt(constants.tokenTypeWhite, 3, 7);
    	gameState.forceTokenAt(constants.tokenTypeWhite, 6, 7);
    	gameState.forceEmptyTokenAt(4, 7);
    	gameState.forceEmptyTokenAt(5, 7);
    	
    	gameState.forcePlayerToMove(1);
    	gameState.applyMoveFromPosition(1, constants.positionsByCoordinates[5][7]);
    	
    	var gameEvaluator = new LS_REVERSI.ReversiGameEvaluator(2, 3, constants, moveHelper)
    	var minimax = new LS_AI.Minimax(gameEvaluator, gameState, new LS_AI.CallStack());
    	minimax.advanceGame();
    	expect(gameState === undefined).toBeFalsy();    	
    });
    
    it("advanecs on CPUVsCPU", function() {
    	var constants = new LS_REVERSI.ConstantsHolder();
		var moveHelper = new LS_REVERSI.MoveHelper(constants);
		
		var startingGameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
    	
		var mockEvaluateIntermediateState = function(playerToWin, gameState, gameStateSummary) {
			return 1;
		};
		
    	var evaluatorBlack = new LS_REVERSI.ReversiGameEvaluator(1, 3, constants, moveHelper, mockEvaluateIntermediateState);
    	var evaluatorWhite = new LS_REVERSI.ReversiGameEvaluator(2, 3, constants, moveHelper, mockEvaluateIntermediateState);
    	var runner = new LS_AI.CPUVsCPURunner(evaluatorBlack, evaluatorWhite, startingGameState);
    	
    	var currentGameState;
    	
    	currentGameState = runner.advanceGame();
    	currentGameState = runner.advanceGame();
    	currentGameState = runner.advanceGame();
    	currentGameState = runner.advanceGame();
    	expect(currentGameState).toNotBe(undefined);
    });
});
    
