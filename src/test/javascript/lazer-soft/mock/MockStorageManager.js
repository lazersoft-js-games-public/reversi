var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @param {String} type can be either "parameterized" or "scripted"
 * @returns {LS_REVERSI.MockStorageManager}
 */
LS_REVERSI.MockStorageManager = function(keyPrefix) {
	this.keyPrefix = keyPrefix;
	this.storage = new Array();
};

/**
 * @public
 */
LS_REVERSI.MockStorageManager.prototype.store = function(key, value) {
	this.storage[key] = value;
};

/**
 * @public
 */
LS_REVERSI.MockStorageManager.prototype.get = function(key) {
	return this.storage[key];
};

/**
 * @public
 */
LS_REVERSI.MockStorageManager.prototype.remove = function(key) {
	this.storage[key] = undefined;
};

/**
 * @public
 */
LS_REVERSI.MockStorageManager.prototype.listKeys = function() {
	return Object.keys(this.storage);
};