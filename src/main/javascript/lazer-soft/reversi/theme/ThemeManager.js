var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.ThemeManager}
 */
LS_REVERSI.ThemeManager = function() {
	
};

/**
 * @public
 * @param {String} themeDir
 * @param {String} [name]
 * @returns {LS_REVERSI.Theme}
 */
LS_REVERSI.ThemeManager.prototype.createTheme = function(imageLoader, themeName) {
	
	var boardImage = imageLoader.getImage("board");
	var boardCellHighlightImage = imageLoader.getImage("boardCellHighlight");
	var tokenBlackImage = imageLoader.getImage("tokenBlack");
	var tokenWhiteImage = imageLoader.getImage("tokenWhite");
	var tokenPlaceholderImage = imageLoader.getImage("tokenPlaceholder");
	var cursorActiveImage = imageLoader.getImage("cursorActive");
	var cursorWaitImage = imageLoader.getImage("cursorWait");
	
	return new LS_REVERSI.Theme(themeName, 
			boardImage, boardCellHighlightImage, 
			tokenBlackImage, tokenWhiteImage, 
			tokenPlaceholderImage, cursorActiveImage, cursorWaitImage);
};

/**
 * @public
 * @returns {LS_GAME_LOOP.LoadableImage[]}
 */
LS_REVERSI.ThemeManager.prototype.getThemeLoadableImages = function(themeDir) {
	if(!themeDir.endsWith("/")) {
		themeDir = themeDir + "/"
	}
	var themeLoadableImages = [
	                           new LS_GAME_LOOP.LoadableImage("board", themeDir + "board.png"),
	                           new LS_GAME_LOOP.LoadableImage("boardCellHighlight", themeDir + "board_cell_highlight.png"),
	                           new LS_GAME_LOOP.LoadableImage("tokenBlack", themeDir + "token_black.png"),
	                           new LS_GAME_LOOP.LoadableImage("tokenWhite", themeDir + "token_white.png"),
	                           new LS_GAME_LOOP.LoadableImage("tokenPlaceholder", themeDir + "token_placeholder.png"),
	                           new LS_GAME_LOOP.LoadableImage("cursorActive", themeDir + "cursor_active.png"),
	                           new LS_GAME_LOOP.LoadableImage("cursorWait", themeDir + "cursor_wait.png")
	                           ];
	return themeLoadableImages;
};