var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.Theme}
 */
LS_REVERSI.Theme = function(name, boardImage, boardCellHighlightImage, tokenBlackImage, tokenWhiteImage, tokenPlaceholderImage, cursorActiveImage, cursorWaitImage) {
	this.name = name;
	this.boardImage = boardImage;
	this.boardCellHighlightImage = boardCellHighlightImage;
	this.tokenBlackImage = tokenBlackImage;
	this.tokenWhiteImage = tokenWhiteImage;
	this.tokenPlaceholderImage = tokenPlaceholderImage;
	this.cursorActiveImage = cursorActiveImage;
	this.cursorWaitImage = cursorWaitImage;
};