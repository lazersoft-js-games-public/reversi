var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};


/**
 * @public 
 */
LS_REVERSI.launchGame = function(launchGameArgs) {
	try {
		if(launchGameArgs.debug) {
			if(window.devicePixelRatio  != 1) {
				console.warn("window.devicePixelRatio reports value different from 1: " + window.devicePixelRatio  + " . Your browser might be zoomed, which may generate blurriness (especially on text) in some browsers (like Chrome).")
			}
		}
	} catch(e) {
		//nothing to do;
	}
	
	var boardWidth = launchGameArgs.boardWidth;
	var themeManager = new LS_REVERSI.ThemeManager();
	
	var loadableImages = themeManager.getThemeLoadableImages(launchGameArgs.themeURL);
	loadableImages[loadableImages.length] = new LS_GAME_LOOP.LoadableImage("splashScreen", launchGameArgs.splashScreenURL)
	var imageLoader = new LS_GAME_LOOP.ImageLoader(loadableImages, function() {
		var canvasId = launchGameArgs.canvasId;
		var theme = themeManager.createTheme(imageLoader, "default-theme");
		var frameRenderer = new LS_GAME_LOOP.AnimationFrameRenderer();
		var constants = new LS_REVERSI.ConstantsHolder();
		var moveHelper = new LS_REVERSI.MoveHelper(constants);
		
		
		var flipAnimationsFrameCount = Math.floor(launchGameArgs.gameUpdatesPerSecond / (1000 / launchGameArgs.flipAnimationDurationMs));
		if(flipAnimationsFrameCount % 2 != 0) {
			flipAnimationsFrameCount = flipAnimationsFrameCount + 1;
		}
		if(flipAnimationsFrameCount < 2) {
			flipAnimationsFrameCount = 2;
		}
		var entityFactory = new LS_REVERSI.EntityFactory(theme, boardWidth, frameRenderer, flipAnimationsFrameCount, constants, canvasId, moveHelper);
		
		var tokensByPosition = entityFactory.createTokensByPosition();
		var boardCellsByPosition = entityFactory.createBoardCellsByPosition();
		
		var entities = new Array();
		for(var x = 0; x < constants.boardSize; x++) {
			for(var y = 0; y < constants.boardSize; y++) {
				entities.push(tokensByPosition[x][y]);
				entities.push(boardCellsByPosition[x][y]);
			}
		}
		
		var board = entityFactory.createBoard();
		var cursor = entityFactory.createCursor(launchGameArgs.hardwareCursor);
		entities.push(board);
		entities.push(cursor);
		
		var storageManager = new LS_REVERSI.StorageManager(launchGameArgs.localStorageKeyPrefix);
		var aiConfigurationLocalStorageManager = new LS_REVERSI.AIConfigurationLocalStorageManager(storageManager, constants);
		if(launchGameArgs.aiConfigurationFileLocation === "localStorage") {
			LS_REVERSI.setDefaultAIConfigsForLocalStorageIfNeeded(aiConfigurationLocalStorageManager);
		}
		var aiConfigurationImporter = new LS_REVERSI.AIConfigurationImporter(launchGameArgs.aiConfigurationFileLocation, aiConfigurationLocalStorageManager, function(aiConfigurations) {

			LS_REVERSI.loadCustomScriptedAIConfigurations();
			if(launchGameArgs.loadScriptedAIConfigurations) {
				var aiConfigPos = aiConfigurations.length;
				for(var i = 0; i < LS_REVERSI.SCRIPTED_AI_CONFIGURATIONS.length; i++) {
					aiConfigurations[aiConfigPos] = LS_REVERSI.SCRIPTED_AI_CONFIGURATIONS[i];
					aiConfigPos++;
				}
				aiConfigurations.sort(function(aiConfig1, aiConfig2) {
            		return aiConfig1.displayOrder - aiConfig2.displayOrder;
            	});
			}
							
			var aiConfigurationNames = new Array();
			for(var i = 0; i < aiConfigurations.length; i++) {
				aiConfigurationNames[i] = aiConfigurations[i].name;
			}
			
			var gameMenuInputEventQueue = new Array();
			var gameMenuInputListener = new LS_REVERSI.GameMenuInputListener(launchGameArgs.inputRadioIdHumanPlayerBlack, launchGameArgs.inputRadioIdHumanPlayerWhite, 
					launchGameArgs.inputSelectIdDifficulty, 
					launchGameArgs.inputButtonIdStart, launchGameArgs.inputButtonIdUndo, 
					launchGameArgs.outputSpanIdHumanScore, launchGameArgs.outputSpanIdAIScore,
					aiConfigurationNames,
					gameMenuInputEventQueue, 
					new LS_REVERSI.GameMenuInputEventFactory(),
					launchGameArgs.inputButtonIdPauseResume);
			gameMenuInputListener.listen();
			
			var splashScreen = entityFactory.createSplashScreen(imageLoader.getImage("splashScreen"));
			entities.push(splashScreen);
			
			var gameOverScreen = entityFactory.createGameOverScreen();
			entities.push(gameOverScreen);
			
			var demoOverScreen = entityFactory.createDemoOverScreen();
			entities.push(demoOverScreen);
			
			var gameFiniteStateMachine = entityFactory.createGameFiniteStateMachine(tokensByPosition, boardCellsByPosition, cursor, gameMenuInputEventQueue,  
					new LS_REVERSI.AIConfigurationToReversiGameEvaluatorConverter(constants, moveHelper, launchGameArgs.aiMaxNodesToEvaluate),
					aiConfigurations, launchGameArgs.debug, boardWidth, splashScreen, gameOverScreen, demoOverScreen);			
			gameFiniteStateMachine.registerScoreChangeListener(gameMenuInputListener);
			entities.push(gameFiniteStateMachine);
			
			if(launchGameArgs.debug) {
				var gameFSMDebug = new LS_REVERSI.GameFSMDebug(gameFiniteStateMachine);
				entities.push(gameFSMDebug);
				var gameLoopStatusEntity = new LS_GAME_LOOP.GameLoopStatusEntity(1000);
				gameLoopStatusEntity.x = boardWidth - 10 - gameLoopStatusEntity.width;
				entities.push(gameLoopStatusEntity);
			}
			
			var canvasInputEventQueue = new LS_GAME_LOOP.InputEventQueue(500);
			var canvasInputListener = new LS_GAME_LOOP.CanvasInputListener(canvasId, canvasInputEventQueue);
			canvasInputListener.listen();
							
			var gameLoop = new LS_GAME_LOOP.GameLoop(canvasId,
					boardWidth,
					boardWidth,
					new LS_GAME_LOOP.Metronome(1000/ launchGameArgs.gameUpdatesPerSecond),
					entities,
					canvasInputEventQueue,
					function(entity1, entity2) {
						return entity1.zIndex - entity2.zIndex;
					});
			
			var pauseResumeListener = new LS_GAME_LOOP.PauseResumeListener(document.getElementById(launchGameArgs.inputButtonIdPauseResume), gameLoop);
			pauseResumeListener.listen();
			
			gameLoop.start();
		});
		aiConfigurationImporter.importAIConfigurations();
			
	});
	imageLoader.loadImages();
};

/**
 * @public
 */
LS_REVERSI.launchAIEditor = function(launchAIEditorArgs) {
	var boardWidth = launchAIEditorArgs.boardWidth;
	var themeManager = new LS_REVERSI.ThemeManager();
	var loadableImages = themeManager.getThemeLoadableImages(launchAIEditorArgs.themeURL);
	loadableImages.push(new LS_GAME_LOOP.LoadableImage("retro-console-background", launchAIEditorArgs.retroConsoleBackground));
	var imageLoader = new LS_GAME_LOOP.ImageLoader(loadableImages, function() {
		var canvasId = launchAIEditorArgs.canvasId;
		var theme = themeManager.createTheme(imageLoader, "current-theme");
		var frameRenderer = new LS_GAME_LOOP.AnimationFrameRenderer();
		var constants = new LS_REVERSI.ConstantsHolder();
		var moveHelper = new LS_REVERSI.MoveHelper(constants);
		
		var retroConsole = new LS_REVERSI.RetroConsole(0, 0, boardWidth, imageLoader, frameRenderer);
		
		var entityFactory = new LS_REVERSI.EntityFactory(theme, boardWidth, frameRenderer, 2, constants, canvasId, moveHelper);
		
		var tokensByPosition = entityFactory.createTokensByPosition();
		var boardCellsByPosition = entityFactory.createBoardCellsByPosition();
		
		var entities = new Array();
		for(var x = 0; x < constants.boardSize; x++) {
			for(var y = 0; y < constants.boardSize; y++) {
				entities.push(tokensByPosition[x][y]);
				entities.push(boardCellsByPosition[x][y]);
			}
		}
		
		var board = entityFactory.createBoard();
		var cursor = entityFactory.createCursor(launchAIEditorArgs.hardwareCursor);
		entities.push(board);
		entities.push(cursor);
		
		var evaluatorBlack = new LS_REVERSI.ReversiGameEvaluator(1, 1, constants, moveHelper);
    	var evaluatorWhite = new LS_REVERSI.ReversiGameEvaluator(2, 1, constants, moveHelper);
    	var startingGameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);			
    	var cpuVsCpuRunner = new LS_AI.CPUVsCPURunner(evaluatorBlack, evaluatorWhite, startingGameState);
    	
    	var gameStateHistoryInputEventQueue = new Array();
    	
    	var parameterizedAIEditorInputListener1 = new LS_REVERSI.ParameterizedAIEditorInputListener(1,
    			launchAIEditorArgs.inputIdBaseEvaluationDepth1, 
    			launchAIEditorArgs.inputIdTakenCornerScore1, 
    			launchAIEditorArgs.inputIdCornerExposingTokenScore1, 
    			launchAIEditorArgs.inputIdTakenBorderScore1, 
    			launchAIEditorArgs.inputIdBorderExposingTokenScore1, 
    			launchAIEditorArgs.inputIdCountTokenScore1);	    	
    	var parameterizedAIEditorInputListener2 = new LS_REVERSI.ParameterizedAIEditorInputListener(2,
    			launchAIEditorArgs.inputIdBaseEvaluationDepth2,
    			launchAIEditorArgs.inputIdTakenCornerScore2, 
    			launchAIEditorArgs.inputIdCornerExposingTokenScore2, 
    			launchAIEditorArgs.inputIdTakenBorderScore2, 
    			launchAIEditorArgs.inputIdBorderExposingTokenScore2, 
    			launchAIEditorArgs.inputIdCountTokenScore2);
    	var commonControlsAIEditorInputListener1 = new LS_REVERSI.CommonControlsAIEditorInputListener(launchAIEditorArgs.inputIdName1,  
    			launchAIEditorArgs.inputIdDisplayOrder1,
    			launchAIEditorArgs.checkboxIdExportable1,
    			launchAIEditorArgs.buttonIdSave1, launchAIEditorArgs.buttonIdSet1, 1);
    	var commonControlsAIEditorInputListener2 = new LS_REVERSI.CommonControlsAIEditorInputListener(launchAIEditorArgs.inputIdName2,
    			launchAIEditorArgs.inputDisplayOrder2,
    			launchAIEditorArgs.checkboxExportable2,
    			launchAIEditorArgs.buttonIdSave2, launchAIEditorArgs.buttonIdSet2, 2);
    	
    	var storageManager = new LS_REVERSI.StorageManager(launchAIEditorArgs.localStorageKeyPrefix);
    	var aiConfigurationLocalStorageManager = new LS_REVERSI.AIConfigurationLocalStorageManager(storageManager, constants);
    	
    	var aiConfigurationListInputListener1 = new LS_REVERSI.AIConfigurationListInputListenr(launchAIEditorArgs.outputTableId1, 1, aiConfigurationLocalStorageManager);   	
    	var aiConfigurationListInputListener2 = new LS_REVERSI.AIConfigurationListInputListenr(launchAIEditorArgs.outputTableId2, 2, aiConfigurationLocalStorageManager);
    	
    	var aiEditorController = new LS_REVERSI.AIEditorController(parameterizedAIEditorInputListener1, 
    			parameterizedAIEditorInputListener2,
    			commonControlsAIEditorInputListener1, 
    			commonControlsAIEditorInputListener2,
    			aiConfigurationListInputListener1,
    			aiConfigurationListInputListener2,
    			aiConfigurationLocalStorageManager,
    			new LS_REVERSI.AIConfigurationToReversiGameEvaluatorConverter(constants, moveHelper, launchAIEditorArgs.aiMaxNodesToEvaluate),
    			cpuVsCpuRunner,
    			constants, moveHelper,
    			retroConsole);
    	aiEditorController.startControlling();
    	
    	var gameStateHistoryInputListener = new LS_REVERSI.GameStateHistoryInputListener(launchAIEditorArgs.buttonIdReset,
    			launchAIEditorArgs.buttonIdBack, 
    			launchAIEditorArgs.buttonIdPauseResume, 
    			launchAIEditorArgs.buttonIdForward,
    			gameStateHistoryInputEventQueue, new LS_REVERSI.GameStateHistoryInputEventFactory(), aiEditorController);
    	gameStateHistoryInputListener.listen();
    	
    	var aiConfigurationExporter = new LS_REVERSI.AIConfigurationExporter(aiConfigurationLocalStorageManager);
    	var aiConfigurationExporterInputListener = new LS_REVERSI.AIConfigurationExporterInputListener(launchAIEditorArgs.buttonIdExport, aiConfigurationExporter);
    	aiConfigurationExporterInputListener.listen();
    	
    	var aiConfigurationImporterInputListener = new LS_REVERSI.AIConfigurationImporterInputListener(launchAIEditorArgs.textInputIdImportLocation, 
    			launchAIEditorArgs.buttonIdImport,
    			new LS_REVERSI.AIConfigurationImporter(undefined, aiConfigurationLocalStorageManager, undefined),
    			aiConfigurationLocalStorageManager);
    	aiConfigurationImporterInputListener.listen();
    	
    	var cpuVsCpuFiniteStateMachine = new LS_REVERSI.CPUVsCPUFiniteStateMachine(tokensByPosition, 
    			boardCellsByPosition, 
    			startingGameState, 
    			cpuVsCpuRunner, 
    			constants, 
    			gameStateHistoryInputEventQueue,
    			gameStateHistoryInputListener,
    			false);
    	entities.push(cpuVsCpuFiniteStateMachine);
		
		var canvasInputEventQueue = new LS_GAME_LOOP.InputEventQueue(500);
		var canvasInputListener = new LS_GAME_LOOP.CanvasInputListener(canvasId, canvasInputEventQueue);
		canvasInputListener.listen();
		
		var gameLoop = new LS_GAME_LOOP.GameLoop(canvasId,
				boardWidth,
				boardWidth,
				new LS_GAME_LOOP.Metronome(1000 / launchAIEditorArgs.gameUpdatesPerSecond),
				entities,
				canvasInputEventQueue);
		
		gameLoop.start();	
					
		new LS_REVERSI.AIEditorDebugConsole(launchAIEditorArgs.debugCanvasId, cpuVsCpuFiniteStateMachine.gameStateHistory, imageLoader, frameRenderer).startConsole(retroConsole);
	});
	imageLoader.loadImages();
};

/**
 * @private
 */
LS_REVERSI.setDefaultAIConfigsForLocalStorageIfNeeded = function(aiConfigurationLocalStorageManager) {
	var localStorageAIConfigs = aiConfigurationLocalStorageManager.getAll();
	if(localStorageAIConfigs.length == 0) {
		aiConfigurationLocalStorageManager.store({
		    "name": "Default (Medium)",
		    "displayOrder": 0,
		    "exportable": true,
		    "type": "parameterized",
		    "parameters": {
		        "evaluationDepth": 1,
		        "tokenScore": {
		            "takenCorner": 1,
		            "cornerExposing": -1,
		            "takenBorder": 1,
		            "borderExposing": -1,
		            "count": 0
		        }
		    }
		});
	}
};