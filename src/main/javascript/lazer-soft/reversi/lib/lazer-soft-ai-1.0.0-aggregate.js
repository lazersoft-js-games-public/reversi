var LS_AI=LS_AI=LS_AI||{};
LS_AI.AStar=function(){this.aStarMap=null;
this.distanceCalculator=null;
this.exploredNodesCount=0;
this.closestNode=null
};
LS_AI.AStar.prototype.getPath=function(s,q,h,f,j,r){var m=new Date().getTime();
if(s<0||s>=j.getWidth()||q<0||q>=j.getHeight()){throw"Start coordinates outside aStarMap"
}if(h<0||h>=j.getWidth()||f<0||f>=j.getHeight()){throw"Destination coordinates outside aStarMap"
}this.reinit(j,r);
var a=new LS_AI.AStarNodeOpenList(j.getWidth(),j.getHeight(),500);
var b=new LS_AI.AStarNodeList(j.getWidth(),j.getHeight(),1000);
var p=r.getDistance(new LS_AI.AStarNode(h,f),new LS_AI.AStarNode(s,q));
var l=new LS_AI.AStarNode(h,f,p);
var u=new LS_AI.AStarNode(s,q,0,r.getDistance(new LS_AI.AStarNode(s,q),l));
a.add(u);
var c=null;
var e=false;
while(!a.isEmpty()){c=a.removeLowestFNode();
if(l.equals(c)){e=true;
break
}b.add(c);
this.processSucessors(c,a,b,l,j)
}var k=null;
if(e){k=c
}else{if(this.closestNode!=null){k=this.closestNode
}else{k=u
}}var d=this.getPathLength(k);
var v=new Array();
var o=k;
var n=0;
while(o!=null){v[d-1-n]=o;
n=n+1;
o=o.getParent()
}var t=new Date().getTime()-m;
return new LS_AI.AStarPath(v,e,t,this.exploredNodesCount)
};
LS_AI.AStar.prototype.processSucessors=function(c,d,e,h,b){var l=this.distanceCalculator.getNeighbors(c,b);
for(var f=0;
f<l.length;
f++){var k=l[f];
var j=e.searchByPosition(k.x,k.y);
if(j!=null){continue
}var g=c.g+(f<=3?1:this.distanceCalculator.diagonalNeighborG);
var a=d.searchByPosition(k.x,k.y);
if(a!=null&&a.g<=g){continue
}if(a!=null){d.remove(k.x,k.y)
}else{this.exploredNodesCount++
}k.g=g;
k.h=this.distanceCalculator.getDistance(k,h);
k.f=k.g+k.h;
k.setParent(c);
if(this.closestNode==null||this.closestNode.h>k.h){this.closestNode=k
}d.add(k)
}};
LS_AI.AStar.prototype.reinit=function(b,a){this.aStarMap=b;
this.distanceCalculator=a;
this.exploredNodesCount=0;
this.closestNode=null
};
LS_AI.AStar.prototype.getPathLength=function(a){var c=0;
var b=a;
while(b!=null){c++;
b=b.getParent()
}return c
};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.AStarCustomDistanceCalculator=function(){this.diagonalNeighborG=this.getDistance(new LS_AI.AStarNode(0,0),new LS_AI.AStarNode(1,1))
};
LS_AI.AStarCustomDistanceCalculator.prototype={getDistance:function(d,c){var b=Math.abs(d.x-c.x);
var a=Math.abs(d.y-c.y);
if(a>=b){return 0.41*b+0.941246*a
}else{return 0.41*a+0.941246*b
}},getNeighbors:function(e,d){var h=null;
var f=null;
var k=null;
var j=null;
var i=null;
var a=null;
var g=null;
var c=null;
var b=0;
if(e.x>0&&!d.isObstacle(e.x-1,e.y)){b++;
h=new LS_AI.AStarNode(e.x-1,e.y);
h.sourceNeighborG=1
}if(e.x<d.getWidth()-1&&!d.isObstacle(e.x+1,e.y)){b++;
k=new LS_AI.AStarNode(e.x+1,e.y);
k.sourceNeighborG=1
}if(e.y>0&&!d.isObstacle(e.x,e.y-1)){b++;
f=new LS_AI.AStarNode(e.x,e.y-1);
f.sourceNeighborG=1
}if(e.y<d.getHeight()-1&&!d.isObstacle(e.x,e.y+1)){b++;
j=new LS_AI.AStarNode(e.x,e.y+1);
j.sourceNeighborG=1
}if(e.x>0&&e.y>0&&!d.isObstacle(e.x-1,e.y-1)){b++;
i=new LS_AI.AStarNode(e.x-1,e.y-1);
i.sourceNeighborG=this.diagonalNeighborG
}if(e.x>0&&e.y<d.getHeight()-1&&!d.isObstacle(e.x-1,e.y+1)){b++;
c=new LS_AI.AStarNode(e.x-1,e.y+1);
c.sourceNeighborG=this.diagonalNeighborG
}if(e.x<d.getWidth()-1&&e.y>0&&!d.isObstacle(e.x+1,e.y-1)){b++;
a=new LS_AI.AStarNode(e.x+1,e.y-1);
a.sourceNeighborG=this.diagonalNeighborG
}if(e.x<d.getWidth()-1&&e.y<d.getHeight()-1&&!d.isObstacle(e.x+1,e.y+1)){b++;
g=new LS_AI.AStarNode(e.x+1,e.y+1);
g.sourceNeighborG=this.diagonalNeighborG
}var l=new Array();
b=0;
if(h!=null){l[b++]=h
}if(f!=null){l[b++]=f
}if(k!=null){l[b++]=k
}if(j!=null){l[b++]=j
}if(i!=null){l[b++]=i
}if(c!=null){l[b++]=c
}if(a!=null){l[b++]=a
}if(g!=null){l[b++]=g
}return l
}};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.AStarEuclideanDistanceCalculator=function(){this.diagonalNeighborG=this.getDistance(new LS_AI.AStarNode(0,0),new LS_AI.AStarNode(1,1))
};
LS_AI.AStarEuclideanDistanceCalculator.prototype={getDistance:function(b,a){return Math.sqrt(Math.pow(b.x-a.x,2)+Math.pow(b.y-a.y,2))
},getNeighbors:function(e,d){var h=null;
var f=null;
var k=null;
var j=null;
var i=null;
var a=null;
var g=null;
var c=null;
var b=0;
if(e.x>0&&!d.isObstacle(e.x-1,e.y)){b++;
h=new LS_AI.AStarNode(e.x-1,e.y);
h.sourceNeighborG=1
}if(e.x<d.getWidth()-1&&!d.isObstacle(e.x+1,e.y)){b++;
k=new LS_AI.AStarNode(e.x+1,e.y);
k.sourceNeighborG=1
}if(e.y>0&&!d.isObstacle(e.x,e.y-1)){b++;
f=new LS_AI.AStarNode(e.x,e.y-1);
f.sourceNeighborG=1
}if(e.y<d.getHeight()-1&&!d.isObstacle(e.x,e.y+1)){b++;
j=new LS_AI.AStarNode(e.x,e.y+1);
j.sourceNeighborG=1
}if(e.x>0&&e.y>0&&!d.isObstacle(e.x-1,e.y-1)){b++;
i=new LS_AI.AStarNode(e.x-1,e.y-1);
i.sourceNeighborG=this.diagonalNeighborG
}if(e.x>0&&e.y<d.getHeight()-1&&!d.isObstacle(e.x-1,e.y+1)){b++;
c=new LS_AI.AStarNode(e.x-1,e.y+1);
c.sourceNeighborG=this.diagonalNeighborG
}if(e.x<d.getWidth()-1&&e.y>0&&!d.isObstacle(e.x+1,e.y-1)){b++;
a=new LS_AI.AStarNode(e.x+1,e.y-1);
a.sourceNeighborG=this.diagonalNeighborG
}if(e.x<d.getWidth()-1&&e.y<d.getHeight()-1&&!d.isObstacle(e.x+1,e.y+1)){b++;
g=new LS_AI.AStarNode(e.x+1,e.y+1);
g.sourceNeighborG=this.diagonalNeighborG
}var l=new Array();
b=0;
if(h!=null){l[b++]=h
}if(f!=null){l[b++]=f
}if(k!=null){l[b++]=k
}if(j!=null){l[b++]=j
}if(i!=null){l[b++]=i
}if(c!=null){l[b++]=c
}if(a!=null){l[b++]=a
}if(g!=null){l[b++]=g
}return l
}};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.AStarManhattanDistanceCalculator=function(){this.diagonalNeighborG=this.getDistance(new LS_AI.AStarNode(0,0),new LS_AI.AStarNode(1,1))
};
LS_AI.AStarManhattanDistanceCalculator.prototype={getDistance:function(b,a){var c=Math.abs(b.x-a.x,2)+Math.abs(b.y-a.y,2);
return c
},getNeighbors:function(e,d){var h=null;
var f=null;
var k=null;
var j=null;
var i=null;
var a=null;
var g=null;
var c=null;
var b=0;
if(e.x>0&&!d.isObstacle(e.x-1,e.y)){b++;
h=new LS_AI.AStarNode(e.x-1,e.y);
h.sourceNeighborG=1
}if(e.x<d.getWidth()-1&&!d.isObstacle(e.x+1,e.y)){b++;
k=new LS_AI.AStarNode(e.x+1,e.y);
k.sourceNeighborG=1
}if(e.y>0&&!d.isObstacle(e.x,e.y-1)){b++;
f=new LS_AI.AStarNode(e.x,e.y-1);
f.sourceNeighborG=1
}if(e.y<d.getHeight()-1&&!d.isObstacle(e.x,e.y+1)){b++;
j=new LS_AI.AStarNode(e.x,e.y+1);
j.sourceNeighborG=1
}if(e.x>0&&e.y>0&&!d.isObstacle(e.x-1,e.y-1)){b++;
i=new LS_AI.AStarNode(e.x-1,e.y-1);
i.sourceNeighborG=this.diagonalNeighborG
}if(e.x>0&&e.y<d.getHeight()-1&&!d.isObstacle(e.x-1,e.y+1)){b++;
c=new LS_AI.AStarNode(e.x-1,e.y+1);
c.sourceNeighborG=this.diagonalNeighborG
}if(e.x<d.getWidth()-1&&e.y>0&&!d.isObstacle(e.x+1,e.y-1)){b++;
a=new LS_AI.AStarNode(e.x+1,e.y-1);
a.sourceNeighborG=this.diagonalNeighborG
}if(e.x<d.getWidth()-1&&e.y<d.getHeight()-1&&!d.isObstacle(e.x+1,e.y+1)){b++;
g=new LS_AI.AStarNode(e.x+1,e.y+1);
g.sourceNeighborG=this.diagonalNeighborG
}var l=new Array();
b=0;
if(h!=null){l[b++]=h
}if(f!=null){l[b++]=f
}if(k!=null){l[b++]=k
}if(j!=null){l[b++]=j
}if(i!=null){l[b++]=i
}if(c!=null){l[b++]=c
}if(a!=null){l[b++]=a
}if(g!=null){l[b++]=g
}return l
}};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.AStarMap=function(b,a){this.width=b;
this.height=a;
this.terrain=new Array()
};
LS_AI.AStarMap.prototype={setObstacle:function(a,b){if(this.terrain[a]===undefined){this.terrain[a]=new Array()
}this.terrain[a][b]=true
},isObstacle:function(a,c){var b=this.terrain[a];
if(b===undefined){return false
}return b[c]
},clearObstacle:function(a,c){var b=this.terrain[a];
if(b!==undefined){b.splice(c,1)
}},setObstacles:function(f,c,e,b){for(var a=Math.min(f,e);
a<=Math.max(f,e);
a++){for(var d=Math.min(c,b);
d<=Math.max(c,b);
d++){this.setObstacle(a,d)
}}},getWidth:function(){return this.width
},getHeight:function(){return this.height
}};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.AStarNode=function(a,d,c,b){this.x=a;
this.y=d;
this.g=c;
this.h=b;
this.f=c+b;
this.hash=a;
this.parent=null
};
LS_AI.AStarNode.prototype={getParent:function(){return this.parent
},setParent:function(a){this.parent=a
},getF:function(){return this.f
},hashCode:function(){return this.x
},equals:function(a){return this.y==a.y&&this.x==a.x
},toString:function(){return"["+this.x+":"+this.y+" "+this.getF().toFixed(2)+"]"
},toDetailedString:function(){return"["+this.x+":"+this.y+" "+this.g.toFixed(2)+" + "+this.h.toFixed(2)+" = "+this.getF().toFixed(2)
}};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.AStarPath=function(a,b,d,c){this.nodes=a;
this.reachesDestination=b;
this.computeTimeMs=d;
this.exploredNodesCount=c
};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.AStarNodeHashmap=function(a){this.buckets=new Array();
this.bucketCount=a;
this.size=0
};
LS_AI.AStarNodeHashmap.prototype.add=function(e){var b=this.locateBucketPosition(e);
var f=this.buckets[b];
if(f===undefined){f=new Array();
this.buckets[b]=f;
f[0]=e;
this.size++
}else{var a=null;
for(var c=0;
c<f.length;
c++){var d=f[c];
if(d!==undefined&&d.equals(e)){a=c;
break
}}if(a!=null){f[a]=e
}else{f[f.length]=e;
this.size++
}}};
LS_AI.AStarNodeHashmap.prototype.get=function(a,e){var b=new LS_AI.AStarNode(a,e);
var d=this.locateBucket(b);
var c=this.locateNode(b,d);
return c
};
LS_AI.AStarNodeHashmap.prototype.remove=function(a,g){var c=new LS_AI.AStarNode(a,g);
var b=this.locateBucketPosition(c);
var f=this.buckets[b];
var e=this.locateNodePosition(c,f);
var d=null;
if(e!=null){d=f[e];
f.splice(e,1);
this.size--
}return d
};
LS_AI.AStarNodeHashmap.prototype.getSize=function(){return this.size
};
LS_AI.AStarNodeHashmap.prototype.isEmpty=function(){return this.size<=0
};
LS_AI.AStarNodeHashmap.prototype.locateBucketPosition=function(a){return a.hashCode()%this.bucketCount
};
LS_AI.AStarNodeHashmap.prototype.locateBucket=function(b){var a=this.locateBucketPosition(b);
var c=this.buckets[a];
return c
};
LS_AI.AStarNodeHashmap.prototype.locateNodePosition=function(c,e){var a=null;
if(e!==undefined){for(var b=0;
b<e.length;
b++){var d=e[b];
if(d!==undefined&&d.equals(c)){a=b;
break
}}}return a
};
LS_AI.AStarNodeHashmap.prototype.locateNode=function(b,e){var d=null;
if(e!==undefined){for(var a=0;
a<e.length;
a++){var c=e[a];
if(c!==undefined&&c.equals(b)){d=c;
break
}}}return d
};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.AStarNodeList=function(c,b,a){this.nodePositionHash=new LS_AI.AStarNodeHashmap(a===undefined?5000:a)
};
LS_AI.AStarNodeList.prototype={add:function(a){this.nodePositionHash.add(a)
},remove:function(a,b){this.nodePositionHash.remove(a,b)
},searchByPosition:function(a,b){return this.nodePositionHash.get(a,b)
},getSize:function(){return this.nodePositionHash.getSize()
},isEmpty:function(){return this.nodePositionHash.isEmpty()
}};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.AStarNodeOpenList=function(b,a){this.lowestFNode=new LS_AI.AStarNode(0,0,Number.POSITIVE_INFINITY,Number.POSITIVE_INFINITY);
LS_AI.AStarNodeList.call(this,b,a)
};
LS_AI.AStarNodeOpenList.prototype=Object.create(LS_AI.AStarNodeList.prototype);
LS_AI.AStarNodeOpenList.prototype.constructor=LS_AI.AStarNodeOpenList;
LS_AI.AStarNodeOpenList.prototype.add=function(a){if(a==null){throw"Node cannot be null here"
}if(a===undefined){throw"Node cannot be undefined here"
}this.nodePositionHash.add(a);
if(a.getF()<=this.lowestFNode.getF()){this.lowestFNode=a
}};
LS_AI.AStarNodeOpenList.prototype.remove=function(a,c){var b=this.nodePositionHash.remove(a,c);
if(b!=null){if(b.getF()==this.lowestFNode.getF()){this.lowestFNode=this.calculateLowestFNode()
}}return b
};
LS_AI.AStarNodeOpenList.prototype.removeLowestFNode=function(){return this.remove(this.lowestFNode.x,this.lowestFNode.y)
};
LS_AI.AStarNodeOpenList.prototype.calculateLowestFNode=function(){var b=new LS_AI.AStarNode(0,0,0,Number.POSITIVE_INFINITY);
for(var a=0;
a<this.nodePositionHash.buckets.length;
a++){var d=this.nodePositionHash.buckets[a];
if(d!==undefined){for(j=0;
j<d.length;
j++){var c=d[j];
if(c.getF()<b.getF()){b=c
}}}}return b
};



var LS_AI=LS_AI||{};
LS_AI.CPUVsCPURunner=function(c,a,b){this.minimaxBlack=new LS_AI.Minimax(c,b,new LS_AI.CallStack());
this.minimaxWhite=new LS_AI.Minimax(a,b,new LS_AI.CallStack());
this.gameState=b;
this.minimaxPerPlayer=[];
this.minimaxPerPlayer[1]=this.minimaxBlack;
this.minimaxPerPlayer[2]=this.minimaxWhite
};
LS_AI.CPUVsCPURunner.prototype.advanceGame=function(){if(!this.gameState.isGameEnded()){var a=this.gameState.getPlayerToMove();
var c=this.minimaxPerPlayer[a];
this.gameState=c.advanceGame();
var b=this.minimaxPerPlayer[this.getOpponenet(a)];
b.advanceGame(this.gameState.getLastMove());
return this.gameState
}else{throw"Can't advance, game has ended"
}};
LS_AI.CPUVsCPURunner.prototype.setStartingGameState=function(a){this.gameState=a;
this.minimaxBlack.setStartingGameState(a);
this.minimaxWhite.setStartingGameState(a)
};
LS_AI.CPUVsCPURunner.prototype.setGameEvaluatorBlack=function(a){this.minimaxBlack.setGameEvaluator(a)
};
LS_AI.CPUVsCPURunner.prototype.setGameEvaluatorWhite=function(a){this.minimaxWhite.setGameEvaluator(a)
};
LS_AI.CPUVsCPURunner.prototype.getOpponenet=function(a){return 3-a
};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.GameEvaluator=function(a){this.playerToWin=a
};
LS_AI.GameEvaluator.prototype.getNextGameStateIterator=function(a){throw"Not implemented"
};
LS_AI.GameEvaluator.prototype.evaluate=function(a){throw"Not implemented"
};
LS_AI.GameEvaluator.prototype.getEvaluationDepth=function(a){throw"Not implemented"
};
LS_AI.GameEvaluator.prototype.getMaxNodesToEvaluate=function(){return Number.POSITIVE_INFINITY
};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.GameState=function(a){this.playerToMove=a;
this.minimaxRunReport=null
};
LS_AI.GameState.prototype.applyMove=function(b,a){throw"Not implemented"
};
LS_AI.GameState.prototype.getLastMove=function(){throw"Not implemented"
};
LS_AI.GameState.prototype.isGameEnded=function(){throw"Not implemented"
};
LS_AI.GameState.prototype.getPlayerToMove=function(){return this.playerToMove
};
LS_AI.GameState.prototype.getMinimaxRunReport=function(){return this.minimaxRunReport
};
LS_AI.GameState.prototype.setMinimaxRunReport=function(a){this.minimaxRunReport=a
};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.Minimax=function(d,c,b,a){this.gameEvaluator=d;
this.gameState=c;
this.callStack=b===undefined?new LS_AI.CallStack():b;
this.debug=a===undefined?false:a;
this.cpuPlayer=d.playerToWin;
this.humanPlayer=this.getOpponent(this.cpuPlayer)
};
LS_AI.Minimax.prototype.advanceGame=function(){if(this.gameState.isGameEnded()){throw"Game has ended. Can't advance game"
}if(this.gameState.getPlayerToMove()==this.cpuPlayer){if(this.debug){console.log("Generating Minimax tree for player: "+this.cpuPlayer+" using GameEvaluator: "+this.gameEvaluator.toString())
}var d=new LS_AI.Tree(this.gameEvaluator,this.callStack,this.debug);
var c=d.grow(this.gameState);
if(this.debug){console.log("Final tree growth: "+d.toString())
}var a=this.getMaxScoredChildNode(c);
this.gameState=a.gameState;
var e=this.generateMinimaxRunReport(d);
this.gameState.setMinimaxRunReport(e)
}else{if(arguments.length==0){throw"Human player's turn but no move provided for game advancement"
}var b=arguments[0];
this.gameState.applyMove(this.humanPlayer,b)
}return this.gameState
};
LS_AI.Minimax.prototype.getGameOutcome=function(a){var b=this.gameEvaluator.evaluate(a);
if(b==Number.POSITIVE_INFINITY){return this.gameEvaluator.playerToWin
}else{if(b==Number.NEGATIVE_INFINITY){return this.getOpponent(this.gameEvaluator.playerToWin)
}else{if(a.isGameEnded()&&b==0){return 0
}else{return undefined
}}}};
LS_AI.Minimax.prototype.getHumanPlayer=function(){return this.humanPlayer
};
LS_AI.Minimax.prototype.getCpuPlayer=function(){return this.cpuPlayer
};
LS_AI.Minimax.prototype.setStartingGameState=function(a){this.gameState=a
};
LS_AI.Minimax.prototype.setGameEvaluator=function(a){this.gameEvaluator=a;
this.cpuPlayer=this.gameEvaluator.playerToWin;
this.humanPlayer=this.getOpponent(this.cpuPlayer)
};
LS_AI.Minimax.prototype.getMaxScoredChildNode=function(d){var e=Number.NEGATIVE_INFINITY;
var c;
for(var b=0;
b<d.childNodes.length;
b++){var a=d.childNodes[b];
if(a.score>e||(a.score==e&&c===undefined)){e=a.score;
c=a
}}return c
};
LS_AI.Minimax.prototype.getOpponent=function(a){return 3-a
};
LS_AI.Minimax.prototype.generateMinimaxRunReport=function(a){return new LS_AI.MinimaxRunReport(a.nodesCount,a.nodesGenerationAndScoringMs,a.lastRootScore,a.lastEvaluationDepth,a.lastMinEvaluationDepthReached,a.maxNodesToEvaluateReached)
};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.MinimaxRunReport=function(f,a,e,d,c,b){this.exploredNodesCount=f;
this.durationMs=a;
this.score=e;
this.evaluationDepth=d;
this.minEvaluationDepthReached=c;
this.maxNodesToEvaluateReached=b
};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.NextGameStateIterator=function(a){this.currentGameState=a
};
LS_AI.NextGameStateIterator.prototype.next=function(){throw"Not implemented"
};
LS_AI.NextGameStateIterator.prototype.hasNext=function(){throw"Not implemented"
};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.CallStack=function(){this.stack=new Array()
};
LS_AI.CallStack.prototype.pop=function(b){var a=this.stack.pop();
if(this.stack.length!=0){this.peek().v=b
}return a
};
LS_AI.CallStack.prototype.push=function(a){this.stack.push(a)
};
LS_AI.CallStack.prototype.peek=function(){if(!this.isEmpty()){return this.stack[this.stack.length-1]
}return null
};
LS_AI.CallStack.prototype.isEmpty=function(){return this.stack.length==0
};
LS_AI.CallStack.prototype.getSize=function(){return this.stack.length
};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.StackFrame=function(a,d,c,b){this.node=a;
this.alpha=d;
this.beta=c;
this.maximizingPlayer=b;
this.v=null
};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.Tree=function(c,b,a){this.gameEvaluator=c;
this.callStack=b;
this.debugGrowth=a===undefined?false:a;
this.nodesCount=0;
this.nodesGenerationAndScoringMs=0;
this.lastEvaluationDepth=0;
this.lastMinEvaluationDepthReached=null;
this.lastRootScore=0;
this.maxNodesToEvaluateReached=false
};
LS_AI.Tree.prototype.grow=function(b){this.lastEvaluationDepth=this.gameEvaluator.getEvaluationDepth(b);
var a=new LS_AI.TreeNode(b,this.gameEvaluator.getEvaluationDepth(b),this.gameEvaluator);
this.lastMinEvaluationDepthReached=a.depth;
this.nodesCount++;
this.generateAndScoreNodes(a);
this.lastRootScore=a.score;
return a
};
LS_AI.Tree.prototype.toString=function(){return"nodes: "+this.nodesCount+", total time: "+(this.nodesGenerationAndScoringMs)+" ms, lastEvaluationDepth: "+this.lastEvaluationDepth+", maxNodesToEvaluateReached: "+this.maxNodesToEvaluateReached+", root score: "+this.lastRootScore
};
LS_AI.Tree.prototype.generateAndScoreNodes=function(a){var b=new Date().getTime();
this.alphaBeta(a);
this.nodesGenerationAndScoringMs=new Date().getTime()-b
};
LS_AI.Tree.prototype.alphaBeta=function(c){this.callStack.push(new LS_AI.StackFrame(c,Number.NEGATIVE_INFINITY,Number.POSITIVE_INFINITY,true));
while(!this.callStack.isEmpty()){var b=this.callStack.peek();
if(b.node.depth==0||b.node.gameEnded||this.isMaxNodesToEvaluateReached(b.node)){var a=this.gameEvaluator.evaluate(b.node.gameState);
b.node.score=a;
b=this.callStack.pop(a);
continue
}if(b.node.hasNextChild()){var a=this.scoreItermediaryNode(b);
if(a!=null){b.node.score=a;
if(!(b.alpha<=b.beta)){b=this.callStack.pop(a);
continue
}}b=new LS_AI.StackFrame(b.node.generateAndGetNextChildNode(),b.alpha,b.beta,this.isMaximizing(b.node));
if(b.node.depth<this.lastMinEvaluationDepthReached){this.lastMinEvaluationDepthReached=b.node.depth
}this.nodesCount++;
if(this.debugGrowth&&this.nodesCount%10000==0){console.log("Tree growth at "+this.nodesCount+" nodes, now...")
}this.callStack.push(b)
}else{var a=this.scoreItermediaryNode(b);
b.node.score=a;
b=this.callStack.pop(a)
}}};
LS_AI.Tree.prototype.isMaximizing=function(a){return a.gameState.playerToMove==this.gameEvaluator.playerToWin
};
LS_AI.Tree.prototype.scoreItermediaryNode=function(b){if(b.v==null){return null
}var c=b.v;
var a;
if(this.isMaximizing(b.node)){if(b.node.score==null){b.node.score=Number.NEGATIVE_INFINITY
}a=Math.max(b.node.score,c);
b.alpha=Math.max(b.alpha,a)
}else{if(b.node.score==null){b.node.score=Number.POSITIVE_INFINITY
}a=Math.min(b.node.score,c);
b.beta=Math.min(b.beta,a)
}return a
};
LS_AI.Tree.prototype.isMaxNodesToEvaluateReached=function(a){if(!this.maxNodesToEvaluateReached){this.maxNodesToEvaluateReached=this.nodesCount>=this.gameEvaluator.getMaxNodesToEvaluate()
}return this.maxNodesToEvaluateReached
};
var LS_AI=LS_AI=LS_AI||{};
LS_AI.TreeNode=function(a,b,c){this.gameState=a;
this.depth=b;
this.score;
this.gameEnded=this.gameState.isGameEnded();
if(!a.isGameEnded()){this.evaluator=c;
this.childNodes=new Array();
this.nextGameStateIterator=this.evaluator.getNextGameStateIterator(this.gameState)
}};
LS_AI.TreeNode.prototype.generateAndGetNextChildNode=function(){var a=null;
if(this.nextGameStateIterator.hasNext()){var b=this.nextGameStateIterator.next();
a=new LS_AI.TreeNode(b,this.depth-1,this.evaluator);
this.childNodes.push(a)
}return a
};
LS_AI.TreeNode.prototype.hasNextChild=function(){return !this.gameEnded&&this.nextGameStateIterator.hasNext()
};
LS_AI.TreeNode.prototype.equals=function(a){return this.gameState.equals(a.gameState)
};
LS_AI.TreeNode.prototype.toString=function(){return this.gameState.toString()
};
