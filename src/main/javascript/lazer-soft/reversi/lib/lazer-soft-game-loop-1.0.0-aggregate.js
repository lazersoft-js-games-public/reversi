var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.Entity=function(b,e,c,a,d){this.x=b;
this.y=e;
this.width=c;
this.height=a;
this.zIndex=d===undefined?100:d
};
LS_GAME_LOOP.Entity.prototype.processInput=function(a){throw"Called abstract function"
};
LS_GAME_LOOP.Entity.prototype.updateState=function(){throw"Called abstract function"
};
LS_GAME_LOOP.Entity.prototype.updateGraphics=function(a){throw"Called abstract function"
};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.GameLoop=function(g,b,e,d,f,c,a){this.canvasId=g;
this.canvasWidth=b;
this.canvasHeight=e;
this.canvas=null;
this.context2d=null;
this.metronome=d;
this.entities=f;
this.inputEventQueue=c;
this.drawOrderCompareFunction=a===undefined?this.defaultDrawOrderCompareFunction:a;
this.started=false;
this.paused=false;
this.toResume=false
};
LS_GAME_LOOP.GameLoop.prototype.start=function(){if(this.started){throw"Game loop already started"
}this.initCanvas();
this.initRequestAnimationFrameFunction();
this.started=true;
this.metronome.start();
this.loop()
};
LS_GAME_LOOP.GameLoop.prototype.togglePauseResume=function(){if(!this.paused){this.metronome.pause();
this.paused=true
}else{this.metronome.resume();
this.paused=false;
this.toResume=true;
this.loop()
}};
LS_GAME_LOOP.GameLoop.prototype.stop=function(){if(!this.started){return false
}this.started=false
};
LS_GAME_LOOP.GameLoop.prototype.isPaused=function(){return this.paused
};
LS_GAME_LOOP.GameLoop.prototype.loop=function(){var a=this;
if(a.toResume){a.metronome.resume();
a.toResume=false
}var c=a.metronome.getTicksSinceLast();
for(var b=0;
b<c;
b++){a.processInput();
a.updateState()
}a.updateGraphics();
if(a.paused){return
}window.requestAnimationFrame(function(){a.loop()
})
};
LS_GAME_LOOP.GameLoop.prototype.processInput=function(){var d=this.inputEventQueue.popAllEvents();
for(var c=0;
c<d.length;
c++){var e=d[c];
for(var b=0;
b<this.entities.length;
b++){var a=this.entities[b];
a.processInput(e)
}}};
LS_GAME_LOOP.GameLoop.prototype.updateState=function(){for(var b=0;
b<this.entities.length;
b++){var a=this.entities[b];
a.updateState()
}};
LS_GAME_LOOP.GameLoop.prototype.updateGraphics=function(){if(this.drawOrderCompareFunction!=null){this.entities.sort(this.drawOrderCompareFunction)
}this.context2d.clearRect(0,0,this.canvas.width,this.canvas.height);
this.context2d.fillStyle="black";
for(var b=0;
b<this.entities.length;
b++){var a=this.entities[b];
a.updateGraphics(this.context2d)
}};
LS_GAME_LOOP.GameLoop.prototype.initCanvas=function(){this.canvas=document.getElementById(this.canvasId);
if(this.canvas==null||this.canvas===undefined){throw"Could not get canvas for id "+this.canvasId
}this.canvas.width=this.canvasWidth;
this.canvas.height=this.canvasHeight;
this.context2d=this.canvas.getContext("2d")
};
LS_GAME_LOOP.GameLoop.prototype.initRequestAnimationFrameFunction=function(){window.requestAnimationFrame=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||window.msRequestAnimationFrame||window.oRequestAnimationFrame;
if(window.requestAnimationFrame==null||window.requestAnimationFrame===undefined){throw"Could not get requestAnimatorFrame function"
}};
LS_GAME_LOOP.GameLoop.prototype.defaultDrawOrderCompareFunction=function(b,a){if(b.zIndex==a.zIndex){return b.y-a.y
}else{return b.zIndex-a.zIndex
}};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.Metronome=function(a){this.tickDurationMs=a;
this.started=false;
this.lastTicksCountTimeMs=null;
this.pauseTimeMs=null
};
LS_GAME_LOOP.Metronome.prototype.start=function(){if(this.started){throw"Metronome already started"
}this.lastTicksCountTimeMs=this.getCurrentTimeMs();
this.started=true
};
LS_GAME_LOOP.Metronome.prototype.pause=function(){if(!this.started||this.isPaused()){return false
}this.pauseTimeMs=this.getCurrentTimeMs();
return true
};
LS_GAME_LOOP.Metronome.prototype.resume=function(){if(!this.started||!this.isPaused()){return false
}var a=this.getCurrentTimeMs();
this.lastTicksCountTimeMs+=a-this.pauseTimeMs;
this.pauseTimeMs=null;
return true
};
LS_GAME_LOOP.Metronome.prototype.getTicksSinceLast=function(){var b=this.getCurrentTimeMs();
var a=Math.floor((b-this.lastTicksCountTimeMs)/this.tickDurationMs);
this.lastTicksCountTimeMs=this.lastTicksCountTimeMs+a*this.tickDurationMs;
return a
};
LS_GAME_LOOP.Metronome.prototype.update=function(){return this.pauseTimeMs!=null
};
LS_GAME_LOOP.Metronome.prototype.getCurrentTimeMs=function(){return new Date().getTime()
};
LS_GAME_LOOP.Metronome.prototype.isPaused=function(){return this.pauseTimeMs!=null
};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.AnimatedImagesFactory=function(a){this.imageLoader=a
};
LS_GAME_LOOP.AnimatedImagesFactory.prototype.createAnimationFrame=function(a,b){return new LS_GAME_LOOP.AnimationFrame(this.imageLoader.getImage(a),b)
};
LS_GAME_LOOP.AnimatedImagesFactory.prototype.createAnimation=function(e,d,f,b){var a=new Array();
for(var c=0;
c<d.length;
c++){var g=this.createAnimationFrame(e,d[c]);
a[c]=g
}return new LS_GAME_LOOP.Animation(a,f,b)
};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.Animation=function(a,c,b){this.animationFrames=a;
this.ticksPerFrame=c;
this.looping=b===undefined?false:b;
this.started=false;
this.ended=false;
this.accumulatedTicks=0;
this.currentFrameIdx=-1
};
LS_GAME_LOOP.Animation.prototype.start=function(){if(this.started){return false
}this.currentFrameIdx=0;
this.started=true;
return true
};
LS_GAME_LOOP.Animation.prototype.stop=function(){if(!this.started){return false
}this.started=false;
return true
};
LS_GAME_LOOP.Animation.prototype.reset=function(){this.started=false;
this.ended=false;
this.accumulatedTicks=0;
this.currentFrameIdx=-1
};
LS_GAME_LOOP.Animation.prototype.isEnded=function(){if(this.looping){return false
}return this.ended
};
LS_GAME_LOOP.Animation.prototype.onTick=function(){if(this.started){this.accumulatedTicks++
}};
LS_GAME_LOOP.Animation.prototype.getCurrentAnimationFrame=function(){if(this.currentFrameIdx==-1){return undefined
}this.updateCurrentFrameIdx();
return this.animationFrames[this.currentFrameIdx]
};
LS_GAME_LOOP.Animation.prototype.getAnimationFrame=function(a){return this.animationFrames[a]
};
LS_GAME_LOOP.Animation.prototype.getFirstAnimationFrame=function(){return this.getAnimationFrame(0)
};
LS_GAME_LOOP.Animation.prototype.getLastAnimationFrame=function(){return this.getAnimationFrame(this.animationFrames.length-1)
};
LS_GAME_LOOP.Animation.prototype.getCurrentAnimationFrameIndex=function(){this.updateCurrentFrameIdx();
return this.currentFrameIdx
};
LS_GAME_LOOP.Animation.prototype.updateCurrentFrameIdx=function(){if(this.accumulatedTicks>=this.ticksPerFrame){var b=Math.floor(this.accumulatedTicks/this.ticksPerFrame);
this.accumulatedTicks-=b*this.ticksPerFrame;
var a=this.currentFrameIdx+b;
if(a>=this.animationFrames.length){if(this.looping==false){a=this.animationFrames.length-1;
this.ended=true
}else{a=a%this.animationFrames.length
}}this.currentFrameIdx=a
}};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.AnimationFrame=function(b,a){this.image=b;
this.drawProperties=a
};
LS_GAME_LOOP.AnimationFrame.prototype.getImage=function(){return this.image
};
LS_GAME_LOOP.AnimationFrame.prototype.getDrawProperties=function(){return this.drawProperties
};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.AnimationFrameRenderer=function(){};
LS_GAME_LOOP.AnimationFrameRenderer.prototype.draw=function(b,a,h,g){var f=g.getImage();
var e=g.getDrawProperties();
var d=e.deltaX===undefined?a:a+e.deltaX;
var c=e.deltaY===undefined?h:h+e.deltaY;
if(this.hasCropInfo(e)){b.drawImage(f,e.cropX,e.cropY,e.cropWidth,e.cropHeight,d,c,e.drawWidth,e.drawHeight)
}else{b.drawImage(f,d,c,e.drawWidth,e.drawHeight)
}};
LS_GAME_LOOP.AnimationFrameRenderer.prototype.hasCropInfo=function(a){var b=0;
if(a.cropX!==undefined){b++
}if(a.cropY!==undefined){b++
}if(a.cropWidth!==undefined){b++
}if(a.cropHeight!==undefined){b++
}if(b==0){return false
}if(b==4){return true
}throw"ImageDrawProperties must have either all or none of its cropXXX fields set"
};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.ImageDrawProperties=function(d,e,c,a,h,g,f,b){this.drawWidth=d;
this.drawHeight=e;
this.deltaX=c;
this.deltaY=a;
this.cropX=h;
this.cropY=g;
this.cropWidth=f;
this.cropHeight=b
};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.ImageLoader=function(a,b){this.imageCache=new Array();
this.erroneusLoadableImages=new Array();
this.loadableImages=a;
this.loadFinishedCallback=b;
this.onerror=function(c){return function(d){c.erroneusLoadableImages[d.id]=d
}
}(this)
};
LS_GAME_LOOP.ImageLoader.prototype.loadImages=function(){var b=this;
if(b.loadableImages.length>0){var a=b.loadableImages.pop();
var c;
if(b.loadableImages.length==0){c=b.createImage(a,b.loadFinishedCallback,b.onerror)
}else{c=b.createImage(a,function(){b.loadImages()
},b.onerror)
}b.imageCache[a.id]=c
}};
LS_GAME_LOOP.ImageLoader.prototype.getImage=function(a){return this.imageCache[a]
};
LS_GAME_LOOP.ImageLoader.prototype.getErroneusLoadableImages=function(){return this.erroneusLoadableImages
};
LS_GAME_LOOP.ImageLoader.prototype.isLoaded=function(a){return this.erroneusLoadableImages[a]===undefined
};
LS_GAME_LOOP.ImageLoader.prototype.createImage=function(b,c,a){image=new Image();
image.onload=c;
image.onerror=function(d){b.loadErrorEvent=d;
a(b);
c()
};
image.src=b.src;
return image
};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.LoadableImage=function(b,a){this.id=b;
this.src=a;
this.loadErrorEvent
};
LS_GAME_LOOP.LoadableImage.prototype.toString=function(){return"["+this.id+"] "+this.src
};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.CanvasInputEvent=function(e,c,a,b,d){this.type=e;
this.canvasX=c;
this.canvasY=a;
this.mouseButton=b;
this.key=d
};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.CanvasInputListener=function(b,a){this.canvasId=b;
this.canvas;
this.inputEventQueue=a
};
LS_GAME_LOOP.CanvasInputListener.prototype.listen=function(){this.initCanvas();
this.registerMouseMove();
this.registerMouseUp();
this.registerMouseOut();
this.registerMouseUp()
};
LS_GAME_LOOP.CanvasInputListener.prototype.initCanvas=function(){this.canvas=document.getElementById(this.canvasId);
if(this.canvas==null||this.canvas===undefined){throw"Could not get canvas for id "+this.canvasId
}};
LS_GAME_LOOP.CanvasInputListener.prototype.registerMouseMove=function(){var a=this;
this.canvas.addEventListener("mousemove",function(f){var d=a.convertXToLocalCanvas(f);
var c=a.convertYToLocalCanvas(f);
var b=new LS_GAME_LOOP.CanvasInputEvent("mousemove",d,c,f.button);
a.inputEventQueue.push(b)
})
};
LS_GAME_LOOP.CanvasInputListener.prototype.registerMouseUp=function(){var a=this;
this.canvas.addEventListener("mouseup",function(f){var d=a.convertXToLocalCanvas(f);
var c=a.convertYToLocalCanvas(f);
var b=new LS_GAME_LOOP.CanvasInputEvent("mouseup",d,c);
a.inputEventQueue.push(b)
})
};
LS_GAME_LOOP.CanvasInputListener.prototype.registerMouseOut=function(){var a=this;
this.canvas.addEventListener("mouseout",function(f){var d=a.convertXToLocalCanvas(f);
var c=a.convertYToLocalCanvas(f);
var b=new LS_GAME_LOOP.CanvasInputEvent("mouseout",d,c);
a.inputEventQueue.push(b)
})
};
LS_GAME_LOOP.CanvasInputListener.prototype.registerMouseUp=function(){var a=this;
this.canvas.addEventListener("mouseup",function(f){var d=a.convertXToLocalCanvas(f);
var c=a.convertYToLocalCanvas(f);
var b=new LS_GAME_LOOP.CanvasInputEvent("mouseup",d,c);
a.inputEventQueue.push(b)
})
};
LS_GAME_LOOP.CanvasInputListener.prototype.registerMouseDown=function(){var a=this;
this.canvas.addEventListener("mousedown",function(f){var d=a.convertXToLocalCanvas(f);
var c=a.convertYToLocalCanvas(f);
var b=new LS_GAME_LOOP.CanvasInputEvent("mousedown",d,c);
a.inputEventQueue.push(b)
})
};
LS_GAME_LOOP.CanvasInputListener.prototype.convertXToLocalCanvas=function(b){var a;
if(b.offsetX==undefined){a=b.pageX-this.canvas.offsetLeft
}else{a=b.offsetX
}if(a<0){a=0
}else{if(a>this.canvas.width){a=this.canvas.width
}}return a
};
LS_GAME_LOOP.CanvasInputListener.prototype.convertYToLocalCanvas=function(b){var a;
if(b.offsetY==undefined){a=b.pageY-this.canvas.offsetTop
}else{a=b.offsetY
}if(a<0){a=0
}else{if(a>this.canvas.height){a=this.canvas.height
}}return a
};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.InputEventQueue=function(a){this.maxSize=a;
this.queue=new Array()
};
LS_GAME_LOOP.InputEventQueue.prototype.push=function(a){if(this.queue.length>=this.maxSize){return false
}this.queue.push(a);
return true
};
LS_GAME_LOOP.InputEventQueue.prototype.popAllEvents=function(){var a=new Array();
for(var b=0;
b<this.queue.length;
b++){a[b]=this.queue.shift()
}return a
};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.PauseResumeListener=function(b,a){this.pauseResumeControl=b;
this.gameLoop=a
};
LS_GAME_LOOP.PauseResumeListener.prototype.listen=function(){this.registerClick()
};
LS_GAME_LOOP.PauseResumeListener.prototype.registerClick=function(){var a=this;
this.pauseResumeControl.addEventListener("click",function(b){a.gameLoop.togglePauseResume()
})
};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.CursorEntity=function(d,b,a,c){LS_GAME_LOOP.Entity.call(this,0,0,0,0,Number.POSITIVE_INFINITY);
this.framesById=d;
this.frameRenderer=a;
this.frameIdToSet=b;
this.currentFrame=null;
this.mouseout=false;
document.getElementById(c).style.cursor="none"
};
LS_GAME_LOOP.CursorEntity.prototype=Object.create(LS_GAME_LOOP.Entity.prototype);
LS_GAME_LOOP.CursorEntity.prototype.constructor=LS_GAME_LOOP.CursorEntity;
LS_GAME_LOOP.CursorEntity.prototype.setCurrentState=function(a){this.frameIdToSet=a
};
LS_GAME_LOOP.CursorEntity.prototype.processInput=function(a){if(a instanceof LS_GAME_LOOP.CanvasInputEvent&&a.type.startsWith("mouse")){if(a.type==="mouseout"){this.mouseout=true
}else{this.mouseout=false;
this.x=a.canvasX;
this.y=a.canvasY
}}};
LS_GAME_LOOP.CursorEntity.prototype.updateState=function(){if(this.frameIdToSet!=null){this.currentFrame=this.framesById[this.frameIdToSet];
this.frameIdToSet=null
}};
LS_GAME_LOOP.CursorEntity.prototype.updateGraphics=function(a){if(this.currentFrame!=null&&!this.mouseout){this.frameRenderer.draw(a,this.x,this.y,this.currentFrame)
}};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.GameLoopStatusEntity=function(a){LS_GAME_LOOP.Entity.call(this,10,10,182,42,Number.POSITIVE_INFINITY);
this.sampleDurationMs=a===undefined?150:a;
this.inputUpdatesCount=null;
this.stateUpdatesCount=null;
this.graphicsUpdatesCount=null;
this.inputUpdatesPerSecond=null;
this.stateUpdatesPerSecond=null;
this.graphicsUpdatesPerSecond=null;
this.updatesStartTimeMs=null
};
LS_GAME_LOOP.GameLoopStatusEntity.prototype=Object.create(LS_GAME_LOOP.Entity.prototype);
LS_GAME_LOOP.GameLoopStatusEntity.prototype.constructor=LS_GAME_LOOP.GameLoopStatusEntity;
LS_GAME_LOOP.GameLoopStatusEntity.prototype.processInput=function(a){this.inputUpdatesCount++
};
LS_GAME_LOOP.GameLoopStatusEntity.prototype.updateState=function(){this.stateUpdatesCount++;
if(this.updatesStartTimeMs==null){this.updatesStartTimeMs=new Date().getTime();
return
}var b=new Date().getTime();
var a=b-this.updatesStartTimeMs;
if(a>=this.sampleDurationMs){this.inputUpdatesPerSecond=(this.inputUpdatesCount/a)*1000;
this.stateUpdatesPerSecond=(this.stateUpdatesCount/a)*1000;
this.graphicsUpdatesPerSecond=(this.graphicsUpdatesCount/a)*1000;
this.skippedFramesPerSecond=(this.skippedFramesCount/a)*1000;
this.updatesStartTimeMs=b;
this.inputUpdatesCount=0;
this.stateUpdatesCount=0;
this.graphicsUpdatesCount=0;
this.skippedFramesCount=0
}};
LS_GAME_LOOP.GameLoopStatusEntity.prototype.updateGraphics=function(a){this.graphicsUpdatesCount++;
a.fillStyle="white";
a.fillRect(this.x,this.y,this.width,this.height);
a.font="10px Arial";
a.fillStyle="black";
if(this.stateUpdatesPerSecond!=null||this.graphicsUpdatesPerSecond!=null){a.fillText("Input updates per second: "+this.roundToFixedDecimals(this.inputUpdatesPerSecond,4),this.x,this.y+12);
a.fillText("State updates per second: "+this.roundToFixedDecimals(this.stateUpdatesPerSecond,4),this.x,this.y+24);
a.fillText("Graphics updates per second: "+this.roundToFixedDecimals(this.graphicsUpdatesPerSecond,4),this.x,this.y+36)
}else{a.fillText("Input updates per second: ???",this.x,this.y+12);
a.fillText("State updates per second: ???",this.x,this.y+24);
a.fillText("Graphics updates per second: ???",this.x,this.y+36)
}};
LS_GAME_LOOP.GameLoopStatusEntity.prototype.roundToFixedDecimals=function(b,a){var c=Math.pow(10,a);
return parseFloat(Math.round(b*c)/c).toFixed(a)
};
var LS_GAME_LOOP=LS_GAME_LOOP||{};
LS_GAME_LOOP.HardwareCursorEntity=function(c,a,b){LS_GAME_LOOP.Entity.call(this,0,0,0,0,Number.POSITIVE_INFINITY);
this.stylesById=c;
this.canvasId=b;
this.canvas=document.getElementById(this.canvasId);
this.updateCursorStyleAndRefresh(this.stylesById[a])
};
LS_GAME_LOOP.HardwareCursorEntity.prototype=Object.create(LS_GAME_LOOP.Entity.prototype);
LS_GAME_LOOP.HardwareCursorEntity.prototype.constructor=LS_GAME_LOOP.HardwareCursorEntity;
LS_GAME_LOOP.HardwareCursorEntity.prototype.setCurrentState=function(a){this.styleIdToSet=a
};
LS_GAME_LOOP.HardwareCursorEntity.prototype.processInput=function(a){};
LS_GAME_LOOP.HardwareCursorEntity.prototype.updateState=function(){if(this.styleIdToSet!=null){this.updateCursorStyleAndRefresh(this.stylesById[this.styleIdToSet]);
this.styleIdToSet=null
}};
LS_GAME_LOOP.HardwareCursorEntity.prototype.updateGraphics=function(a){};
LS_GAME_LOOP.HardwareCursorEntity.prototype.updateCursorStyleAndRefresh=function(a){this.canvas.style.cssText=a
};
