var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @returns {LS_REVERSI.ConstantsHolder}
 */
LS_REVERSI.ConstantsHolder = function() {
	
	var self = this;
	
	/**
	 * @public
	 * @type {number}
	 */
	this.boardSize = 8;
	
	/**
	 * @public
	 * 4X8 board positions bit mask, stored using x-first key
	 * (x, y) position bit mask is stored at 8*y+x array position as decimal value of the bit mask
	 * the bit values for (0, 0) position of each of the 4 storage areas (tokens/empties/uppper/lower) are stored in the 4 "center" positions of the empties
	 */
	this.positionMask = function() {
		var arr = [];
		var binaryAllZero = "";
		for(var i = 0; i < 31; i++) {
			binaryAllZero += "0";
		}
		for(var x = 0; x < 8; x++) {
			arr[x] = [];
			for(var y = 0; y < 4; y++) {	
				if(x == 0 && y == 0) {
					arr[y][x] = null;
				} else {
					var binaryStr =  binaryAllZero.substr(0, 8 * y + x - 1) + "1";
					if(8 * y + x < 31) {
						binaryStr += binaryAllZero.substr(8 * y + x);
					}
					arr[x][y] = parseInt(binaryStr, 2);
				}
			}
		}
		return arr;
	}(); 
	
	/**
	 * @public
	 * Reserved positions :
	 * - used to store token type for locations (0, 0) of uppper and lower parts of the board (aka (0,0) and (0,  4) )
	 * - kept in the empty upper storage
	 * - kept in the positions normally occupied by 2 of the central starting tokens (because those positions are never empty,
	 * so there's no need to keep track of their empty status) 
	 */
	this.emptyUpperReservedPositionMask = {
			TOKEN_TYPE_UPPER_0_0: self.positionMask[3][3],
			TOKEN_TYPE_LOWER_0_0: self.positionMask[4][3]
	};
	
	/**
	 * @public
	 * idem, but for storage of empty state of locations (0, 0) of upper and lower parts of the board (aka (0,0) and (0,  4) )
	 */
	this.emptyLowerReservedPositionMask = {
			EMPTY_UPPER_0_0: self.positionMask[3][0],
			EMPTY_LOWER_0_0: self.positionMask[4][0]
	};
	
	/**
	 * @public
	 * @type {LS_REVERSI.Position[][]}
	 */
	
	this.positionsByCoordinates = function() {
		var arr = [];
		for(var x = 0; x < 8; x++) {
			arr[x] = [];
			for(var y = 0; y < 8; y++) {
				arr[x][y] = new LS_REVERSI.Position(x, y, self.positionMask[x][y % 4]);
			}
		}
		return arr;
	}();
	
	/**
	 * @public
	 * @type {LS_REVERSI.Vector[]}
	 */
	this.vectors = [
	    new LS_REVERSI.Vector(-1, -1), //left-upper
	    new LS_REVERSI.Vector(0, -1), //upper
	    new LS_REVERSI.Vector(1, -1), //right-upper
	    new LS_REVERSI.Vector(1, 0), //right
	    new LS_REVERSI.Vector(1, 1), //right-lower
	    new LS_REVERSI.Vector(0, 1), //lower
	    new LS_REVERSI.Vector(-1, 1), //left-lower
	    new LS_REVERSI.Vector(-1, 0) //left
	                  ];
	/**
	 * @public
	 * @type {LS_REVERSI.TokenType}
	 */
	this.tokenTypeWhite = new LS_REVERSI.TokenType(2, "white");
	
	/**
	 * @public
	 * @type {LS_REVERSI.TokenType}
	 */
	this.tokenTypeBlack = new LS_REVERSI.TokenType(1, "black");
	
	/**
	 * @public
	 * @type {LS_REVERSI.TokenType}
	 */
	this.tokenTypeEmpty = new LS_REVERSI.TokenType(undefined, "empty");

	/**
	 * @public
	 */
	this.tokenTypeWhite.opponentTokenType = this.tokenTypeBlack;
	/**
	 * @public
	 */
	this.tokenTypeBlack.opponentTokenType = this.tokenTypeWhite;
	
	/**
	 * @public
	 */
	this.tokenTypeByPlayer = [null, this.tokenTypeBlack, this.tokenTypeWhite];
	
	/**
	 * @public
	 */
	this.cornerExposingByCorner = [];
	//upper-left corner:
	this.cornerExposingByCorner[0] = [];
	this.cornerExposingByCorner[0][0] = [[1, 0], [1, 1], [0, 1]];
	//lower-left corner:
	this.cornerExposingByCorner[0][7] = [[0, 6], [1, 6], [1, 7]];
	//lower-right corner:
	this.cornerExposingByCorner[7] = [];
	this.cornerExposingByCorner[7][7] = [[6, 6], [7, 6], [6, 7]];
	//upper-right corner:
	this.cornerExposingByCorner[7][0] = [[6, 0], [6, 1], [7, 1]];
	
	this.borderExposingByBorder = [];
	//left border:
	this.borderExposingByBorder[0] = [];
	this.borderExposingByBorder[0][2] = [1, 2];
	this.borderExposingByBorder[0][3] = [1, 3];
	this.borderExposingByBorder[0][4] = [1, 4];
	this.borderExposingByBorder[0][5] = [1, 5];
	//right border:
	this.borderExposingByBorder[7] = [];
	this.borderExposingByBorder[7][2] = [6, 2];
	this.borderExposingByBorder[7][3] = [6, 3];
	this.borderExposingByBorder[7][4] = [6, 4];
	this.borderExposingByBorder[7][5] = [6, 5];
	//top border:
	this.borderExposingByBorder[2] = [];
	this.borderExposingByBorder[2][0] = [2, 1];
	this.borderExposingByBorder[3] = [];
	this.borderExposingByBorder[3][0] = [3, 1];
	this.borderExposingByBorder[4] = [];
	this.borderExposingByBorder[4][0] = [4, 1];
	this.borderExposingByBorder[5] = [];
	this.borderExposingByBorder[5][0] = [5, 1];
	//bottom border:
	this.borderExposingByBorder[2][7] = [2, 6];
	this.borderExposingByBorder[3][7] = [3, 6];
	this.borderExposingByBorder[4][7] = [4, 6];
	this.borderExposingByBorder[5][7] = [5, 6];	
	
	/**
	 * @public
	 */
	this.boardCoordinatesByInterest = function() {
		var arr = new Array();
		for(var i = 0; i < 64; i++) {
			arr = new Array();
		}
		
		//corners:
		arr[0] = [0, 0];
		arr[1] = [0, 7];
		arr[2] = [7, 7];
		arr[3] = [7, 0];
		
		//corner adjacent:
		//upper-left
		arr[4] = [1, 0];
		arr[5] = [1, 1];
		arr[6] = [0, 1];
		//upper-right
		arr[7] = [6, 0];
		arr[8] = [6, 1];
		arr[9] = [7, 1];
		//lower-left
		arr[10] = [0, 6];
		arr[11] = [1, 6];
		arr[12] = [1, 7];
		//lower-right
		arr[13] = [6, 6];
		arr[14] = [6, 7];
		arr[15] = [7, 6];
		
		//border middle:
		arr[16] = [0, 2];
		arr[17] = [0, 3];
		arr[18] = [0, 4];
		arr[19] = [0, 5];
		
		arr[20] = [7, 2];
		arr[21] = [7, 3];
		arr[22] = [7, 4];
		arr[23] = [7, 5];
		
		arr[24] = [2, 0];
		arr[25] = [3, 0];
		arr[26] = [4, 0];
		arr[27] = [5, 0];
		
		arr[28] = [2, 7];
		arr[29] = [3, 7];
		arr[30] = [4, 7];
		arr[31] = [5, 7];
		
		//border middle adjacent:
		arr[32] = [1, 2];
		arr[33] = [1, 3];
		arr[34] = [1, 4];
		arr[35] = [1, 5];
		
		arr[36] = [6, 2];
		arr[37] = [6, 3];
		arr[38] = [6, 4];
		arr[39] = [6, 5];
		
		arr[40] = [2, 1];
		arr[41] = [3, 1];
		arr[42] = [4, 1];
		arr[43] = [5, 1];
		
		arr[44] = [2, 6];
		arr[45] = [3, 6];
		arr[46] = [4, 6];
		arr[47] = [5, 6];
		
		//board center:
		i = 48;
		for(var x = 2; x < 6; x++) {
			for(var y = 2; y < 6; y++) {
				arr[i] = [x, y];
				i++;
			}
		}
		
		return arr;
	}();
	
	/**
	 * @public
	 */
	this.parametrizedAIGetEvaluationDepthFunction = function(baseDepth) {
		var getEvaluationDepth = function(gameState) {
			//baseDepth should be max 6;
			if(gameState.getTokenCount() < 34) {
				return baseDepth;
			} else if(gameState.getTokenCount() < 43) {
				return baseDepth + 1;
			} else if(gameState.getTokenCount() < 50) {
				return baseDepth + 2;			
			} else {
				return baseDepth + 4;
			}
		}
		getEvaluationDepth.baseDepth = baseDepth;
		
		return getEvaluationDepth;
	};
};