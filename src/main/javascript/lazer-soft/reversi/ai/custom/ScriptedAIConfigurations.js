var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * 
 */
LS_REVERSI.SCRIPTED_AI_CONFIGURATIONS = new Array();

/**
 * 
 */
LS_REVERSI.loadCustomScriptedAIConfigurations = function() {
	LS_REVERSI.SCRIPTED_AI_CONFIGURATIONS[0] = new LS_REVERSI.AIConfiguration("Scripted - Score Matrix", 25, true, "scripted", null,
			//evaluate this intermediate game state:
			function(playerToWin, gameState, gameStateSummary) {
				var scoreMatrix = [[500, 	-75, 	28, 14, 14, 28,	-75, 	500	],
				                   [-75, 	-95, 	-6,	-4, -4,	-6, -95, 	-75	],
				                   [28, 	-6,		-1, -1,	-1,	-1, -6,		28	],
				                   [14, 	-4, 	-1, -1,	-1, -1, -4,		14	],
				                   [14, 	-4,		-1,	-1,	-1, -1, -4, 	14	],
				                   [28, 	-6,		-1,	-1, -1,	-1,	-6,		28	],
				                   [-75,	-95, 	-6,	-4, -4, -6, -95, 	-75	],
				                   [500, 	-75, 	28,	14, 14, 28, -75, 	500	]];
				
				var player1Score = 0;
				var player2Score = 0;
				
				for(var x = 0; x < 8; x++) {
					for(var y = 0; y < 8; y++) {
						var tokenType = gameState.getTokenTypeAt(x, y);
						if(tokenType.player == 1) {
							player1Score += scoreMatrix[x][y];
						} else if(tokenType.player == 2) {
							player2Score += scoreMatrix[x][y];
						}
					}
				}
				
				var score = 0;
				if(playerToWin == 1) {
					score = player1Score - player2Score;
				} else {
					score = player2Score - player1Score;
				}
				return score;
			},
			//get evaluation depth for this game state:
			function(gameState) {
				if(gameState.getTokenCount() < 40) {
					return 6;
				} if(gameState.getTokenCount() < 50) {
					return 7;
				} else {
					return 10;
				}
			}
	);
};