var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @constructor
 * @param {number} player
 * @param {string} color
 * @returns {LS_REVERSI.TokenType}
 */
LS_REVERSI.TokenType = function(player, color) {
	this.player = player;
	this.color = color;
	this.opponentTokenType;
};

/**
 * @public
 * @retuns {string}
 */
LS_REVERSI.TokenType.prototype.toString = function() {
	return "Token type: " + this.color + " (corresponding to player " + this.player + ")";
}