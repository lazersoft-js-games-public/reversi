var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @constructor
 * @param {LS_REVERSI.ReversiGameState} gameState
 * @param {LS_REVERSI.MoveHelper} moveHelper
 * @param {LS_REVERSI.ReversiConstantsHolder} constants
 * @returns {LS_REVERSI.NextMoveIterator}
 */
LS_REVERSI.NextMoveIterator = function(gameState, moveHelper, constants, interestingMovesFirst) {
	this.gameState = gameState;
	this.moveHelper = moveHelper;
	this.constants = constants;
	this.interestingMovesFirst = interestingMovesFirst === undefined ? true : interestingMovesFirst;
	
	this.player = gameState.getPlayerToMove();
	this.playerTokenType = constants.tokenTypeByPlayer[this.player];
		
	this.currentX = 0;
	this.currentY = 0;
	
	this.currentCoordsIdx = 0;
	
	this.nextMove = this.findNext();
};

/**
 * @public
 * @returns {LS_REVERSI.Move}
 */
LS_REVERSI.NextMoveIterator.prototype.next = function() {
	var nextMove = this.nextMove;
	this.nextMove = this.findNext();
	return nextMove;
};

/**
 * @public
 * @returns {Boolean}
 */
LS_REVERSI.NextMoveIterator.prototype.hasNext = function() {
	return this.nextMove != null;
};

/**
 * @private find next move
 * @returns {LS_REVERSI.Move}
 */
LS_REVERSI.NextMoveIterator.prototype.findNext = function() {
	if(this.interestingMovesFirst) {
		return this.findNextInterestingFirst();
	} else {
		return this.findNextOrderedByXY();
	}
};

/**
 * @private search board coordinates by x, y increment.
 * @returns {LS_REVERSI.Move}
 */
LS_REVERSI.NextMoveIterator.prototype.findNextOrderedByXY = function() {
	var move = null;
	outer: for(var y = this.currentY; y < 8; y++) {
		for(var x = this.currentX; x < 8; x++) {//for each board position
			if(this.gameState.getTokenTypeAt(x, y) === this.constants.tokenTypeEmpty) {//start possible-move-check from an empty position
				var flippedTokensPositions = this.moveHelper.getFlippedTokensPositions(this.constants.tokenTypeByPlayer[this.gameState.playerToMove], x, y, this.gameState);
				if(flippedTokensPositions.length > 0) {
					this.currentX = x + 1;
					this.currentY = y;
					if(this.currentX == 8) {
						this.currentX = 0;
						this.currentY = this.currentY + 1;						
					}
					move = this.moveHelper.toMove(this.player, this.constants.positionsByCoordinates[x][y], this.gameState);
					break outer;
				}
			}
		}
		this.currentX = 0;
	}	
	if(move == null) {
		this.currentX = 9;
		this.currentY = 9;
	}
	return move;
};

/**
 * @private search board coordinates by interest
 * @returns {LS_REVERSI.Move}
 */
LS_REVERSI.NextMoveIterator.prototype.findNextInterestingFirst = function() {
	var move = null;
	for(var coordsIdx = this.currentCoordsIdx; coordsIdx < this.constants.boardCoordinatesByInterest.length; coordsIdx++) {	
		var boardCoords = this.constants.boardCoordinatesByInterest[coordsIdx];
		var x = boardCoords[0];
		var y = boardCoords[1];
		if(this.gameState.getTokenTypeAt(x, y) === this.constants.tokenTypeEmpty) {//only check empty positions for possible moves
			var flippedTokensPositions = this.moveHelper.getFlippedTokensPositions(this.constants.tokenTypeByPlayer[this.gameState.playerToMove], x, y, this.gameState);
			if(flippedTokensPositions.length > 0) {
				move = this.moveHelper.toMove(this.player, this.constants.positionsByCoordinates[x][y], this.gameState);		
				this.currentCoordsIdx = coordsIdx + 1;
				break;
			}
		}
	}
	
	if(move == null) {
		this.currentCoordsIdx = this.constants.boardCoordinatesByInterest.length;
	}
	
	return move;
};