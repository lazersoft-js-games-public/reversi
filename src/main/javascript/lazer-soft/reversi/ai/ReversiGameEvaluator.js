var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @param {number} playerToWin
 * @param {Number|function} evaluationDepth Can be a number, in which case that evaluation depth will be used at any stage in the game evaluation.
 * Or it can be a function(LS_REVERSI.ReversiGameState) that returns a dinamic number for the value of the current evaluation depth.
 * @param {LS_REVERSI.ReversiConstantsHolder} constants
 * @constructor
 * @returns {LS_REVERSI.ReversiGameEvaluator}
 */
LS_REVERSI.ReversiGameEvaluator = function(playerToWin, evaluationDepth, constants, moveHelper, evaluateIntermediateState, maxNodesToEvaluate) {
	LS_AI.GameEvaluator.call(this, playerToWin);
	
	if(this.isFunction(evaluationDepth)) {
		this.getEvaluationDepth = evaluationDepth;
	} else {
		this.evaluationDepth = evaluationDepth;
	}
	this.constants = constants;
	this.moveHelper = moveHelper;
	if(evaluateIntermediateState !== undefined) {
		this.evaluateIntermediateState = evaluateIntermediateState;
	}	
	this.maxNodesToEvaluate = maxNodesToEvaluate === undefined ? 1000000 : maxNodesToEvaluate; 
};

LS_REVERSI.ReversiGameEvaluator.prototype = Object.create(LS_AI.GameEvaluator.prototype);
LS_REVERSI.ReversiGameEvaluator.prototype.constructor = LS_REVERSI.ReversiGameEvaluator;

/**
 * @public
 * @param {LS_REVERSI.GameState} gameState
 * @returns {LS_REVERSI.ReversiNextGameStateIterator}
 */
LS_REVERSI.ReversiGameEvaluator.prototype.getNextGameStateIterator = function(gameState) {
	return new LS_REVERSI.ReversiNextGameStateIterator(gameState, this.moveHelper, this.constants);
};

/**
 * @public
 * @param {LS_REVERSI.GameState} gameState
 * @returns {Number} Number.POSITIVE_INFINITY if game ended and playerToWin wins,
 *  Number.NEGATIVE_INFINITY if game ended and playerToWin loses,
 *  0 if game ended in draw.
 *  any other number to reflect how good the playerToWin's current position is (the bigger the number, the better the position)
 */
LS_REVERSI.ReversiGameEvaluator.prototype.evaluate = function(gameState) {
	if(gameState.isGameEnded()) {
		var tokenCountWhite = this.getTokenCount(this.constants.tokenTypeWhite, gameState);
		var tokenCountBlack = this.getTokenCount(this.constants.tokenTypeBlack, gameState);
		if(tokenCountWhite == tokenCountBlack) {
			return 0;
		}
		if(tokenCountWhite > tokenCountBlack) {
			if(this.playerToWin == 2) {
				return Number.POSITIVE_INFINITY;
			} else {
				return Number.NEGATIVE_INFINITY;
			}
		} else {
			if(this.playerToWin == 2) {
				return Number.NEGATIVE_INFINITY;
			} else {
				return Number.POSITIVE_INFINITY;
			}
		}
	} else {
		var gameStateSummary = this.getGameStateSummary(gameState);
		return this.evaluateIntermediateState(this.playerToWin, gameState, gameStateSummary);
	}
};

/**
 * @public
 */
LS_REVERSI.ReversiGameEvaluator.prototype.getTokenCount = function(tokenType, gameState) {
	var tokenCount = 0;
	for(var y = 0; y < this.constants.boardSize; y++) {
		for(var x = 0; x < this.constants.boardSize; x++) {
			if(gameState.getTokenTypeAt(x, y) === tokenType) {
				tokenCount++;
			}
		}
	}
	return tokenCount;
};

/**
 * @public
 */
LS_REVERSI.ReversiGameEvaluator.prototype.evaluateIntermediateState = function(playerToWin, gameState, gameStateSummary) {
	throw "Unsupported operation exception";
};

/**
 * @public
 * @param {LS_REVERSI.GameState} gameState
 * @returns {Number}
 */
LS_REVERSI.ReversiGameEvaluator.prototype.getEvaluationDepth = function(gameState) {
	return this.evaluationDepth;
};

/**
 * @public
 * @param {LS_REVERSI.GameState} gameState
 * @returns {Number}
 */
LS_REVERSI.ReversiGameEvaluator.prototype.getMaxNodesToEvaluate = function(gameState) {
	return this.maxNodesToEvaluate;
};

/**
 * @public
  
   01234567
  
 0 ABCCCCBA
 1 BBDDDDBB
 2 CDEEEEDC		
 3 CDEEEEDC
 4 CDEEEEDC
 5 CDEEEEDC
 6 BBDDDDBB
 7 ABCCCCBA
 
 */
LS_REVERSI.ReversiGameEvaluator.prototype.isCorner = function(x, y) {
	return (x == 0 && y == 0) ||
	(x == 0 && y == 7) ||
	(x == 7 && y == 7) ||
	(x == 7 && y == 0);
};

/**
 * @public
 */
LS_REVERSI.ReversiGameEvaluator.prototype.isCornerExposing = function(x, y) {
	return (x == 1 && (y == 0 || y == 1 )) || (x == 0 && y == 1) ||
			(x == 6 && (y == 0 || y == 1 )) || (x == 7 && y == 1) ||
			(x == 1 && (y == 6 || y == 7 )) || (x == 0 && y == 6) ||
			(x == 6 && (y == 6 || y == 7 )) || (x == 7 && y == 6);
};

/**
 * @public
 */
LS_REVERSI.ReversiGameEvaluator.prototype.isBorderMiddle = function(x, y) {
	return ((x == 0 || x == 7) && y >= 2 && y <= 5) ||
		((y == 0 || y == 7) && x >= 2 && x <= 5);
};

/**
 * @public
 */
LS_REVERSI.ReversiGameEvaluator.prototype.isExposingBorderMiddle = function(x, y) {
	return ((x == 1 || x == 6) && y >= 2 && y <= 5) ||
		((y == 1 || y == 6) && x >= 2 && x <= 5);
};

/**
 * @public
 */
LS_REVERSI.ReversiGameEvaluator.prototype.isBoardMiddle = function(x, y) {
	return x >= 2 && x <= 5 &&
		y >= 2 && y <= 5; 
};

/**
 * @public
 */
LS_REVERSI.ReversiGameEvaluator.prototype.toString = function() {
	var str = "\n";
	str += "ReversiGameEvaluator.evaluateIntermediateState(): ";
	str += this.evaluateIntermediateState.toString();
	str += "\n"
	str += "ReversiGameEvaluator.getEvaluationDepth(): ";
	str += 	this.getEvaluationDepth.toString();
	return str;
};

/**
 * @private
 */
LS_REVERSI.ReversiGameEvaluator.prototype.getGameStateSummary = function(gameState) {
	var gameStateSummary = {};
	gameStateSummary.takenCornersWhite = 0;
	gameStateSummary.takenCornersBlack = 0;
	gameStateSummary.cornerExposingTokensWhite = 0;
	gameStateSummary.cornerExposingTokensBlack = 0;
	gameStateSummary.takenBordersWhite = 0;
	gameStateSummary.takenBordersBlack = 0;
	gameStateSummary.borderExposingTokensWhite = 0;
	gameStateSummary.borderExposingTokensBlack = 0;
	gameStateSummary.tokenCountWhite = 0;
	gameStateSummary.tokenCountBlack = 0;
	
	for(var y = 0; y < this.constants.boardSize; y++) {
		for(var x = 0; x < this.constants.boardSize; x++) {
			if(this.isCorner(x, y)) {
				var t1 = this.constants.tokenTypeBlack;
				var t2 = gameState.getTokenTypeAt(x, y);
				//corner and corner adjacent token count:
				if(gameState.getTokenTypeAt(x, y) === this.constants.tokenTypeWhite) {
					gameStateSummary.takenCornersWhite++;
				} else if(gameState.getTokenTypeAt(x, y) === this.constants.tokenTypeBlack) {
					gameStateSummary.takenCornersBlack++;
				} else {
					var cornerExposings = this.constants.cornerExposingByCorner[x][y];
					for(var i = 0; i < cornerExposings.length; i++) {
						var cornerExposing = cornerExposings[i];
						if(gameState.getTokenTypeAt(cornerExposing[0], cornerExposing[1]) === this.constants.tokenTypeWhite) {
							gameStateSummary.cornerExposingTokensWhite++;
						} else if(gameState.getTokenTypeAt(cornerExposing[0], cornerExposing[1]) === this.constants.tokenTypeBlack) {
							gameStateSummary.cornerExposingTokensBlack++;
						}
					}
				}
			} else {
				if(this.isBorderMiddle(x, y)) {			
					//border and border adjacent token count:
					if(gameState.getTokenTypeAt(x, y) === this.constants.tokenTypeWhite) {
						gameStateSummary.takenBordersWhite++;
					} else if(gameState.getTokenTypeAt(x, y) === this.constants.tokenTypeBlack) {
						gameStateSummary.takenBordersBlack++;
					} else {
						var borderExposing = this.constants.borderExposingByBorder[x][y];
						if(gameState.getTokenTypeAt(borderExposing[0], borderExposing[1]) === this.constants.tokenTypeWhite) {
							gameStateSummary.borderExposingTokensWhite++;
						} else if(gameState.getTokenTypeAt(borderExposing[0], borderExposing[1]) === this.constants.tokenTypeBlack) {
							gameStateSummary.borderExposingTokensBlack++;
						}
					}
				}
			}
			
			//total token count:
			if(gameState.getTokenTypeAt(x, y) === this.constants.tokenTypeWhite) {
				gameStateSummary.tokenCountWhite++;
			} else if(gameState.getTokenTypeAt(x, y) === this.constants.tokenTypeBlack) {
				gameStateSummary.tokenCountBlack++;
			}
		}
	}
	
	return gameStateSummary;
};

/**
 * @private
 */
LS_REVERSI.ReversiGameEvaluator.prototype.isFunction = function(variable) {
	var getType = {};
	return variable && getType.toString.call(variable) === '[object Function]'; 
};