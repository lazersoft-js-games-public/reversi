var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @returns {LS_REVERSI.GameStateHistory}
 */
LS_REVERSI.GameStateHistory = function() {
	this.storage = new Array();
	this.currentLocation = -1;
};

/**
 * @public
 */
LS_REVERSI.GameStateHistory.prototype.advanceFromCurrent = function(nextGameState) {
	this.currentLocation++;
	this.storage[this.currentLocation] = nextGameState.clone();
	this.storage = this.storage.slice(0, this.currentLocation + 1);
};

/**
 * @public
 */
LS_REVERSI.GameStateHistory.prototype.getCurrent = function() {
	if(this.currentLocation < 0 || this.currentLocation >= this.storage.length) {
		return undefined;
	}
	return this.storage[this.currentLocation].clone();
}

/**
 * @public
 */
LS_REVERSI.GameStateHistory.prototype.goBackOne = function() {
	if(this.currentLocation <= 0) {
		return false;
	}
	this.currentLocation--;
	return true;
};

/**
 * @public
 */
LS_REVERSI.GameStateHistory.prototype.goForwardOne = function() {
	if(this.currentLocation >= this.storage.length -1) {
		return false
	}
	this.currentLocation++;
	return true;
};

/**
 * @public
 */
LS_REVERSI.GameStateHistory.prototype.getCurrentLocation = function() {
	return this.currentLocation;
};

/**
 * @public
 */
LS_REVERSI.GameStateHistory.prototype.getSize = function() {
	return this.storage.length;
};

/**
 * @public
 */
LS_REVERSI.GameStateHistory.prototype.getEntries = function() {
	var storageClone = new Array();
	for(var i = 0; i < this.storage.length; i++) {
		storageClone[i] = this.storage[i].clone();
	}
	return storageClone;
};