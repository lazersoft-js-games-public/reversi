var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @param {LS_REVERSI.ReversiGameState} currentGameState
 * @param {LS_REVERSI.MoveHelper} moveHelper
 * @param {LS_REVERSI.ReversiConstantsHolder} constants
 * @returns {LS_REVERSI.ReversiNextGameStateIterator}
 */
LS_REVERSI.ReversiNextGameStateIterator = function(currentGameState, moveHelper, constants) {
	LS_AI.NextGameStateIterator.call(this, currentGameState);
	this.nextMoveIterator = new LS_REVERSI.NextMoveIterator(currentGameState, moveHelper, constants);
};

LS_REVERSI.ReversiNextGameStateIterator.prototype = Object.create(LS_AI.NextGameStateIterator.prototype);
LS_REVERSI.ReversiNextGameStateIterator.prototype.constructor = LS_REVERSI.ReversiNextGameStateIterator;

/**
 * @public 
 * @returns {LS_REVERSI.ReversiGameState}
 */ 
LS_REVERSI.ReversiNextGameStateIterator.prototype.next = function() {
	var nextMove = this.nextMoveIterator.next();
	var nextGameState = this.currentGameState.clone();
	nextGameState.applyMove(this.currentGameState.getPlayerToMove(), nextMove);
	return nextGameState;
};

/**
 * @public
 * @returns {Boolean}
 */
LS_REVERSI.ReversiNextGameStateIterator.prototype.hasNext = function() {
	return this.nextMoveIterator.hasNext();
};