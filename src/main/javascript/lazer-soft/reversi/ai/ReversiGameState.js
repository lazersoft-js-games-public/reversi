var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @param {number} playerToMove
 * @returns {LS_REVERSI.ReversiGameState}
 */
LS_REVERSI.ReversiGameState = function(playerToMove, constants, moveHelper) {	
	LS_AI.GameState.call(this, playerToMove);
	
	this.constants = constants;
	this.moveHelper = moveHelper;
	
	this.gameEnded = false;
	
	//default starting positions (white black / black white in the 4 center squares):
	this.tokensUpper = 16;
	this.tokensLower = 134217728;
	
	//emptyPositionsUpper are all true 
	//(even the lower/center 2, as they denote 0,0 token types for upper/lower tokens):
	//except the one reserved for holding (0,0) and (0,4) tokens color, which should be set to 0 
	//(which means black if not empty - and they're never empty) 
	this.emptyPositionsUpper = 2147483623;
	
	//emptyPositionsLower are all true 
	//(even the upper/center 2, as they denote 0,0 empty positions for upper/lower emptyPositions):
	this.emptyPositionsLower = 2147483647;
	
	this.lastMove;
	
	this.tokensCount = 4;
};

LS_REVERSI.ReversiGameState.prototype = Object.create(LS_AI.GameState.prototype);
LS_REVERSI.ReversiGameState.prototype.constructor = LS_REVERSI.ReversiGameState;

/**
 * @public
 * @param {LS_REVERSI.Player} player
 * @param {LS_REVERSI.Position} move
 * 
 * Not part of the Minimax framework. 
 * To be used when applying human player action (converts "player" + (click) position to LS_REVERSI.Move)
 */
LS_REVERSI.ReversiGameState.prototype.applyMoveFromPosition = function(player, position) {
	var move = this.moveHelper.toMove(player, position, this);
	this.applyMove(player, move);
}

/**
 * @public
 * @param {LS_REVERSI.Player} player
 * @param {LS_REVERSI.Move} move
 */
LS_REVERSI.ReversiGameState.prototype.applyMove = function(player, move) {
	if(player != this.getPlayerToMove()) {
		throw "It's not player " + player +"'s turn to move";
	}
		
	this.tokensCount++;
	this.lastMove = move;
	
	var tokenType = this.constants.tokenTypeByPlayer[player];
	this.placeToken(tokenType, move.position);
	
	//flip tokens:
	for(var i = 0; i < move.flippedTokensPositions.length; i++) {
		var flippedTokenPosition = move.flippedTokensPositions[i];
		this.flipToken(tokenType, flippedTokenPosition);
	}
	
	this.updatePlayerToMoveAndGameEnded();
};

/**
 * @public
 * @returns {LS_REVERSI.Move}
 */
LS_REVERSI.ReversiGameState.prototype.getLastMove = function() {
	return this.lastMove;
};

/**
 * @public
 * @returns {Boolean}
 */
LS_REVERSI.ReversiGameState.prototype.isGameEnded = function() {
	return this.gameEnded;
};

/**
 * @public
 * @returns {REVERSI.ReversiGameState}
 */
LS_REVERSI.ReversiGameState.prototype.clone = function(/* playerToMove */) {
	var clonePlayerToMove = arguments.length == 1 ? arguments[0] : this.playerToMove;
	var clone = new LS_REVERSI.ReversiGameState(clonePlayerToMove, this.constants, this.moveHelper);
	clone.gameEnded = this.gameEnded;
	clone.tokensUpper = this.tokensUpper;
	clone.tokensLower = this.tokensLower;
	clone.emptyPositionsUpper = this.emptyPositionsUpper;
	clone.emptyPositionsLower = this.emptyPositionsLower;
	clone.lastMove = this.lastMove;
	clone.tokensCount = this.tokensCount;
	clone.setMinimaxRunReport(this.getMinimaxRunReport());
	return clone;
};

/**
 * @public
 * @param {Number} x
 * @param {Number} y
 * @returns {LS_REVERSI.TokenType}
 */
LS_REVERSI.ReversiGameState.prototype.getTokenTypeAt = function(x, y) {
	var position = this.constants.positionsByCoordinates[x][y];
	var isLowerBoardHalf = position.isLowerBoardHalf;
	
	if(position.mask == null) {//is one of the reserved positions
		
		var mask = this.constants.emptyLowerReservedPositionMask.EMPTY_UPPER_0_0;
		if(isLowerBoardHalf) {
			mask = this.constants.emptyLowerReservedPositionMask.EMPTY_LOWER_0_0;
		}
		
		//check if empty (emptyPositions has a 1 there):
		if((this.emptyPositionsLower & mask) != 0) {
			return this.constants.tokenTypeEmpty;
		}
		
		var mask = this.constants.emptyUpperReservedPositionMask.TOKEN_TYPE_UPPER_0_0;
		if(isLowerBoardHalf) {
			mask = this.constants.emptyUpperReservedPositionMask.TOKEN_TYPE_LOWER_0_0;
		}
		
		//get token type (1=white, 0=black):
		if((this.emptyPositionsUpper & mask) != 0) {
			return this.constants.tokenTypeWhite;
		} else {
			return this.constants.tokenTypeBlack;
		}	
	} else {//is not a reserved position
		
		var mask = position.mask;
		
		if(position.canBeEmpty) {
			
			var emptyPositions = this.emptyPositionsUpper;
			if(isLowerBoardHalf) {
				emptyPositions = this.emptyPositionsLower;
			}
			
			if((emptyPositions & mask) != 0) {
				return this.constants.tokenTypeEmpty;
			}
		}
		
		var tokens = this.tokensUpper;
		if(isLowerBoardHalf) {
			tokens = this.tokensLower;
		}
		
		//get token type (1=white, 0=black):
		if((tokens & mask) != 0) {
			return this.constants.tokenTypeWhite;
		} else {
			return this.constants.tokenTypeBlack;
		}	
	}
};

/**
 * @public
 * @returns {number}
 */
LS_REVERSI.ReversiGameState.prototype.getTokenCount = function() {
	return this.tokensCount;
};

/**
 * @public
 * @param {String} [endLineSeparator]
 * @returns {String}
 */
LS_REVERSI.ReversiGameState.prototype.toString = function(endLineSeparator) {
	if(arguments.length == 0) {
		endLineSeparator = "\n";
	}
	var str = "To move: #" + this.playerToMove + 
		", Last move: " + (this.getLastMove() === undefined ? "-none- " : this.getLastMove().toString()) +
		endLineSeparator;
	str += this.toStringBoardOnly(endLineSeparator);
	return str;
};

/**
 * @public
 * @param {String} [endLineSeparator]
 * @returns {String}
 */
LS_REVERSI.ReversiGameState.prototype.toStringBoardOnly = function(endLineSeparator) {
	if(arguments.length == 0) {
		endLineSeparator = "\n";
	}
	var str = "";
	for(var y = 0; y < 8; y++) {
		for(var x = 0; x < 8; x++) {			
			if(this.getTokenTypeAt(x, y) === this.constants.tokenTypeWhite) {
				str += "O";
			} else if(this.getTokenTypeAt(x, y) === this.constants.tokenTypeBlack) {
				str += "*";
			} else if(this.getTokenTypeAt(x, y) === this.constants.tokenTypeEmpty) {
				str += "_";
			}
		}
		str += endLineSeparator;
	}
	return str;
};

/**
 * @public
 * for test/debug only
 */
LS_REVERSI.ReversiGameState.prototype.forcePlayerToMove = function(player) {
	this.playerToMove = player;
};

/**
 * @public
 * for test/debug only
 */
LS_REVERSI.ReversiGameState.prototype.forceLastMove = function(move) {
	this.lastMove = move;
};

/**
 * @public
 * for test/debug only
 */
LS_REVERSI.ReversiGameState.prototype.forceTokenAt = function(tokenType, x, y) {
	if(tokenType === this.constants.tokenTypeEmpty &&
			(x == 3 || x == 4) && (y == 3 && y == 4)) {
		throw "Can't place empty position in one of the 4 center squares (" + x + " ," + y + ")";
	}
	
	if(this.getTokenTypeAt(x, y) === tokenType) {
		//it's already set
		return;
	}
	
	if(tokenType === this.constants.tokenTypeEmpty) {
		var position = this.constants.positionsByCoordinates[x][y];
		if(position.isLowerBoardHalf) {
			if(position.mask == null) {//if move is on reserved position
				//flip position to empty (from 1 to 0) 
				this.emptyPositionsLower = this.emptyPositionsLower ^ this.constants.emptyLowerReservedPositionMask.EMPTY_LOWER_0_0;
			} else {
				//flip position to empty (from 1 to 0) 
				this.emptyPositionsLower = this.emptyPositionsLower ^ position.mask;
			}
		} else {
			if(position.mask == null) {//if move is on reserved position
				//flip position to empty (from 1 to 0)
				this.emptyPositionsLower = this.emptyPositionsLower ^ this.constants.emptyLowerReservedPositionMask.EMPTY_UPPER_0_0;
			} else {
				//flip position to empty (from 1 to 0)
				this.emptyPositionsUpper = this.emptyPositionsUpper ^ position.mask;
			}
		}
	} else {
		var existingTokenType = this.getTokenTypeAt(x, y);
		if(existingTokenType !== this.constants.tokenTypeEmpty && existingTokenType !== tokenType) {
			this.flipToken(tokenType, this.constants.positionsByCoordinates[x][y]);
		} else {
			this.placeToken(tokenType, this.constants.positionsByCoordinates[x][y]);
		}
	}
	
	this.updateTokensCount();
};

/**
 * @public
 * for test/debug only
 */
LS_REVERSI.ReversiGameState.prototype.forceEmptyTokenAt = function(x, y) {
	this.forceTokenAt(this.constants.tokenTypeEmpty, x, y);
};

/**
 * @public
 * for test/debug only
 */
LS_REVERSI.ReversiGameState.prototype.forceAllTokens = function(tokenType) {
	for(var x = 0; x < this.constants.boardSize; x++) {
		for(var y = 0; y < this.constants.boardSize; y++) {
			this.forceTokenAt(tokenType, x, y);
		}
	}
};

/**
 * @public
 * for test/debug only
 */
LS_REVERSI.ReversiGameState.prototype.forceAllTokensWhite = function() {
	this.forceAllTokens(this.constants.tokenTypeWhite);
};

/**
 * @public
 * for test/debug only
 */
LS_REVERSI.ReversiGameState.prototype.forceAllTokensBlack = function() {
	this.forceAllTokens(this.constants.tokenTypeBlack);
};

/**
* @private
* only to be used for placing actual (black or white) token.
* NOT for setting position to empty.
*/
LS_REVERSI.ReversiGameState.prototype.placeToken = function(tokenType, position) {
	var player = 1;
	if(tokenType === this.constants.tokenTypeWhite) {
		player = 2;
	}
	if(position.isLowerBoardHalf) {
		if(position.mask == null) {//if move is on reserved position
			if(player == 2) {
				//if player 2, set token to white (1):
				this.emptyPositionsUpper = this.emptyPositionsUpper | this.constants.emptyUpperReservedPositionMask.TOKEN_TYPE_LOWER_0_0;				
			}//otherwise leave it at 0 for black
					
			//mark position as no longer empty (flip from 1 to 0), 
			this.emptyPositionsLower = this.emptyPositionsLower ^ this.constants.emptyLowerReservedPositionMask.EMPTY_LOWER_0_0;
		} else {
			if(player == 2) {
				//if player 2, set token to white (1):
				this.tokensLower = this.tokensLower | position.mask;				
			}//otherwise leave it at 0 for black
					
			//mark position as no longer empty (flip from 1 to 0), 
			this.emptyPositionsLower = this.emptyPositionsLower ^ position.mask;
		}
	} else {
		if(position.mask == null) {//if move is on reserved position
			if(player == 2) {
				//if player 2, set token to white (1):
				this.emptyPositionsUpper = this.emptyPositionsUpper | this.constants.emptyUpperReservedPositionMask.TOKEN_TYPE_UPPER_0_0;				
			}//otherwise leave it at 0 for black
					
			//mark position as no longer empty (flip from 1 to 0), 
			this.emptyPositionsLower = this.emptyPositionsLower ^ this.constants.emptyLowerReservedPositionMask.EMPTY_UPPER_0_0;
		} else {
			if(player == 2) {
				//if player 2, set token to white (1):
				this.tokensUpper = this.tokensUpper | position.mask;				
			}//otherwise leave it at 0 for black
			
			//mark position as no longer empty (flip from 1 to 0), 
			this.emptyPositionsUpper = this.emptyPositionsUpper ^ position.mask;
		}
	}
};

/**
 * @private
 */
LS_REVERSI.ReversiGameState.prototype.flipToken = function(tokenType, flippedTokenPosition) {
	if(flippedTokenPosition.mask == null) {
		var mask = this.constants.emptyUpperReservedPositionMask.TOKEN_TYPE_UPPER_0_0;
		if(flippedTokenPosition.isLowerBoardHalf) {
			mask = this.constants.emptyUpperReservedPositionMask.TOKEN_TYPE_LOWER_0_0;			
		}			
		//flip token:
		this.emptyPositionsUpper = this.emptyPositionsUpper ^ mask;
	} else {
		//flip token:
		if(flippedTokenPosition.isLowerBoardHalf) {
			this.tokensLower = this.tokensLower ^ flippedTokenPosition.mask;
		} else {
			this.tokensUpper = this.tokensUpper ^ flippedTokenPosition.mask;
		}
	}
};

/**
 * @private
 * @returns {Boolean}
 */
LS_REVERSI.ReversiGameState.prototype.opponentHasNextMove = function() {
	var clone = this.clone(this.getOpponent(this.playerToMove));
	var nextMoveIterator = new LS_REVERSI.NextMoveIterator(clone, this.moveHelper, this.constants);
	return nextMoveIterator.hasNext();
};

/**
 * @private
 * @returns {Boolean}
 */
LS_REVERSI.ReversiGameState.prototype.playerHasNextMove = function() {
	return new LS_REVERSI.NextMoveIterator(this, this.moveHelper, this.constants).hasNext();
};

/**
 * @private
 * @returns {Boolean}
 */
LS_REVERSI.ReversiGameState.prototype.getOpponent = function(player) {
	return 3 - player;
};

/**
 * @private
 */
LS_REVERSI.ReversiGameState.prototype.updatePlayerToMoveAndGameEnded = function() {
	if(this.opponentHasNextMove()) {
		this.playerToMove = this.getOpponent(this.playerToMove);
	} else {
		if(!this.playerHasNextMove()) {
			this.playerToMove = undefined;
			this.gameEnded = true;
		} /* else {
			this.playerToMove doesn't change, and game hasn't ended
		} */
	}
};

/**
 * @private
 * for test/debug only.
 * This should only be used in conjuction with forceXXX functions
 * The standard way to change game state, via applyMove token, will update organically the token count
 */
LS_REVERSI.ReversiGameState.prototype.updateTokensCount = function() {
	var curTokenCount = 0;
	for(var x = 0; x < this.constants.boardSize; x++) {
		for(var y = 0; y < this.constants.boardSize; y++) {
			if(this.getTokenTypeAt(x, y) !== this.constants.tokenTypeEmpty) {
				curTokenCount++;
			}
		}
	}
	this.tokensCount = curTokenCount;
};