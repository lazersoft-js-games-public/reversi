var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @constructor
 * @param {Number} playerThatHasMoved
 * @param {LS_REVERSI.Position} position
 * @param {LS_REVERSI.Position[]} flippedTokensPositions
 * @returns {LS_REVERSI.Move}
 */
LS_REVERSI.Move = function(playerThatHasMoved, position, flippedTokensPositions) {
	/**
	 * @public
	 * @type {number}
	 */
	this.player = playerThatHasMoved;
	
	/**
	 * @public
	 * @type {LS_REVERSI.Position}
	 */
	this.position = position;
	
	/**
	 * @public
	 * @type {LS_REVERSI.Position[]}
	 */
	this.flippedTokensPositions = flippedTokensPositions;
};

/**
 * @public
 * @returns {String}
 */
LS_REVERSI.Move.prototype.toString = function() {
	return "(" + this.position.x + ", " +  this.position.y + ") by " + this.player;
};