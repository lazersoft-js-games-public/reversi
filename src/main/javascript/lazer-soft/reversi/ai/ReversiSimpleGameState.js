var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.ReversiSimpleGameState}
 */
LS_REVERSI.ReversiSimpleGameState = function(playerToMove, lastMove, simplifiedTokens) {	
	this.playerToMove = playerToMove;
	this.lastMove = lastMove;
	this.simplifiedTokens = simplifiedTokens
};

/**
 * 
 */
LS_REVERSI.ReversiSimpleGameState.prototype.toReversiGameState = function(constants, moveHelper) {
	var gameState = new LS_REVERSI.ReversiGameState(1, constants, moveHelper);
	for(var x = 0; x < constants.boardSize; x++) {
		for(var y = 0; y < constants.boardSize; y++) {
			var tokenChar = this.simplifiedTokens[y][x];
			if(tokenChar === 'o' || tokenChar === 'O') {
				gameState.forceTokenAt(constants.tokenTypeWhite, x, y);
			} else if(tokenChar === '*' || tokenChar === 'X') {
				gameState.forceTokenAt(constants.tokenTypeBlack, x, y);
			}
		}
	}
	gameState.forcePlayerToMove(this.playerToMove);
	gameState.forceLastMove(this.lastMove);
	return gameState.clone();
};