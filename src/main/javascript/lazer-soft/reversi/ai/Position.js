var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.Position}
 */
LS_REVERSI.Position = function(x, y, positionMask) {
	this.x = x;
	this.y = y;
	this.mask = positionMask;
	this.isLowerBoardHalf = (y >= 4);
	this.canBeEmpty = !((x == 3 || x == 4) && (y == 3 || y == 4));
};

/**
 * @public
 * @param {String} endLineSeparator
 * @returns {String}
 */
LS_REVERSI.Position.prototype.toString = function(endLineSeparator) {
	if(arguments.length == 0) {
		endLineSeparator = "\n";
	}
	var head = "(" + this.x + ", " + this.y + ") Can be empty: " + this.canBeEmpty; 
	var str = "";
	if(this.mask == null) {
		str = (this.isLowerBoardHalf ? "lower" : "upper") + " Reserved position" ;
	} else {
		var maskStr = this.mask.toString(2);
	
		for(var i = 0; i < 32 - maskStr.length; i++) {
			str += "0";
			if((i + 1) % 8 == 0) {
				str += endLineSeparator;
			}
		}
		for(var i = 32 - maskStr.length; i < 32; i++) {
			str += maskStr.charAt(i - (32 - maskStr.length));
			if((i + 1) % 8 == 0) {
				str += endLineSeparator;
			}
		}
		
		var padding = "-PADDING" + endLineSeparator +
		"00000000" + endLineSeparator + 
		"00000000" + endLineSeparator + 
		"PADDING-";
		
		if(this.isLowerBoardHalf) {
			str = padding  + endLineSeparator + str;
		} else {
			str = str + padding + endLineSeparator;
		}
	}
	
	return head + endLineSeparator + str;
};