var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.MoveHelper}
 */
LS_REVERSI.MoveHelper = function(constants) {
	this.constants = constants;
};

/**
 * @public
 * @returns {LS_REVERSI.Position[]}
 */
LS_REVERSI.MoveHelper.prototype.getFlippedTokensPositions = function(tokenType, x, y, gameState) {
	var flippedTokensPositions = [];
	vectorInnerLoop: for(var i = 0; i < 8; i++) {//get all the vectors around it
		var vector = this.constants.vectors[i];
		var xNeighbor = x + vector.x;//use vector.x to get xNeighbor
		if(xNeighbor >= 0 && xNeighbor < 8) {//make sure xNeighbor is not outside the board
			var yNeighbor = y + vector.y;//use vector.y to get yNeighbor
			if(yNeighbor >= 0 && yNeighbor < 8) {//make sure yNeighbor is not outside the board
				if(gameState.getTokenTypeAt(xNeighbor, yNeighbor) === tokenType.opponentTokenType) { //make sure neighboring token is of opposing color
					var potentialFlippedTokensPositions = [];
					potentialFlippedTokensPositions.push(new LS_REVERSI.Position(xNeighbor, yNeighbor, this.constants.positionMask[xNeighbor][yNeighbor % 4])); //add the neighbor to (possibly) flipped tokens
					var xCurrent = xNeighbor + vector.x; 
					var yCurrent = yNeighbor + vector.y; //advance to next vectorial position
					while(xCurrent >=0 && xCurrent < 8 &&
							yCurrent >= 0 && yCurrent < 8) {
						if(gameState.getTokenTypeAt(xCurrent, yCurrent) === this.constants.tokenTypeEmpty) { //if you stumble upon an empty position, mark this as a new move
							continue vectorInnerLoop; //we're done with possible-move-checking on this vector
						} else if(gameState.getTokenTypeAt(xCurrent, yCurrent) === tokenType.opponentTokenType) { //if you stumble upon opponent's token, add it to flipped positions
							potentialFlippedTokensPositions.push(new LS_REVERSI.Position(xCurrent, yCurrent, this.constants.positionMask[xCurrent][yCurrent % 4]));	
							xCurrent = xCurrent + vector.x;
							yCurrent = yCurrent + vector.y;
						} else {//if we stumble upon a player's token
							for(var j = 0; j < potentialFlippedTokensPositions.length; j++) {
								flippedTokensPositions.push(potentialFlippedTokensPositions[j]);
							}
							continue vectorInnerLoop; //we're done with possible-move-checking on this vector
						}
					}
				}
			}
		}
	}
	return flippedTokensPositions;
};

/**
 * @public
 * @returns {LS_REVERSI.Move}
 */
LS_REVERSI.MoveHelper.prototype.toMove = function(player, position, gameState) {	
	var flippedTokensPositions = this.getFlippedTokensPositions(this.constants.tokenTypeByPlayer[player], position.x, position.y, gameState);
	if(flippedTokensPositions.length == 0) {
		throw "Can't make a move that doesn't flip any tokens (move at (" + position.x + ", " + position.y + " for player " + player + ") flips 0 tokens)";
	}
	return new LS_REVERSI.Move(player, position, flippedTokensPositions);
};