var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @param {number} x
 * @param {number} y
 * @returns {LS_REVERSI.Vector}
 */
LS_REVERSI.Vector = function(x, y) {
	/**
	 * @public
	 * @const
	 * @type {number}
	 */
	this.x = x;
	
	/**
	 * @public
	 * @const
	 * @type {number}
	 */
	this.y = y;
};