var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.AIConfigurationImporter}
 */
LS_REVERSI.AIConfigurationImporter = function(aiConfigurationFileLocation, aiConfigurationLocalStorageManager, configurationLoadedCallback) {
	this.aiConfigurationLocalStorageManager = aiConfigurationLocalStorageManager;
	this.aiConfigurationFileLocation = aiConfigurationFileLocation;
	this.configurationLoadedCallback = configurationLoadedCallback;
};

/**
 * @public
 */
LS_REVERSI.AIConfigurationImporter.prototype.importAIConfigurations = function(/* aiConfigurationFileLocation, configurationLoadedCallback */) {
	var aiConfFileLoc = this.aiConfigurationFileLocation;
	var confLoadedCbk = this.configurationLoadedCallback;
	
	if(arguments.length == 1) {
		aiConfFileLoc = arguments[0];
	}
	if(arguments.length == 2) {
		aiConfFileLoc = arguments[0];
		confLoadedCbk = arguments[1];
	}
	
	var self = this;
	
	if(aiConfFileLoc !== "localStorage") {
		var client = new XMLHttpRequest();
		client.open("GET", aiConfFileLoc);		
		client.onreadystatechange = function(fileContent) {		
			if (client.readyState == 4 && client.status == 200) {
	            var aiConfigurations = JSON.parse(client.responseText);
	            if(aiConfigurations !== undefined) {
	            	aiConfigurations.sort(function(aiConfig1, aiConfig2) {
	            		return aiConfig1.displayOrder - aiConfig2.displayOrder;
	            	});
	            }
	            confLoadedCbk(aiConfigurations);
	        }
			//TODO handle errors
		}
		client.send();
	} else {
		var aiConfigurations = self.aiConfigurationLocalStorageManager.getAll();
		var filteredAIConfigurations = new Array();
		var j = 0;
		for(var i = 0; i < aiConfigurations.length; i++) {
			var aiConfiguration = aiConfigurations[i];
			if(aiConfiguration.exportable) {
				filteredAIConfigurations[j++] = aiConfiguration;
			}
		}
		filteredAIConfigurations.sort(function(aiConfig1, aiConfig2) {
    		return aiConfig1.displayOrder - aiConfig2.displayOrder;
    	});
		confLoadedCbk(filteredAIConfigurations);
	}
};
