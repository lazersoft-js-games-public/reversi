var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @param {String} type can be either "parameterized" or "scripted"
 * @returns {LS_REVERSI.AIConfigurationLocalStorageManager}
 */
LS_REVERSI.AIConfigurationLocalStorageManager = function(storageManager, constants) {
	this.storageManager = storageManager;
	this.constants = constants;
};

/**
 * @public
 */
LS_REVERSI.AIConfigurationLocalStorageManager.prototype.store = function(aiConfiguration) {
	this.storageManager.store(aiConfiguration.name, this.serialize(aiConfiguration));
};

/**
 * @public
 */
LS_REVERSI.AIConfigurationLocalStorageManager.prototype.get = function(name) {
	return this.deserialize(this.storageManager.get(name));
};

/**
 * @public
 */
LS_REVERSI.AIConfigurationLocalStorageManager.prototype.getAll = function() {
	var aiConfigurations = new Array();
	var configurationNames = this.listConfigurationNames();
	for(var i = 0; i < configurationNames.length; i++) {
		aiConfigurations[i] = this.get(configurationNames[i]);
	}
	return aiConfigurations;
};

/**
 * @public
 */
LS_REVERSI.AIConfigurationLocalStorageManager.prototype.listConfigurationNames = function() {
	return this.storageManager.listKeys();
};

/**
 * @public
 */
LS_REVERSI.AIConfigurationLocalStorageManager.prototype.remove = function(name) {
	this.storageManager.remove(name);
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationLocalStorageManager.prototype.toString = function(aiConfiguration) {
	var str = aiConfiguration.displayOrder + ". " + aiConfiguration.name + "[" + aiConfiguration.type + "] (exportable: " + aiConfiguration.exportable + ")" ;
	str += "\n";
	if(aiConfiguration.type === "parameterized") {
		str += "Parameters:\n";
		str += " - evaluation depth: " + aiConfiguration.parameters.evaluationDepth + "\n";
		str += " - taken corner: " + aiConfiguration.parameters.tokenScore.takenCorner + "\n";
		str += " - corner exposing: " + aiConfiguration.parameters.tokenScore.cornerExposing + "\n";
		str += " - taken border: " + aiConfiguration.parameters.tokenScore.takenBorder + "\n";
		str += " - border exposing: " + aiConfiguration.parameters.tokenScore.borderExposing + "\n";
		str += " - count: " + aiConfiguration.parameters.tokenScore.count;
	} else {
		str += "Evaluation function:\n";
		str += aiConfiguration.evaluateIntermediateState + "\n";
		str += "Depth function:\n";
		str += aiConfiguration.getEvaluationDepth
	}
	return str;
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationLocalStorageManager.prototype.serialize = function(aiConfiguration /*, indenationSpaces */) {
	var indentationSpaces = 0;
	if(arguments.length == 2) {
		indentationSpaces = arguments[1];
	}
	
	if(aiConfiguration.type === "parameterized") {
		return JSON.stringify(aiConfiguration, null, indentationSpaces);
	} else if(aiConfiguration.type === "scripted") {
		var serializable = {};
		serializable.name = aiConfiguration.name;
		serializable.displayOrder = aiConfiguration.displayOrder;
		serializable.exportable = aiConfiguration.exportable;		
		serializable.type = aiConfiguration.type;
		serializable.evaluateIntermediateState = aiConfiguration.evaluateIntermediateState.toString(indentationSpaces);
		serializable.getEvaluationDepth = aiConfiguration.getEvaluationDepth.toString(indentationSpaces);
		return JSON.stringify(serializable, indentationSpaces);
	} else {
		this.throwUnknowTypeException(aiConfiguration);
	}
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationLocalStorageManager.prototype.deserialize = function(serializedAIConfiguration) {
	var aiConfig = JSON.parse(serializedAIConfiguration);
	if(aiConfig.type === "parameterized") {
		return aiConfig;
	} else if(aiConfig.type === "scripted") {
		aiConfig.evaluateIntermediateState = this.stringToFunction(aiConfig.evaluateIntermediateState);
		aiConfig.getEvaluationDepth = this.stringToFunction(aiConfig.getEvaluationDepth);
	} else {
		this.throwUnknowTypeException(aiConfig);
	}
	return aiConfig;
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationLocalStorageManager.prototype.stringToFunction = function(functionString) {
	return eval("(" + functionString + ")");
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationLocalStorageManager.prototype.generateEvaluateIntermediateState = function(aiConfiguration) {
	if(aiConfiguration.type === "parameterized") {
		var evaluateIntermediateState = function(playerToWin, gameState, gameStateSummary) {
			var takenCornerScore = aiConfiguration.parameters.tokenScore.takenCorner;
    		var cornerExposingTokenScore = aiConfiguration.parameters.tokenScore.cornerExposing;
    		var takenBorderScore = aiConfiguration.parameters.tokenScore.takenBorder;
    		var borderExposingTokenScore = aiConfiguration.parameters.tokenScore.borderExposing;
    		var countScore = aiConfiguration.parameters.tokenScore.count;
    		
    		var playerToWin = playerToWin;
    		
    		var whiteScore = (takenCornerScore * gameStateSummary.takenCornersWhite) 
	    		+  (cornerExposingTokenScore * gameStateSummary.cornerExposingTokensWhite) 
	    		+ (takenBorderScore * gameStateSummary.takenBordersWhite) 
	    		+ (borderExposingTokenScore * gameStateSummary.borderExposingTokensWhite)
	    		+ (countScore * gameStateSummary.tokenCountWhite);
    		var blackScore = (takenCornerScore * gameStateSummary.takenCornersBlack) 
	    		+  (cornerExposingTokenScore * gameStateSummary.cornerExposingTokensBlack) 
	    		+ (takenBorderScore * gameStateSummary.takenBordersBlack) 
	    		+ (borderExposingTokenScore * gameStateSummary.borderExposingTokensBlack)
	    		+ (countScore * gameStateSummary.tokenCountBlack);
    		if(playerToWin == 2) {
    			return whiteScore - blackScore; 
    		} else {
    			return blackScore - whiteScore;
    		}
		};
		return evaluateIntermediateState;
	} else if(aiConfiguration.type === "scripted") {
		return aiConfiguration.evaluateIntermediateState;
	} else {
		this.throwUnknowTypeException(aiConfiguration);
	}
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationLocalStorageManager.prototype.generateGetEvaluationDepth = function(aiConfiguration) {
	if(aiConfiguration.type === "parameterized") {
		return this.constants.parametrizedAIGetEvaluationDepthFunction(aiConfiguration.parameters.evaluationDepth);
	} else if(aiConfiguration.type === "scripted") {
		return aiConfiguration.getEvaluationDepth;
	} else {
		this.throwUnknowTypeException(aiConfiguration);
	}
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationLocalStorageManager.prototype.throwUnknowTypeException = function(aiConfiguration) {
	throw "No (de)serialization for LS_REVERSI.AIConfiguration of type: " + aiConfiguration.type;
};