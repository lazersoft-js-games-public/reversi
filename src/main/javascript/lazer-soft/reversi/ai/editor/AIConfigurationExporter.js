var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.AIConfigurationExporter}
 */
LS_REVERSI.AIConfigurationExporter = function(aiConfigurationLocalStorageManager) {
	this.aiConfigurationLocalStorageManager = aiConfigurationLocalStorageManager;
};

/**
 * @public
 */
LS_REVERSI.AIConfigurationExporter.prototype.exportAIConfigurations = function() {
	var aiConfigurations = this.aiConfigurationLocalStorageManager.getAll();
	
	var aiConfigurationsStr = "[";
	
	for(var i = 0; i < aiConfigurations.length; i++) {
		var aiConfiguration = aiConfigurations[i];
		if(aiConfiguration.exportable) {
			aiConfigurationsStr += this.aiConfigurationLocalStorageManager.serialize(aiConfiguration, 4);
			if(i != aiConfigurations.length - 1 && aiConfigurations.length != 1) {
				aiConfigurationsStr += ", \n";
			}
		}
	}
		
	aiConfigurationsStr += "]";
	
	this.downloadFile("ai-configurations.json", aiConfigurationsStr);
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationExporter.prototype.downloadFile = function(filename, data) {
	var blob = new Blob([data], {type: 'application/json'});
    if(window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, filename);
    }
    else{
        var elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = filename;        
        document.body.appendChild(elem);
        elem.click();        
        document.body.removeChild(elem);
    }
};
