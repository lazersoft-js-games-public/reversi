var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.AIEditorController}
 */
LS_REVERSI.AIEditorController = function(parameterizedAIEditorInputListener1, 
		parameterizedAIEditorInputListener2, 
		commonControlsAIEditorInputListener1,
		commonControlsAIEditorInputListener2,
		aiConfigurationListInputListener1,
		aiConfigurationListInputListener2,
		aiConfigurationLocalStorageManager, 
		aiConfigurationToReversiGameEvaluatorConverter,
		cpuVsCpuRunner,
		constants, moveHelper,
		retroConsole) {
	
	this.parameterizedAIEditorInputListeners = new Array();
	this.parameterizedAIEditorInputListeners[1] = parameterizedAIEditorInputListener1;
	this.parameterizedAIEditorInputListeners[2] = parameterizedAIEditorInputListener2;
	
	this.commonControlsAIEditorInputListeners = new Array();
	this.commonControlsAIEditorInputListeners[1] = commonControlsAIEditorInputListener1;
	this.commonControlsAIEditorInputListeners[2] = commonControlsAIEditorInputListener2;
	
	this.aiConfigurationListInputListeners = new Array();
	this.aiConfigurationListInputListeners[1] = aiConfigurationListInputListener1;
	this.aiConfigurationListInputListeners[2] = aiConfigurationListInputListener2;
	
	this.aiConfigurationLocalStorageManager = aiConfigurationLocalStorageManager;
	this.aiConfigurationToReversiGameEvaluatorConverter = aiConfigurationToReversiGameEvaluatorConverter;
	this.cpuVsCpuRunner = cpuVsCpuRunner;
	this.constants = constants;
	this.moveHelper = moveHelper;
	this.retroConsole = retroConsole;
	
	this.aiConfigurations = new Array();
};

/**
 * @public
 */
LS_REVERSI.AIEditorController.prototype.startControlling = function() {
	this.listenForInput();
	this.initAIConfiguration();
};


/**
 * @private
 */
LS_REVERSI.AIEditorController.prototype.updateAIConfiguration = function(player, aiConfiguration) {
	this.parameterizedAIEditorInputListeners[player].setAIConfiguration(aiConfiguration);
	this.commonControlsAIEditorInputListeners[player].setAIConfiguration(aiConfiguration);
	this.aiConfigurations[player] = aiConfiguration;
};

/**
 * @private
 */
LS_REVERSI.AIEditorController.prototype.onSave = function(player) {
	this.commonControlsAIEditorInputListeners[player].setSaveButtonWarning(false);
	var aiConfiguration = this.aiConfigurations[player];
	this.aiConfigurationLocalStorageManager.store(aiConfiguration);
	this.aiConfigurationListInputListeners[1].listen(this);
	this.aiConfigurationListInputListeners[2].listen(this);
	this.retroConsole.addTextLine("Stored A.I. Configuration:");
	this.retroConsole.addTextLine("    - named: " + aiConfiguration.name);
	this.retroConsole.addTextLine("    - from panel of player: #" + player);
	this.retroConsole.addTextLine("    - containing: " + this.getAIConfigurationDescription(aiConfiguration));
};

/**
 * @private
 */
LS_REVERSI.AIEditorController.prototype.onSet = function(player) {
	this.commonControlsAIEditorInputListeners[player].setSetButtonWarning(false);
	this.setGameEvaluator(player);
};

/**
 * @private
 */
LS_REVERSI.AIEditorController.prototype.onLoad = function(player, aiConfigurationName) {
	var aiConfiguration = this.aiConfigurationLocalStorageManager.get(aiConfigurationName);
	this.retroConsole.addTextLine("Loaded A.I. Configuration:");
	this.retroConsole.addTextLine("    - named: " + aiConfiguration.name);
	this.retroConsole.addTextLine("    - for player: #" + player);
	this.retroConsole.addTextLine("    - containing: " + this.getAIConfigurationDescription(aiConfiguration));
	this.updateAIConfiguration(player, aiConfiguration);
	this.setGameEvaluator(player);
	this.commonControlsAIEditorInputListeners[player].setSetButtonWarning(false);
	this.commonControlsAIEditorInputListeners[player].setSaveButtonWarning(false);
};

/**
 * @private
 */
LS_REVERSI.AIEditorController.prototype.onDelete = function(aiConfigurationName) {
	this.aiConfigurationLocalStorageManager.remove(aiConfigurationName);
	this.aiConfigurationListInputListeners[1].listen(this);
	this.aiConfigurationListInputListeners[2].listen(this);
	this.retroConsole.addTextLine("Erased A.I. Configuration:");
	this.retroConsole.addTextLine("    - named: " + aiConfigurationName);
};

/**
 * @private
 */
LS_REVERSI.AIEditorController.prototype.onPauseResume = function(pausedResumed) {
	if(pausedResumed === "paused") {
		this.commonControlsAIEditorInputListeners[1].setSetButtonEnable(true);
		this.commonControlsAIEditorInputListeners[2].setSetButtonEnable(true);
	} else if(pausedResumed === "resumed") {
		this.commonControlsAIEditorInputListeners[1].setSetButtonEnable(false);
		this.commonControlsAIEditorInputListeners[2].setSetButtonEnable(false);
	} else {
		throw "paused/resumed callback not understood: pausedResumed=" + pausedResumed;
	}
};

/**
 * @private
 */
LS_REVERSI.AIEditorController.prototype.onEditedAIConfigurationChanged = function(player, setWarning) {
	if(setWarning) {
		this.commonControlsAIEditorInputListeners[player].setSetButtonWarning(true);
	}
	this.commonControlsAIEditorInputListeners[player].setSaveButtonWarning(true);
	var aiConfiguration = this.aiConfigurations[player];
	this.retroConsole.addTextLine("Updated A.I. Configuration:");
	this.retroConsole.addTextLine("    - named: " + aiConfiguration.name);
	this.retroConsole.addTextLine("    - for player: #" + player);
	this.retroConsole.addTextLine("    - containing: " + this.getAIConfigurationDescription(aiConfiguration));
};

/**
 * @private
 */
LS_REVERSI.AIEditorController.prototype.listenForInput = function() {
	this.parameterizedAIEditorInputListeners[1].listen(this);
	this.parameterizedAIEditorInputListeners[2].listen(this);
	this.commonControlsAIEditorInputListeners[1].listen(this, this);
	this.commonControlsAIEditorInputListeners[2].listen(this, this);	
	this.aiConfigurationListInputListeners[1].listen(this);
	this.aiConfigurationListInputListeners[2].listen(this);	
};

/**
 * @private
 */
LS_REVERSI.AIEditorController.prototype.initAIConfiguration = function() {
	var parameterizedAIConfiguration1 = new LS_REVERSI.AIConfiguration("New AI Configuration", 0, false, "parameterized",
		 {
		   	evaluationDepth: 1,
		   	tokenScore: {
		   		takenCorner: 0,
		   		cornerExposing: 0,
		   		takenBorder: 0,
		   		borderExposing: 0,
		   		count: 0
		   	}
		  });
	var parameterizedAIConfiguration2 = new LS_REVERSI.AIConfiguration("New AI Configuration", 0, false, "parameterized",
			 {
			   	evaluationDepth: 1,
			   	tokenScore: {
			   		takenCorner: 0,
			   		cornerExposing: 0,
			   		takenBorder: 0,
			   		borderExposing: 0,
			   		count: 0
			   	}
			  });
	
	this.updateAIConfiguration(1, parameterizedAIConfiguration1);
	this.updateAIConfiguration(2, parameterizedAIConfiguration2);
	this.setGameEvaluator(1);
	this.setGameEvaluator(2);
};

/**
 * @private
 */
LS_REVERSI.AIEditorController.prototype.setGameEvaluator = function(player) {
	var aiConfiguration = this.aiConfigurations[player];
	var gameEvaluator = this.aiConfigurationToReversiGameEvaluatorConverter.convert(player, aiConfiguration);
	if(player == 1) {
		this.retroConsole.addTextLine("Setting A.I. Configuration:");
		this.retroConsole.addTextLine("    - named: " + aiConfiguration.name);
		this.retroConsole.addTextLine("    - for player: #" + player);
		this.retroConsole.addTextLine("    - containing: " + this.getAIConfigurationDescription(aiConfiguration));
		this.cpuVsCpuRunner.setGameEvaluatorBlack(gameEvaluator);
	} else if(player == 2) {
		this.retroConsole.addTextLine("Setting A.I. Configuration:");
		this.retroConsole.addTextLine("    - named: " + aiConfiguration.name);
		this.retroConsole.addTextLine("    - for player: #" + player);
		this.retroConsole.addTextLine("    - containing: " + this.getAIConfigurationDescription(aiConfiguration));
		this.cpuVsCpuRunner.setGameEvaluatorWhite(gameEvaluator);
	}
};

/**
 * @private
 */
LS_REVERSI.AIEditorController.prototype.getAIConfigurationDescription = function(aiConfiguration) {
	if(aiConfiguration.type === "parameterized") {
		var str = "ED:" + aiConfiguration.parameters.evaluationDepth + 
			" TC:" + aiConfiguration.parameters.tokenScore.takenCorner + 
			" CE:" + aiConfiguration.parameters.tokenScore.cornerExposing + 
			" TB:" + aiConfiguration.parameters.tokenScore.takenBorder + 
			" BE:" + aiConfiguration.parameters.tokenScore.borderExposing + 
			" C:" + aiConfiguration.parameters.tokenScore.count; 
		return str;
	} else {
		return "Scripted AIConfiguration. Use LS_REVERSI.ReversiGameEvaluator.toString()";
	}
};

/**
 * @private
 */
LS_REVERSI.AIEditorController.prototype.displayDemoModeRestriction = function() {
	alert("This functionality is not available in Demo");
};
