var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.AIEditorDebugConsole}
 */
LS_REVERSI.AIEditorDebugConsole = function(canvasId, gameStateHistory, imageLoader, frameRenderer) {
	this.canvasId = canvasId;	
	this.gameStateHistory = gameStateHistory;
	this.imageLoader = imageLoader;
	this.frameRenderer = frameRenderer;
	
	this.consoleWidth = 400;
	this.consoleHeight = 400;
};

/**
 * @public
 */
LS_REVERSI.AIEditorDebugConsole.prototype.startConsole = function(retroConsole) {
	var inputEventQueue = new LS_GAME_LOOP.InputEventQueue(500);
	var entities = new Array();
	entities[0] = new LS_REVERSI.GameStateHistoryDebug(0, 300, this.gameStateHistory, this.consoleWidth);
	var aiDebug = new LS_REVERSI.AIDebug(this.gameStateHistory, retroConsole);
	entities[1] = aiDebug;
	
	var gameLoop = new LS_GAME_LOOP.GameLoop(this.canvasId,
			this.consoleWidth,
			this.consoleHeight,
			//set metronome for 20 UPS:
			new LS_GAME_LOOP.Metronome(50),
			entities,
			inputEventQueue);
	gameLoop.start();	
};
