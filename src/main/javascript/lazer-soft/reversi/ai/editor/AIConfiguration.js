var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @param {String} type can be either "parameterized" or "scripted"
 * @param {String} parameters 
 *  {
 *  	evaluationDepth: 4,
 *  	tokenScore: {
 *  		takenCorner: 10,
 *  		cornerExposing: -8,
 *  		takenBorder: 5,
 *  		borderExposing: -3,
 *  		count: -2
 *  	}
 *  }
 * @returns {LS_REVERSI.AIConfiguration}
 * 
 * Because this object is serialized/deserialized by LS_REVERSI.AIConfigurationLocalStorageManager, any modifications made to it's structure (fields/functions, etc) should also be reflected in the 
 * serialization/deserialization functions of LS_REVERSI.AIConfigurationLocalStorageManager
 */
LS_REVERSI.AIConfiguration = function(name, displayOrder, exportable, type, parameters, evaluateIntermediateState, getEvaluationDepth) {
	this.name = name;
	this.displayOrder = displayOrder;
	this.exportable = exportable;
	this.type = type;
	if(type === "parameterized") {
		this.parameters = parameters;
	} else {
		this.evaluateIntermediateState = evaluateIntermediateState;
		this.getEvaluationDepth = getEvaluationDepth;
	}
};