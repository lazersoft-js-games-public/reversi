var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @returns {LS_REVERSI.AIConfigurationToReversiGameEvaluatorConverter}
 */
LS_REVERSI.AIConfigurationToReversiGameEvaluatorConverter = function(constants, moveHelper, maxNodesToEvaluate) {
	this.constants = constants;
	this.moveHelper = moveHelper;
	this.maxNodesToEvaluate = maxNodesToEvaluate;
};

/**
 * @public
 */
LS_REVERSI.AIConfigurationToReversiGameEvaluatorConverter.prototype.convert = function(playerToWin, aiConfiguration) {
	var evaluateIntermediateState = this.generateEvaluateIntermediateState(aiConfiguration);
	var getEvaluationDepth = this.generateGetEvaluationDepth(aiConfiguration);
	var evaluator = new LS_REVERSI.ReversiGameEvaluator(playerToWin, getEvaluationDepth, 
			this.constants, this.moveHelper, evaluateIntermediateState, this.maxNodesToEvaluate);
	return evaluator;
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationToReversiGameEvaluatorConverter.prototype.generateEvaluateIntermediateState = function(aiConfiguration) {
	if(aiConfiguration.type === "parameterized") {
		var evaluateIntermediateState = function(playerToWin, gameState, gameStateSummary) {
			var takenCornerScore = aiConfiguration.parameters.tokenScore.takenCorner;
    		var cornerExposingTokenScore = aiConfiguration.parameters.tokenScore.cornerExposing;
    		var takenBorderScore = aiConfiguration.parameters.tokenScore.takenBorder;
    		var borderExposingTokenScore = aiConfiguration.parameters.tokenScore.borderExposing;
    		var countScore = aiConfiguration.parameters.tokenScore.count;
    		
    		var playerToWin = playerToWin;
    		
    		var whiteScore = takenCornerScore * gameStateSummary.takenCornersWhite
	    		+ cornerExposingTokenScore * gameStateSummary.cornerExposingTokensWhite 
	    		+ takenBorderScore * gameStateSummary.takenBordersWhite 
	    		+ borderExposingTokenScore * gameStateSummary.borderExposingTokensWhite
	    		+ countScore * gameStateSummary.tokenCountWhite;
    		var blackScore = takenCornerScore * gameStateSummary.takenCornersBlack
	    		+ cornerExposingTokenScore * gameStateSummary.cornerExposingTokensBlack 
	    		+ takenBorderScore * gameStateSummary.takenBordersBlack 
	    		+ borderExposingTokenScore * gameStateSummary.borderExposingTokensBlack
	    		+ countScore * gameStateSummary.tokenCountBlack;
    		if(playerToWin == 2) {
    			return whiteScore - blackScore; 
    		} else {
    			return blackScore - whiteScore;
    		}
		};
		return evaluateIntermediateState;
	} else if(aiConfiguration.type === "scripted") {
		return aiConfiguration.evaluateIntermediateState;
	} else {
		this.throwUnknowTypeException(aiConfiguration);
	}
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationToReversiGameEvaluatorConverter.prototype.generateGetEvaluationDepth = function(aiConfiguration) {
	if(aiConfiguration.type === "parameterized") {
		return this.constants.parametrizedAIGetEvaluationDepthFunction(aiConfiguration.parameters.evaluationDepth);
	} else if(aiConfiguration.type === "scripted") {
		return aiConfiguration.getEvaluationDepth;
	} else {
		this.throwUnknowTypeException(aiConfiguration);
	}
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationToReversiGameEvaluatorConverter.prototype.throwUnknowTypeException = function(aiConfiguration) {
	throw "No (de)serialization for LS_REVERSI.AIConfiguration of type: " + aiConfiguration.type;
};