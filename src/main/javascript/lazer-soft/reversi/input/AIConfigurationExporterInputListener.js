var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.AIConfigurationExporterInputListener}
 */
LS_REVERSI.AIConfigurationExporterInputListener = function(exportButtonId, aiConfigurationExporter) {
	this.exportButtonId = exportButtonId;
	this.aiConfigurationExporter = aiConfigurationExporter;
	
	this.exportButton = null;
};


/**
 * @public
 */
LS_REVERSI.AIConfigurationExporterInputListener.prototype.listen = function() {
	this.exportButton = document.getElementById(this.exportButtonId);
	var self = this;
	this.exportButton.onmouseup = function() {
		if(LS_REVERSI.DM) {
			alert("This functionality is not available in Demo");
			return;
		}
		self.aiConfigurationExporter.exportAIConfigurations();
	};
};