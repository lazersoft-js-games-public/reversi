var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.GameStateHistoryInputEventFactory}
 */
LS_REVERSI.GameStateHistoryInputEventFactory = function() {
	this.inputEvents = null;
	this.initEvents();
};

/**
 * @public
 */
LS_REVERSI.GameStateHistoryInputEventFactory.prototype.createResetEvent = function() {
	return this.inputEvents["reset"];
};

/**
 * @public
 */
LS_REVERSI.GameStateHistoryInputEventFactory.prototype.createBackEvent = function() {
	return this.inputEvents["back"];
};

/**
 * @public
 */
LS_REVERSI.GameStateHistoryInputEventFactory.prototype.createForwardEvent = function() {
	return this.inputEvents["forward"];
};

/**
 * @public
 */
LS_REVERSI.GameStateHistoryInputEventFactory.prototype.createPauseResumeEvent = function() {
	return this.inputEvents["pauseResume"];
};

/**
 * @private
 */
LS_REVERSI.GameStateHistoryInputEventFactory.prototype.initEvents = function() {
	this.inputEvents = new Array();
	this.inputEvents["reset"] = new LS_REVERSI.GameStateHistoryInputEvent("reset");
	this.inputEvents["back"] = new LS_REVERSI.GameStateHistoryInputEvent("back");
	this.inputEvents["forward"] = new LS_REVERSI.GameStateHistoryInputEvent("forward");
	this.inputEvents["pauseResume"] = new LS_REVERSI.GameStateHistoryInputEvent("pauseResume");
};