var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.GameMenuInputEventFactory}
 */
LS_REVERSI.GameMenuInputEventFactory = function() {
	this.inputEvents = null;
	this.initEvents();
};

/**
 * @public
 */
LS_REVERSI.GameMenuInputEventFactory.prototype.createSetHumanPlayerBlackEvent = function() {
	return this.inputEvents["setHumanPlayerBlack"];
};

/**
 * @public
 */
LS_REVERSI.GameMenuInputEventFactory.prototype.createSetHumanPlayerWhiteEvent = function() {
	return this.inputEvents["setHumanPlayerWhite"];
};

/**
 * @public
 */
LS_REVERSI.GameMenuInputEventFactory.prototype.createSelectAIConfigurationNameEvent = function(aiConfigurationName) {
	return new LS_REVERSI.GameMenuInputEvent("selectAIConfigurationName", aiConfigurationName);
};

/**
 * @public
 */
LS_REVERSI.GameMenuInputEventFactory.prototype.createStartEvent = function() {
	return this.inputEvents["start"];
};

/**
 * @public
 */
LS_REVERSI.GameMenuInputEventFactory.prototype.createUndoEvent = function() {
	return this.inputEvents["undo"];
};

/**
 * @private
 */
LS_REVERSI.GameMenuInputEventFactory.prototype.initEvents = function() {
	this.inputEvents = new Array();
	this.inputEvents["setHumanPlayerBlack"] = new LS_REVERSI.GameMenuInputEvent("setHumanPlayerBlack");
	this.inputEvents["setHumanPlayerWhite"] = new LS_REVERSI.GameMenuInputEvent("setHumanPlayerWhite");
	this.inputEvents["start"] = new LS_REVERSI.GameMenuInputEvent("start");
	this.inputEvents["undo"] = new LS_REVERSI.GameMenuInputEvent("undo");
};