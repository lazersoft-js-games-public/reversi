var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.GameStateHistoryInputListener}
 */
LS_REVERSI.GameStateHistoryInputListener = function(resetButtonId, backButtonId, pauseResumeButtonId, forwardButtonId, inputEventQueue, gameStateHisotryInputEventFactory, pauseResumeListener) {
	this.resetButtonId = resetButtonId;
	this.backButtonId = backButtonId;
	this.pauseResumeButtonId = pauseResumeButtonId;
	this.forwardButtonId = forwardButtonId;
	this.inputEventQueue = inputEventQueue;
	this.gameStateHisotryInputEventFactory = gameStateHisotryInputEventFactory;
	this.pauseResumeListener = pauseResumeListener === undefined ? null : pauseResumeListener;
	
	this.resetButton = null;
	this.backButton = null;
	this.pauseResumeButton = null;
	this.forwardButton = null;
	
	this.paused = true;
};

/**
 * @public
 */
LS_REVERSI.GameStateHistoryInputListener.prototype.listen = function() {
	this.initButtons();
	this.registerButtons();
	this.updateButtonsState();
};

/**
 * @public
 */
LS_REVERSI.GameStateHistoryInputListener.prototype.onGameEnded = function() {
	if(!this.paused) {
		var event = this.gameStateHisotryInputEventFactory.createPauseResumeEvent();
		this.inputEventQueue.push(event);
		this.paused = true;
		this.updateButtonsState();
		if(this.pauseResumeListener != null) {
			this.pauseResumeListener.onPauseResume("paused");
		}
	}
};

/**
 * @private
 */
LS_REVERSI.GameStateHistoryInputListener.prototype.initButtons = function() {
	this.resetButton = document.getElementById(this.resetButtonId);
	this.backButton = document.getElementById(this.backButtonId);
	this.pauseResumeButton = document.getElementById(this.pauseResumeButtonId);
	this.forwardButton = document.getElementById(this.forwardButtonId);
};

/**
 * @private
 */
LS_REVERSI.GameStateHistoryInputListener.prototype.registerButtons = function() {
	var self = this;
	this.resetButton.addEventListener("mouseup", function(e) {
		var event = self.gameStateHisotryInputEventFactory.createResetEvent();
		self.inputEventQueue.push(event);
	});
	this.backButton.addEventListener("mouseup", function(e) {
		var event = self.gameStateHisotryInputEventFactory.createBackEvent();
		self.inputEventQueue.push(event);
	});
	this.pauseResumeButton.addEventListener("mouseup", function(e) {
		var event = self.gameStateHisotryInputEventFactory.createPauseResumeEvent();
		self.inputEventQueue.push(event);
		self.paused = !self.paused;
		self.updateButtonsState();
		if(self.pauseResumeListener != null) {
			self.pauseResumeListener.onPauseResume(self.paused ? "paused" : "resumed");
		}
	});
	this.forwardButton.addEventListener("mouseup", function(e) {
		var event = self.gameStateHisotryInputEventFactory.createForwardEvent();
		self.inputEventQueue.push(event);
	});	
};

/**
 * @private
 */
LS_REVERSI.GameStateHistoryInputListener.prototype.updateButtonsState = function() {
	if(this.paused) {
		this.enableButton(this.resetButton);
		this.enableButton(this.backButton);
		this.setButtonText(this.pauseResumeButton, "|>");
		this.enableButton(this.forwardButton);
	} else {
		this.disableButton(this.resetButton);
		this.disableButton(this.backButton);
		this.setButtonText(this.pauseResumeButton, "||");
		this.disableButton(this.forwardButton);
	}
};

/**
 * @private
 */
LS_REVERSI.GameStateHistoryInputListener.prototype.disableButton = function(button) {
	button.disabled = true;
};

/**
 * @private
 */
LS_REVERSI.GameStateHistoryInputListener.prototype.enableButton = function(button) {
	button.disabled = false;
};

/**
 * @private
 */
LS_REVERSI.GameStateHistoryInputListener.prototype.setButtonText = function(button, newText) {
	button.setAttribute("value", newText);
};
