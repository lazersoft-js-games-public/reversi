var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.GameMenuInputListener}
 */
LS_REVERSI.GameMenuInputListener = function(radioBlackId, radioWhiteId, selectDifficultyId, buttonStartId, buttonUndoId, humanScoreSpanId, aiScoreSpanId,
		aiConfigurationNames, inputEventQueue, gameMenuInputEventFactory, pauseResumeButtonId) {
	
	this.radioBlackId = radioBlackId;
	this.radioWhiteId = radioWhiteId;
	this.selectDifficultyId = selectDifficultyId;
	this.buttonStartId = buttonStartId;
	this.humanScoreSpanId = humanScoreSpanId;
	this.aiScoreSpanId = aiScoreSpanId;
	this.buttonUndoId = buttonUndoId;
	this.aiConfigurationNames = aiConfigurationNames;
	this.inputEventQueue = inputEventQueue;
	this.gameMenuInputEventFactory = gameMenuInputEventFactory;
	this.pauseResumeButtonId = pauseResumeButtonId === undefined ? null : pauseResumeButtonId;
	
	this.radioBlack = null;
	this.radioWhite = null;
	this.selectDifficulty = null;
	this.buttonStart = null;
	this.buttonUndo = null;
	this.humanScoreSpan = null;
	this.aiScoreSpan = null;
	this.pauseResumeButton = null;
	this.gameLoopPaused = false;
};

/**
 * @public
 */
LS_REVERSI.GameMenuInputListener.prototype.listen = function() {
	this.initControls();
};

/**
 * @public
 */
LS_REVERSI.GameMenuInputListener.prototype.onScoreChange = function(humanScore, aiScore) {
	this.humanScoreSpan.innerHTML = "" + humanScore;
	this.aiScoreSpan.innerHTML = "" + aiScore;
};

/**
 * @private
 */
LS_REVERSI.GameMenuInputListener.prototype.initControls = function() {
	var self = this;
	
	this.radioBlack = document.getElementById(this.radioBlackId);
	this.radioBlack.checked = true;
	this.radioBlack.onchange = function() {
		var event = self.gameMenuInputEventFactory.createSetHumanPlayerBlackEvent();
		self.inputEventQueue.push(event);
	};
	
	this.radioWhite = document.getElementById(this.radioWhiteId);
	this.radioWhite.onchange = function() {
		var event = self.gameMenuInputEventFactory.createSetHumanPlayerWhiteEvent();
		self.inputEventQueue.push(event);
	};
	
	this.selectDifficulty = document.getElementById(this.selectDifficultyId);
	for(var i = 0; i < this.aiConfigurationNames.length; i++) {
		var aiConfigurationName = this.aiConfigurationNames[i];
		var option = document.createElement("option");
		option.text = aiConfigurationName;
		this.selectDifficulty.add(option);
		if(i == 0) {
			option.selected = true;
		}
	}
	
	this.selectDifficulty.onchange = function(e) {
		var selectedOption = self.selectDifficulty[self.selectDifficulty.options.selectedIndex];
		var event = self.gameMenuInputEventFactory.createSelectAIConfigurationNameEvent(selectedOption.text);
		self.inputEventQueue.push(event);
	}
	
	this.buttonStart = document.getElementById(this.buttonStartId);
	this.buttonStart.onmouseup = function() {
		var event = self.gameMenuInputEventFactory.createStartEvent();
		self.inputEventQueue.push(event);
	};
	
	this.buttonUndo = document.getElementById(this.buttonUndoId);
	this.buttonUndo.onmouseup = function() {
		var event = self.gameMenuInputEventFactory.createUndoEvent();
		self.inputEventQueue.push(event);
	};
	
	if(this.pauseResumeButtonId != null) {
		this.pauseResumeButton = document.getElementById(this.pauseResumeButtonId);
		this.pauseResumeButton.onmouseup = function() {
			self.gameLoopPaused = !self.gameLoopPaused;
			self.radioBlack.disabled = self.gameLoopPaused;
			self.radioWhite.disabled = self.gameLoopPaused;
			self.selectDifficulty.disabled = self.gameLoopPaused;
			self.buttonStart.disabled = self.gameLoopPaused;
			self.buttonUndo.disabled = self.gameLoopPaused;
		};
	}
	
	this.humanScoreSpan = document.getElementById(this.humanScoreSpanId);
	this.aiScoreSpan = document.getElementById(this.aiScoreSpanId);
};