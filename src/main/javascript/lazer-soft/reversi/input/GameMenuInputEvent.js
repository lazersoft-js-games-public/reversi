var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.GameMenuInputEvent}
 */
LS_REVERSI.GameMenuInputEvent = function(type, value) {
	this.type = type;
	this.value = value === undefined ? null : value;
};