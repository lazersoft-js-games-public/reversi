var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.CommonControlsAIEditorInputListener}
 */
LS_REVERSI.CommonControlsAIEditorInputListener = function(nameInputId, displayOrderInputId, exportableCheckboxId, saveButtonId, setButtonId, player) {
	this.nameInputId = nameInputId;
	this.displayOrderInputId = displayOrderInputId;
	this.exportableCheckboxId = exportableCheckboxId;
	this.saveButtonId = saveButtonId;
	this.setButtonId = setButtonId;
	this.player = player;
	
	this.nameInput = null;
	this.displayOrderInput = null;
	this.exportableCheckbox = null;
	this.saveButton = null;
	this.setButton = null;
	
	this.saveButtonValue = null;
	this.setButtonValue = null;
	
	this.buttonsListener = null;
	this.inputChangeListener = null;
};

/**
 * @public
 */
LS_REVERSI.CommonControlsAIEditorInputListener.prototype.listen = function(buttonsListener, inputChangeListener) {
	this.buttonsListener = buttonsListener;
	this.inputChangeListener = inputChangeListener;
	this.initInputs();
};

/**
 * @public
 */
LS_REVERSI.CommonControlsAIEditorInputListener.prototype.setAIConfiguration = function(aiConfiguration) {
	this.aiConfiguration = aiConfiguration;
	this.nameInput.value = this.aiConfiguration.name;
	this.displayOrderInput.value = this.aiConfiguration.displayOrder;
	this.exportableCheckbox.checked = this.aiConfiguration.exportable;
};

/**
 * @public
 */
LS_REVERSI.CommonControlsAIEditorInputListener.prototype.setSetButtonEnable = function(enable) {
	this.setButton.disabled = !enable;
};

/**
 * @public
 */
LS_REVERSI.CommonControlsAIEditorInputListener.prototype.setSaveButtonWarning = function(isWarning) {
	if(isWarning) {
		this.saveButton.value = this.saveButtonValue + " (!)";
	} else {
		this.saveButton.value = this.saveButtonValue;
	}
};

/**
 * @public
 */
LS_REVERSI.CommonControlsAIEditorInputListener.prototype.setSetButtonWarning = function(isWarning) {
	if(isWarning) {
		this.setButton.value = this.setButtonValue + " (!)";
	} else {
		this.setButton.value = this.setButtonValue;
	}
};

/**
 * @private
 */
LS_REVERSI.CommonControlsAIEditorInputListener.prototype.initInputs = function() {
	var self = this;
	
	this.nameInput = document.getElementById(this.nameInputId);
	this.nameInput.onchange = function(e) {
		self.aiConfiguration.name = e.target.value;
		self.inputChangeListener.onEditedAIConfigurationChanged(self.player, false);
	};
	
	this.displayOrderInput = document.getElementById(this.displayOrderInputId);
	this.displayOrderInput.onchange = function(e) {
		self.aiConfiguration.displayOrder = self.getAsInt(e.target.value);
		self.inputChangeListener.onEditedAIConfigurationChanged(self.player, false);
	};
	
	this.exportableCheckbox = document.getElementById(this.exportableCheckboxId);
	this.exportableCheckbox.onchange = function(e) {
		self.aiConfiguration.exportable = e.target.checked;
		self.inputChangeListener.onEditedAIConfigurationChanged(self.player, false);
	};
	
	this.saveButton = document.getElementById(this.saveButtonId);
	this.saveButtonValue = this.saveButton.value;
	this.saveButton.onmouseup = function(e) {
		self.buttonsListener.onSave(self.player);
	};
	
	this.setButton = document.getElementById(this.setButtonId);
	this.setButtonValue = this.setButton.value;
	this.setButton.onmouseup = function(e) {
		self.buttonsListener.onSet(self.player);
	};
};

/**
 * @private
 */
LS_REVERSI.CommonControlsAIEditorInputListener.prototype.getAsInt = function(value) {
	return parseInt(value, 10);
};