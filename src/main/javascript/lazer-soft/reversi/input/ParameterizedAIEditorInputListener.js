var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.ParameterizedAIEditorInputListener}
 */
LS_REVERSI.ParameterizedAIEditorInputListener = function(player,
		baseEvaluationDepthInputId,
		takenCornerScoreInputId, cornerExposingTokenScoreInputId, 
		takenBorderScoreInputId, borderExposingTokenScoreInputId, 
		countTokenScoreInputId) {
	
	this.player = player;
	this.baseEvaluationDepthInputId = baseEvaluationDepthInputId;
	this.takenCornerScoreInputId = takenCornerScoreInputId;
	this.cornerExposingTokenScoreInputId = cornerExposingTokenScoreInputId;
	this.takenBorderScoreInputId = takenBorderScoreInputId;
	this.borderExposingTokenScoreInputId = borderExposingTokenScoreInputId;
	this.countTokenScoreInputId = countTokenScoreInputId;
	
	this.baseEvaluationDepthInput = null;
	this.takenCornerScoreInput = null;
	this.cornerExposingTokenScoreInput = null;
	this.takenBorderScoreInput = null;
	this.borderExposingTokenScoreInput = null;
	this.countTokenScoreInput = null;
	
	this.aiConfiguration = null;
	
	this.inputChangeListener = null;
};

/**
 * @public
 */
LS_REVERSI.ParameterizedAIEditorInputListener.prototype.listen = function(inputChangeListener) {
	this.inputChangeListener = inputChangeListener;
	this.initInputs();
};

/**
 * @public
 */
LS_REVERSI.ParameterizedAIEditorInputListener.prototype.setAIConfiguration = function(aiConfiguration) {
	this.aiConfiguration = aiConfiguration;
	
	this.baseEvaluationDepthInput.value = this.aiConfiguration.parameters.evaluationDepth;
	this.takenCornerScoreInput.value = this.aiConfiguration.parameters.tokenScore.takenCorner;
	this.cornerExposingTokenScoreInput.value = this.aiConfiguration.parameters.tokenScore.cornerExposing;
	this.takenBorderScoreInput.value = this.aiConfiguration.parameters.tokenScore.takenBorder;
	this.borderExposingTokenScoreInput.value = this.aiConfiguration.parameters.tokenScore.borderExposing;
	this.countTokenScoreInput.value = this.aiConfiguration.parameters.tokenScore.count;	
};

/**
 * @private
 */
LS_REVERSI.ParameterizedAIEditorInputListener.prototype.initInputs = function() {
	var self = this;
	
	this.baseEvaluationDepthInput = document.getElementById(this.baseEvaluationDepthInputId);
	this.baseEvaluationDepthInput.onchange = function(e) {
		self.aiConfiguration.parameters.evaluationDepth = self.getAsInt(e.target.value);
		self.inputChangeListener.onEditedAIConfigurationChanged(self.player, true);
	};
	
	this.takenCornerScoreInput = document.getElementById(this.takenCornerScoreInputId);
	this.takenCornerScoreInput.onchange = function(e) {
		self.aiConfiguration.parameters.tokenScore.takenCorner = self.getAsInt(e.target.value);
		self.inputChangeListener.onEditedAIConfigurationChanged(self.player, true);
	};
	
	this.cornerExposingTokenScoreInput = document.getElementById(this.cornerExposingTokenScoreInputId);
	this.cornerExposingTokenScoreInput.onchange = function(e) {
		self.aiConfiguration.parameters.tokenScore.cornerExposing = self.getAsInt(e.target.value);
		self.inputChangeListener.onEditedAIConfigurationChanged(self.player, true);
	};
	
	this.takenBorderScoreInput = document.getElementById(this.takenBorderScoreInputId);
	this.takenBorderScoreInput.onchange = function(e) {
		self.aiConfiguration.parameters.tokenScore.takenBorder = self.getAsInt(e.target.value);
		self.inputChangeListener.onEditedAIConfigurationChanged(self.player, true);
	};
	
	this.borderExposingTokenScoreInput = document.getElementById(this.borderExposingTokenScoreInputId);
	this.borderExposingTokenScoreInput.onchange = function(e) {
		self.aiConfiguration.parameters.tokenScore.borderExposing = self.getAsInt(e.target.value);
		self.inputChangeListener.onEditedAIConfigurationChanged(self.player, true);
	};
	
	this.countTokenScoreInput = document.getElementById(this.countTokenScoreInputId);
	this.countTokenScoreInput.onchange = function(e) {
		self.aiConfiguration.parameters.tokenScore.count = self.getAsInt(e.target.value);
		self.inputChangeListener.onEditedAIConfigurationChanged(self.player, true);
	};
};

/**
 * @private
 */
LS_REVERSI.ParameterizedAIEditorInputListener.prototype.getAsInt = function(value) {
	return parseInt(value, 10);
};