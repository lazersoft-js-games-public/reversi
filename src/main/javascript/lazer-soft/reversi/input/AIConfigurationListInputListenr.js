var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.AIConfigurationListInputListenr}
 */
LS_REVERSI.AIConfigurationListInputListenr = function(tableId, player, aiConfigurationLocalStorageManager) {
	this.tableId = tableId;
	this.player = player;
	this.aiConfigurationLocalStorageManager = aiConfigurationLocalStorageManager;
	
	this.table = null;
	this.columnsRow = "<thead>" +
						"<tr>" +
							"<th>A.I. Configuration Name</th>" +
							"<th>Display order</th>" +
							"<th>Is it exportable ?</th>" +
							"<th colspan=\"2\">actions</th>"
						"</tr>" +
					  "</thead>"	
	this.rowTemplate =  "<tr>" +
							"<td>[[name]]</td>" +
							"<td>[[displayOrder]]</td>" +
							"<td>[[exportable]]</td>" +
							"<td><input type='button' value='Edit (player " + this.player + ")' id='[[load_button_id]]'></td>" +
							"<td><input type='button' value='Delete' id='[[delete_button_id]]'></td>" +
						"</tr>";
	
	this.buttonsListener = null;
	
	this.aiConfigurations = null;
};

/**
 * @public
 */
LS_REVERSI.AIConfigurationListInputListenr.prototype.listen = function(buttonsListener) {
	this.buttonsListener = buttonsListener;
	this.loadAIConfigurations();
	this.initTable();
	this.populateTable();
	this.addTableButtonCallbacks();
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationListInputListenr.prototype.initTable = function() {
	this.table = document.getElementById(this.tableId);
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationListInputListenr.prototype.loadAIConfigurations = function() {
	this.aiConfigurations = this.aiConfigurationLocalStorageManager.getAll();
	this.aiConfigurations.sort(function(aiConfig1, aiConfig2) {
		return aiConfig1.displayOrder - aiConfig2.displayOrder;
	});
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationListInputListenr.prototype.populateTable = function() {
	while (this.table.firstChild) {
		this.table.removeChild(this.table.firstChild);
	}

	var tableInnerHTML = this.columnsRow;
	tableInnerHTML += "<tbody>";
	for(var i = 0; i < this.aiConfigurations.length; i++) {
		var aiConfiguration = this.aiConfigurations[i];
		tableInnerHTML += this.generateRowHtml(aiConfiguration, i);
	}
	tableInnerHTML += "</tbody>";
	this.table.innerHTML = tableInnerHTML;
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationListInputListenr.prototype.generateRowHtml = function(aiConfiguration, rowIdx) {
	return this.rowTemplate.
		replace("[[name]]", aiConfiguration.name).
		replace("[[displayOrder]]", aiConfiguration.displayOrder).
		replace("[[exportable]]", aiConfiguration.exportable).
		replace("[[load_button_id]]", "load_" + aiConfiguration.name + "_" + this.player).
		replace("[[delete_button_id]]", "delete_" + aiConfiguration.name + "_" + this.player);
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationListInputListenr.prototype.addTableButtonCallbacks = function() {
	for(var i = 0; i < this.aiConfigurations.length; i++) {
		var aiConfiguration = this.aiConfigurations[i];
		var loadButton = document.getElementById("load_" + aiConfiguration.name + "_" + this.player);
		var deleteButton = document.getElementById("delete_" + aiConfiguration.name + "_" + this.player);
		var self = this;
		loadButton.onmouseup = function(e) {
			self.buttonsListener.onLoad(self.player, e.target.id.substring(5, e.target.id.length - 2));
		}
		deleteButton.onmouseup = function(e) {
			self.buttonsListener.onDelete(e.target.id.substring(7, e.target.id.length - 2));
		}
	}
};