var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.GameStateHistoryInputEvent}
 */
LS_REVERSI.GameStateHistoryInputEvent = function(type) {
	this.type = type;
};