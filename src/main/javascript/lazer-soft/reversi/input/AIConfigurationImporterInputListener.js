var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.AIConfigurationImporterInputListener}
 */
LS_REVERSI.AIConfigurationImporterInputListener = function(importLocationInputId, importButtonId, aiConfigurationImporter, aiConfigurationLocalStorageManager) {
	this.importLocationInputId = importLocationInputId;
	this.importButtonId = importButtonId;
	this.aiConfigurationImporter = aiConfigurationImporter;
	this.aiConfigurationLocalStorageManager = aiConfigurationLocalStorageManager;
	
	this.importLocationInput = null;
	this.importButton = null;
};


/**
 * @public
 */
LS_REVERSI.AIConfigurationImporterInputListener.prototype.listen = function() {
	this.importLocationInput = document.getElementById(this.importLocationInputId);
	this.importLocationInput.value = this.calculateAIConfigURLDefaultValue();
	
	this.importButton = document.getElementById(this.importButtonId);
	var self = this;
	this.importButton.onmouseup = function() {
		if(LS_REVERSI.DM) {
			alert("This functionality is not available in Demo");
			return;
		}
		self.aiConfigurationImporter.importAIConfigurations(self.importLocationInput.value, function(aiConfigurations) {
			var existingAIConfigurationsNames = self.aiConfigurationLocalStorageManager.listConfigurationNames();
			if(existingAIConfigurationsNames !== undefined && existingAIConfigurationsNames != null) {
				for(var i = 0; i < existingAIConfigurationsNames.length; i++) {
					var existingAIConfName = existingAIConfigurationsNames[i];
					self.aiConfigurationLocalStorageManager.remove(existingAIConfName);					
				}
			}
			for(var i = 0; i < aiConfigurations.length; i++) {
				self.aiConfigurationLocalStorageManager.store(aiConfigurations[i]);
			}
			location.reload();
		});
	};
};

/**
 * @private
 */
LS_REVERSI.AIConfigurationImporterInputListener.prototype.calculateAIConfigURLDefaultValue = function() {
	var prefix = window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
	var pathName = window.location.pathname;
	var pathNameElements = pathName.split("/");
	pathName = pathName.substring(0, pathName.lastIndexOf("/") + 1) + "ai/ai-configurations.json";
	return prefix + pathName;
};