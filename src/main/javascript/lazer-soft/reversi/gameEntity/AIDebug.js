var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.AIDebug}
 */
LS_REVERSI.AIDebug = function(gameStateHistory, retroConsole) {
	LS_GAME_LOOP.Entity.call(this, 
			retroConsole.x, retroConsole.y, retroConsole.width, retroConsole.height, 1000);
	this.gameStateHistory = gameStateHistory;
	this.retroConsole = retroConsole;
	
	this.lastDebuggedHistoryLocation = null;
	this.currentHistoryLocation = null;
	this.currentGameState = null;

};

LS_REVERSI.AIDebug.prototype = Object.create(LS_GAME_LOOP.Entity.prototype);
LS_REVERSI.AIDebug.prototype.constructor = LS_REVERSI.AIDebug;

/**
 * @public
 */
LS_REVERSI.AIDebug.prototype.processInput = function(inputEvent) {};

/**
 * @public
 */
LS_REVERSI.AIDebug.prototype.updateState = function() {
	this.currentHistoryLocation = this.gameStateHistory.getCurrentLocation();
	this.currentGameState = this.gameStateHistory.getCurrent();
	this.retroConsole.updateState();
};

/**
 * @public
 * @param {CanvasRenderingContext2D} context2d
 */
LS_REVERSI.AIDebug.prototype.updateGraphics = function(context2d) {
	if(this.currentHistoryLocation != this.lastDebuggedHistoryLocation) {
		//this.retroConsole.clearScreen();
		var minimaxRunReport = this.currentGameState.getMinimaxRunReport();
		if(minimaxRunReport != null) {
			var playerThatMoved = this.currentGameState.getLastMove().player;
			var playerThatMovedDescription = playerThatMoved + " (" + (playerThatMoved == 1 ? "black" : "white") + ")";
			var score = minimaxRunReport.score;
			this.retroConsole.addTextLine("Move #" + this.currentHistoryLocation + " made by player " + playerThatMovedDescription);
			this.retroConsole.addTextLine("Exploration depth level (actual/maximum): " + (minimaxRunReport.evaluationDepth - minimaxRunReport.minEvaluationDepthReached) + "/" + minimaxRunReport.evaluationDepth);
			if(!minimaxRunReport.maxNodesToEvaluateReached) {
				this.retroConsole.addTextLine("Explored nodes: " + minimaxRunReport.exploredNodesCount);
			} else {
				this.retroConsole.addTextLine("Explored nodes: " + minimaxRunReport.exploredNodesCount + ". Stopped due to limit reached!");
			}			
			this.retroConsole.addTextLine("This took (ms): " + minimaxRunReport.durationMs);
			this.retroConsole.addTextLine("Best score for player " + playerThatMovedDescription + " was: " + score);
			if(!this.currentGameState.isGameEnded()) {
				this.retroConsole.addTextLine("And the game is still going");
			} else {
				//TODO out put who has won (or draw) and final score (even for draw)
				var winningPlayer = score == Number.POSITIVE_INFINITY ? playerThatMoved : 3 - playerThatMoved;
				this.retroConsole.addTextLine("And the game has ended. Player " + winningPlayer + " has won.");
			}
			this.retroConsole.addTextLine("-------------------------------");			
		} else {
			//this.retroConsole.addTextLine("A.I. hasn't run yet.");
		}
	}
	this.lastDebuggedHistoryLocation = this.currentHistoryLocation;
	this.retroConsole.updateGraphics(context2d);
};