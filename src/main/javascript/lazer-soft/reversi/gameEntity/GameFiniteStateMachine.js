var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.GameFiniteStateMachine}
 */
LS_REVERSI.GameFiniteStateMachine = function(tokensByPosition, cellsByPosition, cursor, gameState, minimax, constants, gameMenuInputEventQueue, moveHelper, 
		aiConfigurationToReversiGameEvaluatorConverter, aiConfigurations, canvasSize, splashScreen, gameOverScreen, demoOverScreen) {
	LS_GAME_LOOP.Entity.call(0, 0, 0, 0, -1);
	
	this.tokensByPosition = tokensByPosition;
	this.cellsByPosition = cellsByPosition;
	this.cursor = cursor;
	this.minimax = minimax;
	this.constants = constants;
	this.gameMenuInputEventQueue = gameMenuInputEventQueue;
	this.moveHelper = moveHelper;
	this.aiConfigurationToReversiGameEvaluatorConverter = aiConfigurationToReversiGameEvaluatorConverter;
	this.aiConfigurations = aiConfigurations;
	this.canvasSize = canvasSize;
	this.splashScreen = splashScreen;
	this.gameOverScreen = gameOverScreen;
	this.demoOverScreen = demoOverScreen;
	
	this.startingGameState = gameState;
	this.gameStateHistory = new LS_REVERSI.GameStateHistory();
	this.gameStateHistory.advanceFromCurrent(this.startingGameState);
	
	this.cellSize = this.cellsByPosition[0][0].width;
	this.nextMove = null;
	this.nextAvailableMoves = new Array();
	
	this.aiPlayer = null;
	this.humanPlayer = null;
	
	this.states = ["getNextHumanPossibleMoves", 
	               "waitForHumanToMove", 
	               "applyHumanMove", 
	               "waitForTokensToFlip", 
	               "getAIMove", 
	               "waitForDraw", 
	               "applyAIMove", 
	               "checkGameEnded", 
	               "idleOnGameEnded", 
	               "processGameMenuInputEvent",
	               "idleOnDemoEnded"];
	this.lastDifferentState = null;
	this.state = null;
	
	this.stateHistory = new Array();
	this.stateHistoryMaxSize = 15;
	
	this.drawsSinceWaiting = 0;
	this.maxWaitForDrawCount = 2;
	
	this.startingState = {
			humanPlayer: this.minimax.getHumanPlayer(),
			aiConfigurationName: this.aiConfigurations[0].name
	};
	
	this.scoreChangeListener = null;
	
	this.initState();
	
	this.lastGetAIMoveDurationMs = null;
};

LS_REVERSI.GameFiniteStateMachine.prototype = Object.create(LS_GAME_LOOP.Entity.prototype);
LS_REVERSI.GameFiniteStateMachine.prototype.constructor = LS_REVERSI.GameFiniteStateMachine;

/**
 * @public
 */
LS_REVERSI.GameFiniteStateMachine.prototype.processInput = function(inputEvent) {
	if(inputEvent instanceof LS_GAME_LOOP.CanvasInputEvent) {
		if(this.state === "waitForHumanToMove") {
			this.onWaitForHumanToMove(inputEvent);
		}
	}
};

/**
 * @public
 */
LS_REVERSI.GameFiniteStateMachine.prototype.updateState = function() {
	//poor man's FSM:
	switch(this.state) {
	case "getNextHumanPossibleMoves": {
		this.onGetNextHumanPossibleMoves();
		break;
	}
	case "applyHumanMove": {
		this.onApplyHumanMove();	
		break;
	}
	case "waitForTokensToFlip": {
		this.onWaitForTokensToFlip();
		break;
	}
	case "getAIMove": {
		this.onGetAIMove();
		break;
	}
	case "applyAIMove": {
		this.onApplyAIMove();
		break;
	}
	case "checkGameEnded": {
		this.onCheckGameEnded();
		break;
	}
	case "waitForDraw": {
		this.onWaitForDraw();
		break;
	}
	case "waitForHumanToMove": {
		if(this.gameMenuInputEventQueue.length > 0) {
			this.switchState("processGameMenuInputEvent");
		}
		break;
	}
	case "processGameMenuInputEvent": {
		this.onProcessGameMenuInputEvent();
		break;
	}
	case "idleOnGameEnded": {
		this.onIdleOnGameEnded();
		break;
	}
	case "idleOnDemoEnded": {
		this.onIdleOnDemoEnded();
		break;
	}
	}
};

/**
 * @public
 * @param {CanvasRenderingContext2D} context2d
 */
LS_REVERSI.GameFiniteStateMachine.prototype.updateGraphics = function(context2d) {
	switch(this.state) {
	case "waitForDraw": {
		this.onWaitForDraw(context2d);
		break;
	}
	}
};

/**
 * @public
 */
LS_REVERSI.GameFiniteStateMachine.prototype.registerScoreChangeListener = function(scoreChangeListener) {
	this.scoreChangeListener = scoreChangeListener;
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.initState = function() {
	this.switchState("processGameMenuInputEvent");
	this.setStartNewGameState();
	this.updateBoardForNewGameState();
	this.notifyScoreChangeListener();	
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.onGetNextHumanPossibleMoves = function() {
	if(LS_REVERSI.DM) {
		if(this.gameStateHistory.getCurrent().getTokenCount() >= 60) {
			this.switchState("idleOnDemoEnded");
			return;
		}
	}
	
	var nextMoveIterator = new LS_REVERSI.NextMoveIterator(this.gameStateHistory.getCurrent(), this.moveHelper, this.constants);
	this.nextAvailableMoves = new Array();
	while(nextMoveIterator.hasNext()) {
		this.nextAvailableMoves.push(nextMoveIterator.next());
	}
	for(var i = 0; i < this.nextAvailableMoves.length; i++) {
		var move = this.nextAvailableMoves[i];
		this.cellsByPosition[move.position.x][move.position.y].setTokenPlaceholder(true);
	}
	this.switchState("waitForHumanToMove");
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.onWaitForHumanToMove = function(inputEvent) {
	if(inputEvent.type === "mouseup") {
		var cellX = Math.floor(inputEvent.canvasX / this.cellSize);
		var cellY = Math.floor(inputEvent.canvasY / this.cellSize);
		for(var i = 0; i < this.nextAvailableMoves.length; i++) {
			var move = this.nextAvailableMoves[i];
			if(move.position.x == cellX && move.position.y == cellY) {
				this.nextMove = move;
				this.nextAvailableMoves = null;
				this.switchState("applyHumanMove");
				break;
			}
		}
	}
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.onApplyHumanMove = function() {
	this.clearTokenPlaceholders();
	this.gameStateHistory.advanceFromCurrent(this.minimax.advanceGame(this.nextMove));
	var lastMove = this.gameStateHistory.getCurrent().getLastMove();
	var tokenType = this.gameStateHistory.getCurrent().lastMove.player == 1 ? this.constants.tokenTypeBlack : this.constants.tokenTypeWhite; 
	this.tokensByPosition[lastMove.position.x][lastMove.position.y].placeOnBoard(tokenType);
	for(var i = 0; i < lastMove.flippedTokensPositions.length; i++) {
		var pos = lastMove.flippedTokensPositions[i];
		this.tokensByPosition[pos.x][pos.y].startFlipping();
	}
	this.notifyScoreChangeListener();
	this.switchState("waitForTokensToFlip");	
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.onWaitForTokensToFlip = function() {
	this.setWaitCursor();
	if(!this.areTokensFlipping()) {
		this.setActiveCursor();
		this.switchState("checkGameEnded");
	}
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.onGetAIMove = function() {
	var st = new Date().getTime();
	this.gameStateHistory.advanceFromCurrent(this.minimax.advanceGame());
	this.lastGetAIMoveDurationMs = new Date().getTime() - st;
	this.notifyScoreChangeListener();
	this.drawsSinceWaiting = 0;
	this.switchState("waitForDraw");
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.onApplyAIMove = function() {
	var lastMove = this.gameStateHistory.getCurrent().lastMove;
	var tokenType = lastMove.player == 1 ? this.constants.tokenTypeBlack : this.constants.tokenTypeWhite; 
	this.tokensByPosition[lastMove.position.x][lastMove.position.y].placeOnBoard(tokenType);
	for(var i = 0; i < lastMove.flippedTokensPositions.length; i++) {
		var pos = lastMove.flippedTokensPositions[i];
		this.tokensByPosition[pos.x][pos.y].startFlipping();
	}
	this.switchState("waitForTokensToFlip");
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.onCheckGameEnded = function() {
	if(this.gameStateHistory.getCurrent().isGameEnded()) {
		this.switchState("idleOnGameEnded");
	} else {
		if(this.gameStateHistory.getCurrent().getPlayerToMove() == this.humanPlayer) {
			this.switchState("getNextHumanPossibleMoves");
		} else {
			this.setWaitCursor();
			this.switchState("getAIMove");
		}
	}
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.onWaitForDraw = function(context2d) {
	if(context2d !== undefined) {
		this.drawsSinceWaiting++;
	} else if(this.drawsSinceWaiting >= this.maxWaitForDrawCount) {
		this.drawsSinceWaiting = 0;
		this.switchState("applyAIMove");
	}
};


/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.switchState = function(newState) {
	if(newState == null) {
		return;
	}
	if(newState !== this.state) {
		this.lastDifferentState = this.state;
	}
	this.state = newState;
	this.updateStateHistory(newState);
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.updateStateHistory = function(currentState) {
	this.stateHistory.push(currentState);
	if(this.stateHistory.length > this.stateHistoryMaxSize) {
		this.stateHistory.shift();
	}
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.areTokensFlipping = function() {
	var tokensFlipping = false;
	var lastMove = this.gameStateHistory.getCurrent().getLastMove();
	for(var i = 0; i < lastMove.flippedTokensPositions.length; i++) {
		var pos = lastMove.flippedTokensPositions[i];
		var token = this.tokensByPosition[pos.x][pos.y];
		if(token.flipping || token.shouldStartFlipping) {
			tokensFlipping = true;
			break;
		}
	}
	return tokensFlipping;
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.clearTokenPlaceholders = function() {
	for(var x = 0; x < this.constants.boardSize; x++) {
		for(var y = 0; y < this.constants.boardSize; y++) {
			this.cellsByPosition[x][y].setTokenPlaceholder(false);
		}
	}
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.setWaitCursor = function() {
	this.cursor.setCurrentState("wait");
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.setActiveCursor = function() {
	this.cursor.setCurrentState("active");
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.onProcessGameMenuInputEvent = function() {
	var event = this.gameMenuInputEventQueue.shift();
	if(event !== undefined) {
		var eventType = event.type;
		switch(eventType) {
		case "setHumanPlayerBlack": {
			this.startingState.humanPlayer = 1;
			break;
		}
		case "setHumanPlayerWhite": {
			this.startingState.humanPlayer = 2;
			break;
		}
		case "start": {
			this.splashScreen.exitSplashScreen();
			this.gameOverScreen.exitGameOverScreen();
			this.setStartNewGameState();
			this.updateBoardForNewGameState();
			return;
		}
		case "undo": {
			if(this.playerHasWhereToUndo()) {
				while(this.gameStateHistory.goBackOne()) {
					if(this.gameStateHistory.getCurrent().getPlayerToMove() == this.minimax.getHumanPlayer()) {
						break;
					}
				}
				this.minimax.setStartingGameState(this.gameStateHistory.getCurrent());
				this.updateBoardForNewGameState();
				this.gameOverScreen.exitGameOverScreen();
				this.switchState("getNextHumanPossibleMoves");
			} else {
				this.switchState(this.lastDifferentState);
			}
			return;
		}
		case "selectAIConfigurationName": {
			var selectedAIConfigurationName = event.value;
			this.startingState.aiConfigurationName = selectedAIConfigurationName;
			break;
		}
		default: {
			this.switchState(this.lastDifferentState);
			return;
		}
		}
	}
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.onIdleOnGameEnded = function(context2d) {
	if(this.gameMenuInputEventQueue.length > 0) {
		this.onProcessGameMenuInputEvent();
	} else {
		this.gameOverScreen.displayGameOverScreen(this.minimax, this.gameStateHistory.getCurrent());
	}
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.onIdleOnDemoEnded = function(context2d) {
	if(this.gameMenuInputEventQueue.length > 0) {
		this.demoOverScreen.exitDemoOverScreen();
		this.switchState("processGameMenuInputEvent");
	} else {
		this.demoOverScreen.displayDemoOverScreen();
	}
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.setStartNewGameState = function() {
	var aiConfiguration = null;
	for(var i = 0; i < this.aiConfigurations.length; i++) {
		if(this.aiConfigurations[i].name === this.startingState.aiConfigurationName) {
			aiConfiguration = this.aiConfigurations[i];
			break;
		}
	}
	
	var gameEvaluator = this.aiConfigurationToReversiGameEvaluatorConverter.convert(3 - this.startingState.humanPlayer, aiConfiguration);
	this.gameStateHistory = new LS_REVERSI.GameStateHistory();
	this.gameStateHistory.advanceFromCurrent(this.startingGameState);
	this.minimax.setStartingGameState(this.gameStateHistory.getCurrent());
	this.minimax.setGameEvaluator(gameEvaluator);
	if (this.minimax.getHumanPlayer() == 1) {
		this.humanPlayer = 1;
		this.aiPlayer = 2;
		this.switchState("getNextHumanPossibleMoves");
	} else {
		this.humanPlayer = 2;
		this.aiPlayer = 1;
		this.setWaitCursor();
		this.switchState("getAIMove");
	}
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.updateBoardForNewGameState = function() {
	for ( var x = 0; x < this.constants.boardSize; x++) {
		for ( var y = 0; y < this.constants.boardSize; y++) {
			var tokenType = this.gameStateHistory.getCurrent().getTokenTypeAt(x, y);
			if (tokenType !== this.constants.tokenTypeEmpty) {
				this.tokensByPosition[x][y].placeOnBoard(tokenType);
			} else {
				this.tokensByPosition[x][y].removeFromBoard();
			}
		}
	}
	this.clearTokenPlaceholders();
	this.notifyScoreChangeListener();
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.notifyScoreChangeListener = function() {
	if(this.scoreChangeListener != null) {
		var gameState = this.gameStateHistory.getCurrent();
		var gameEvaluator = this.minimax.gameEvaluator;
		var whiteScore =  gameEvaluator.getTokenCount(this.constants.tokenTypeWhite, gameState);
		var blackScore =  gameEvaluator.getTokenCount(this.constants.tokenTypeBlack, gameState);
		var humanPlayer = this.minimax.getHumanPlayer();
		if(humanPlayer == 1) {
			this.scoreChangeListener.onScoreChange(blackScore, whiteScore);
		} else {
			this.scoreChangeListener.onScoreChange(whiteScore, blackScore);
		}
	}
};

/**
 * @private
 */
LS_REVERSI.GameFiniteStateMachine.prototype.playerHasWhereToUndo = function() {
	var historyEntries = this.gameStateHistory.getEntries();
	var entriesOwnedByHuman = 0;
	for(var i = 0; i <= this.gameStateHistory.getCurrentLocation(); i++) {
		var gameState = historyEntries[i];
		if(gameState.getPlayerToMove() == this.minimax.getHumanPlayer()) {
			entriesOwnedByHuman++;
		}
	}
	return entriesOwnedByHuman >= 2;
};