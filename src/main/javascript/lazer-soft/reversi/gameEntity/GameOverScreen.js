var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.GameOverScreen}
 */
LS_REVERSI.GameOverScreen = function(canvasSize, constants) {
	LS_GAME_LOOP.Entity.call(this, 0, 0, canvasSize, canvasSize, 5000);
	this.canvasSize = canvasSize;
	this.constants = constants;
	
	this.displayed = false;
	this.minimax = null;
	this.gameState = null;
};

LS_REVERSI.GameOverScreen.prototype = Object.create(LS_GAME_LOOP.Entity.prototype);
LS_REVERSI.GameOverScreen.prototype.constructor = LS_REVERSI.GameOverScreen;

/**
 * @public
 */
LS_REVERSI.GameOverScreen.prototype.displayGameOverScreen = function(minimax, gameState) {
	this.minimax = minimax;
	this.gameState = gameState;
	this.displayed = true;
};

/**
 * @public
 */
LS_REVERSI.GameOverScreen.prototype.exitGameOverScreen = function() {
	this.displayed = false;
};


/**
 * @public
 */
LS_REVERSI.GameOverScreen.prototype.processInput = function(inputEvent) {};

/**
 * @public
 */
LS_REVERSI.GameOverScreen.prototype.updateState = function() {
	
};

/**
 * @public
 * @param {CanvasRenderingContext2D} context2d
 */
LS_REVERSI.GameOverScreen.prototype.updateGraphics = function(context2d) {
	if(this.displayed) {
		this.drawGameEndedCover(context2d);
		this.drawGameEndedMessage(context2d);
	}
};

/**
 * @private
 */
LS_REVERSI.GameOverScreen.prototype.drawGameEndedCover = function(context2d) {
	var globalAlphaOrig = context2d.globalAlpha;	
	context2d.globalAlpha = 0.5;
	context2d.fillRect(0, 0, this.canvasSize, this.canvasSize);
	context2d.globalAlpha = globalAlphaOrig;
};

/**
 * @private
 */
LS_REVERSI.GameOverScreen.prototype.drawGameEndedMessage = function(context2d) {
	var fillStyleOrig = context2d.fillStyle;
	var fontOrig = context2d.font;
	var textBaselineOrig = context2d.textBaseline;
	var shadowColorOrig = context2d.shadowColor;
	var shadowOffsetXOrig = context2d.shadowOffsetX;
	var shadowOffsetYOrig = context2d.shadowOffsetY;
	var varshadowBlurOrig = context2d.shadowBlur;
		
	var textHeight = (this.canvasSize / 22);	
	context2d.fillStyle = "#CDFEC8";
	context2d.font = textHeight + "px monospace";
	context2d.textBaseline = "top";
	context2d.shadowColor = "green";
	context2d.shadowOffsetX = this.canvasSize / 150;
	context2d.shadowOffsetY = this.canvasSize / 150;
	context2d.shadowBlur = blur;
	
	var text = this.getGameEndedMessage();
	var blur = this.canvasSize / 150;
	var textWidth = context2d.measureText(text).width;
	context2d.fillText(text, (this.canvasSize - textWidth) / 2, (this.canvasSize - textHeight) / 2);
	
	context2d.fillStyle = fillStyleOrig;
	context2d.font = fontOrig;
	context2d.textBaseline = textBaselineOrig;
	context2d.shadowColor = shadowColorOrig;
	context2d.shadowOffsetX = shadowOffsetXOrig;
	context2d.shadowOffsetY = shadowOffsetYOrig;
	context2d.shadowBlur = varshadowBlurOrig;
};

/**
 * @private
 */
LS_REVERSI.GameOverScreen.prototype.getGameEndedMessage = function() {
	var gameEvaluator = this.minimax.gameEvaluator;
	var scorePerPlayer = new Array();	
	scorePerPlayer[1] = gameEvaluator.getTokenCount(this.constants.tokenTypeBlack, this.gameState);
	scorePerPlayer[2] = gameEvaluator.getTokenCount(this.constants.tokenTypeWhite, this.gameState);
	
	var gameOutcome = this.minimax.getGameOutcome(this.gameState);
	
	var message = "GAME OVER. ";
	var humanPlayer = this.minimax.getHumanPlayer();
	var cpuPlayer = this.minimax.getCpuPlayer();
	if(gameOutcome == 0) {
		message += "It's a draw: " + scorePerPlayer[1] + " / " + scorePerPlayer[2] + ".";
	} else if(this.minimax.getHumanPlayer() == gameOutcome) {
		message += "You have won: " + scorePerPlayer[humanPlayer] + " / " + scorePerPlayer[cpuPlayer] + ".";
	} else if(this.minimax.getCpuPlayer() == gameOutcome) {
		message += "Opponent has won: " + scorePerPlayer[cpuPlayer] + " / " + scorePerPlayer[humanPlayer] + ".";
	} else {
		message = "Can't determine game outcome :(";
	}
	return message;
};