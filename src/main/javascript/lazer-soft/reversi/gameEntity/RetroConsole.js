var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};
/**
 * @public
 * @class
 * @returns {LS_REVERSI.RetroConsole}
 */
LS_REVERSI.RetroConsole = function(x, y, width, imageLoader, frameRenderer) {
	this.heightToWidthRatio = 408 / 538;
	this.textStartX = x + width / 7.5;
	this.textStartY = y + width / 7.5;
	this.textSize = width / 40;
	
	LS_GAME_LOOP.Entity.call(this, x, y, width, width * this.heightToWidthRatio);
	this.backgroundFrame = new LS_GAME_LOOP.AnimationFrame(
			imageLoader.getImage("retro-console-background"),
			new LS_GAME_LOOP.ImageDrawProperties(this.width, this.height));
	this.frameRenderer = frameRenderer;
	
	this.textLines = new Array();
	this.displayedTextLines = new Array();
	
	this.lastTimeTextUpdatedMs = new Date().getTime();
	this.drawCharacterDurationMs = 1;
	
	this.cursorBlinkDirationMs = 500;
	this.lastTimeCursorBlinkMs = new Date().getTime();
	this.cursorBlinkOn = true;	
	this.cursorChar = "|"; 
	
	//TODO enforce these:	
	this.maxLinesOnScreen = 21;
	this.maxCharsPerLine = 50;
	
};

LS_REVERSI.RetroConsole.prototype = Object.create(LS_GAME_LOOP.Entity.prototype);
LS_REVERSI.RetroConsole.prototype.constructor = LS_REVERSI.RetroConsole;

/**
 * @public
 */
LS_REVERSI.RetroConsole.prototype.addTextLine = function(textLine) {
	this.textLines[this.textLines.length] = textLine;
};

/**
 * @public
 */
LS_REVERSI.RetroConsole.prototype.clearScreen = function() {
	this.textLines = new Array();
	this.displayedTextLines = new Array();
};

/**
 * @public
 */
LS_REVERSI.RetroConsole.prototype.processInput = function(inputEvent) {};

/**
 * @public
 */
LS_REVERSI.RetroConsole.prototype.updateState = function() {
	var curTimeMs = new Date().getTime();
	
	if(curTimeMs - this.lastTimeTextUpdatedMs >= this.drawCharacterDurationMs) {
		var charsToAdd = (curTimeMs - this.lastTimeTextUpdatedMs) / this.drawCharacterDurationMs;
		for(var i = 0; i < charsToAdd; i++) {
			this.addCharToDisplay();	
			this.lastTimeTextUpdatedMs += this.drawCharacterDurationMs;
		}
	}
	
	if(curTimeMs - this.lastTimeCursorBlinkMs >= this.cursorBlinkDirationMs) {
		this.cursorBlinkOn = !this.cursorBlinkOn;
		this.lastTimeCursorBlinkMs = curTimeMs;
	}
};

/**
 * @public
 * @param {CanvasRenderingContext2D} context2d
 */
LS_REVERSI.RetroConsole.prototype.updateGraphics = function(context2d) {
	this.frameRenderer.draw(context2d, this.x, this.y, this.backgroundFrame);
	context2d.font = this.textSize + "px monospace";
	context2d.fillStyle = "#CDFEC8";
	
	var startLineIdx = this.displayedTextLines.length - this.maxLinesOnScreen;
	if(startLineIdx < 0) {
		startLineIdx = 0;
	}
	for(var i = startLineIdx; i < this.displayedTextLines.length; i++) {
		var textToDisplay = this.displayedTextLines[i];
		if(i == this.displayedTextLines.length - 1) {
			if(this.cursorBlinkOn) {
				textToDisplay = textToDisplay + this.cursorChar;
			}
		}
		context2d.fillText(textToDisplay, this.textStartX, this.textStartY + (this.maxLinesOnScreen - this.displayedTextLines.length + i) * this.textSize);
	}
};

/**
 * @public
 */
LS_REVERSI.RetroConsole.prototype.addCharToDisplay = function() {
	if(this.textLines.length == 0) {
		return;
	}
	
	var lastDrawnLineIdx = Math.max(this.displayedTextLines.length - 1, 0);
	var lastDisplayedLine = this.displayedTextLines[lastDrawnLineIdx] === undefined ? "" : this.displayedTextLines[lastDrawnLineIdx];
	if(this.textLines[lastDrawnLineIdx].length > lastDisplayedLine.length) {			
		this.displayedTextLines[lastDrawnLineIdx] = lastDisplayedLine + this.textLines[lastDrawnLineIdx].charAt(lastDisplayedLine.length);
	} else {
		if(this.textLines.length > this.displayedTextLines.length) {
			this.displayedTextLines[this.displayedTextLines.length] = this.textLines[this.displayedTextLines.length].charAt(0);
		}
	}
};