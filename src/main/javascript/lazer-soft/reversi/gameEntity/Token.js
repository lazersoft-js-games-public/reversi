var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.Token}
 */
LS_REVERSI.Token = function(cellX, cellY, width, 
		flipAnimationBlackToWhite, flipAnimationWhiteToBlack, frameRenderer,
		constants,
		onBoard) {
	LS_GAME_LOOP.Entity.call(this, cellX * width, cellY * width, width, width, 20);
	this.cellX = cellX;
	this.cellY = cellY;
	this.flipAnimationBlackToWhite = flipAnimationBlackToWhite;
	this.flipAnimationWhiteToBlack = flipAnimationWhiteToBlack;
	this.frameRenderer = frameRenderer;
	this.tokenType = null;
	this.constants = constants;
	this.onBoard = onBoard === undefined ? false : true;
	
	this.shouldStartFlipping = false;
	this.flipping = false;
	this.currentAnimation = this.tokenType === this.constants.tokenTypeWhite ? this.flipAnimationWhiteToBlack : this.flipAnimationBlackToWhite;
	
	this.flippedHighlight = false;
	this.placedHighlight = false;
};

LS_REVERSI.Token.prototype = Object.create(LS_GAME_LOOP.Entity.prototype);
LS_REVERSI.Token.prototype.constructor = LS_REVERSI.Token;

/**
 * @public
 */
LS_REVERSI.Token.prototype.placeOnBoard = function(tokenType) {
	this.setTokenType(tokenType);
	this.onBoard = true;
};

/**
 * @public
 */
LS_REVERSI.Token.prototype.setTokenType = function(tokenType) {
	this.tokenType = tokenType;
	this.currentAnimation = this.tokenType === this.constants.tokenTypeWhite ? this.flipAnimationWhiteToBlack : this.flipAnimationBlackToWhite;
};

/**
 * @public
 */
LS_REVERSI.Token.prototype.removeFromBoard = function() {
	this.onBoard = false;
};

/**
 * @public
 */
LS_REVERSI.Token.prototype.startFlipping = function() {
	if(!this.onBoard) {
		return false;
	}
	
	if(this.flipping) {
		return false;
	}
	this.shouldStartFlipping = true;
	return true;
};

/**
 * @public
 */
LS_REVERSI.Token.prototype.processInput = function(inputEvent) {};

/**
 * @public
 */
LS_REVERSI.Token.prototype.updateState = function() {
	if(!this.onBoard) {
		return;
	}
	
	if(this.shouldStartFlipping) {
		this.currentAnimation.reset();
		this.currentAnimation.start();
		this.shouldStartFlipping = false;
		this.flipping = true;
	} else if(this.flipping) {
		if(!this.currentAnimation.isEnded()) {
			this.currentAnimation.onTick();
		} else {
			this.flipping = false;
			this.tokenType = this.tokenType.opponentTokenType;
			this.currentAnimation = this.tokenType === this.constants.tokenTypeWhite ? this.flipAnimationWhiteToBlack : this.flipAnimationBlackToWhite;
		}
	}
};

/**
 * @public
 * @param {CanvasRenderingContext2D} context2d
 */
LS_REVERSI.Token.prototype.updateGraphics = function(context2d) {
	if(!this.onBoard) {
		return;
	}
	
	if(this.flippedHighlight) {
		this.drawEncompassingRectangle("yellow", 1, 2, context2d);
	} else if(this.placedHighlight) {
		this.drawEncompassingRectangle("red", 2, 2,	 context2d);
	}
	
	var currentFrame;
	if(this.flipping) {
		currentFrame = this.currentAnimation.getCurrentAnimationFrame();
	} else {
		currentFrame = this.currentAnimation.getFirstAnimationFrame();
	}
	this.frameRenderer.draw(context2d, this.x, this.y, currentFrame);
};

/**
 * @public
 */
LS_REVERSI.Token.prototype.setJustFlipped = function(justFlipped) {
	this.flippedHighlight = justFlipped;
};

/**
 * @public
 */
LS_REVERSI.Token.prototype.setJustPlaced = function(justPlaced) {
	this.placedHighlight = justPlaced;
};

/**
 * @private
 */
LS_REVERSI.Token.prototype.drawEncompassingRectangle = function(color, lineWidth, padding, context2d) {
	context2d.beginPath();
	context2d.lineWidth = lineWidth;
	context2d.strokeStyle = color;
	context2d.rect(this.x + padding, this.y + padding, this.width - 2 * padding, this.height - 2 * padding); 
	context2d.stroke();
};
