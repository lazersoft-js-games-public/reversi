var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.BoardCell}
 */
LS_REVERSI.BoardCell = function(cellX, cellY, width, highlighFrame, tokenPlaceHolderFrame, frameRenderer) {
	LS_GAME_LOOP.Entity.call(this, cellX * width, cellY * width, width, width, 15);
	this.cellX = cellX;
	this.cellY = cellY;
	this.highlighFrame = highlighFrame;
	this.tokenPlaceHolderFrame = tokenPlaceHolderFrame;
	this.frameRenderer = frameRenderer;
	
	this.leftX = this.x;
	this.rightX = this.x + width;
	this.upY = this.y;
	this.downY = this.y + width;
	
	this.mouseOver = false;
	this.tokenPlaceholder = false;
};

LS_REVERSI.BoardCell.prototype = Object.create(LS_GAME_LOOP.Entity.prototype);
LS_REVERSI.BoardCell.prototype.constructor = LS_REVERSI.BoardCell;

/**
 * @public
 */
LS_REVERSI.BoardCell.prototype.setTokenPlaceholder = function(tokenPlaceholder) {
	this.tokenPlaceholder = tokenPlaceholder;
};

/**
 * @public
 */
LS_REVERSI.BoardCell.prototype.processInput = function(inputEvent) {
	if(inputEvent.canvasX >= this.leftX && 
			inputEvent.canvasX < this.rightX && 
			inputEvent.canvasY >= this.upY && 
			inputEvent.canvasY < this.downY) {
		this.mouseOver = true;
	} else {
		this.mouseOver = false;
	}
};

/**
 * @public
 */
LS_REVERSI.BoardCell.prototype.updateState = function() {
	
};

/**
 * @public
 * @param {CanvasRenderingContext2D} context2d
 */
LS_REVERSI.BoardCell.prototype.updateGraphics = function(context2d) {
	if(this.mouseOver) {
		this.frameRenderer.draw(context2d, this.x, this.y, this.highlighFrame);
	}	
	if(this.tokenPlaceholder) {
		this.frameRenderer.draw(context2d, this.x, this.y, this.tokenPlaceHolderFrame);
	}
};