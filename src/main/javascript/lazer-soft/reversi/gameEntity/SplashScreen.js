var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.SplashScreen}
 */
LS_REVERSI.SplashScreen = function(canvasSize, splashScreenFrame, frameRenderer) {
	LS_GAME_LOOP.Entity.call(this, 0, 0, canvasSize, canvasSize, 5000);
	this.canvasSize = canvasSize;
	this.splashScreenFrame = splashScreenFrame;
	this.frameRenderer = frameRenderer;
	
	this.displayed = true;
	this.fading = false;
	this.transparency = 1;
	this.transparencyDecrement = 0.055;
};

LS_REVERSI.SplashScreen.prototype = Object.create(LS_GAME_LOOP.Entity.prototype);
LS_REVERSI.SplashScreen.prototype.constructor = LS_REVERSI.SplashScreen;

/**
 * @public
 */
LS_REVERSI.SplashScreen.prototype.exitSplashScreen = function() {
	this.fading = true;
};


/**
 * @public
 */
LS_REVERSI.SplashScreen.prototype.processInput = function(inputEvent) {
//	if(this.displayed && !this.fading) {
//		if(inputEvent instanceof LS_GAME_LOOP.CanvasInputEvent && inputEvent.type !== "mousemove" && inputEvent.type !== "mouseout") {
//			this.fading = true;
//		}
//	}
};

/**
 * @public
 */
LS_REVERSI.SplashScreen.prototype.updateState = function() {
	if(this.displayed) {
		if(this.fading) {
			this.transparency -= this.transparencyDecrement;
			if(this.transparency <= 0) {
				this.displayed = false;
				this.isFading = false;
				this.transparency = 1;
			}
		}
	}
	
};

/**
 * @public
 * @param {CanvasRenderingContext2D} context2d
 */
LS_REVERSI.SplashScreen.prototype.updateGraphics = function(context2d) {
	if(this.displayed) {
		if(this.transparency != 1) {
			context2d.globalAlpha = this.transparency;			
		}
		this.frameRenderer.draw(context2d, this.x, this.y, this.splashScreenFrame);
		if(this.transparency != 1) {
			context2d.globalAlpha = 1;			
		}
	}
};