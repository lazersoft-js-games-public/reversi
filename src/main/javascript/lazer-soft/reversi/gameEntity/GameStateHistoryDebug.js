var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.GameStateHistoryDebug}
 */
LS_REVERSI.GameStateHistoryDebug = function(x, y, gameStateHistory, width) {
	LS_GAME_LOOP.Entity.call(this, 
			x, y, width, 200, Number.POSITIVE_INFINITY);
	this.gameStateHistory = gameStateHistory;
	//must be an even number:
	this.maxEntriesToDisplay = 10;
};

LS_REVERSI.GameStateHistoryDebug.prototype = Object.create(LS_GAME_LOOP.Entity.prototype);
LS_REVERSI.GameStateHistoryDebug.prototype.constructor = LS_REVERSI.GameStateHistoryDebug;

/**
 * @public
 */
LS_REVERSI.GameStateHistoryDebug.prototype.processInput = function(inputEvent) {};

/**
 * @public
 */
LS_REVERSI.GameStateHistoryDebug.prototype.updateState = function() {};

/**
 * @public
 * @param {CanvasRenderingContext2D} context2d
 */
LS_REVERSI.GameStateHistoryDebug.prototype.updateGraphics = function(context2d) {
	var y = this.y + 20;
	var x = null;
	var displayedCount = 0
	for(var i = Math.max(0, this.gameStateHistory.getCurrentLocation() - this.maxEntriesToDisplay / 2); 
			i < Math.min(this.gameStateHistory.getCurrentLocation() + this.maxEntriesToDisplay / 2, this.gameStateHistory.getEntries().length);
			i++) {
		
		context2d.fillStyle="black";
		
		x = this.x + displayedCount * 15;
		displayedCount++;
		var gameState = this.gameStateHistory.getEntries()[i];
		var playerToMove = gameState.playerToMove;		
		if(playerToMove == 1) {
			context2d.fillText("B", x + 2, y);
		} else if(playerToMove == 2) {
			context2d.fillText("W", x, y);
		} else {
			context2d.fillText("?", x, y);
		}
		
		context2d.fillStyle="#D3D3D3";
		var lastMove = gameState.getLastMove();
		if(lastMove !== undefined) {
			var playerThatMoved = lastMove.player;		
			if(playerThatMoved == 1) {
				context2d.fillText("B", x + 2, y + 14);
			} else if(playerThatMoved == 2) {
				context2d.fillText("W", x + 2, y + 14);
			} else {
				context2d.fillText("?", x + 2, y + 14);
			}
		} else {
			context2d.fillText("?", x + 2, y + 14);
		}
		
		context2d.fillStyle="black";
		context2d.fillText(gameState.getTokenCount(), x + 2, y + 28);
		
		if(i == this.gameStateHistory.getCurrentLocation()) {
			context2d.fillText("^", x + 3, y + 42);
		}
	}
	context2d.fillText("location: " + this.gameStateHistory.getCurrentLocation() + "/" + this.gameStateHistory.getSize(), 0, y + 56);
};