var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.DemoOverScreen}
 */
LS_REVERSI.DemoOverScreen = function(canvasSize, constants) {
	LS_GAME_LOOP.Entity.call(this, 0, 0, canvasSize, canvasSize, 5000);
	this.canvasSize = canvasSize;
	this.constants = constants;
	
	this.displayed = false;
};

LS_REVERSI.DemoOverScreen.prototype = Object.create(LS_GAME_LOOP.Entity.prototype);
LS_REVERSI.DemoOverScreen.prototype.constructor = LS_REVERSI.DemoOverScreen;

/**
 * @public
 */
LS_REVERSI.DemoOverScreen.prototype.displayDemoOverScreen = function() {
	this.displayed = true;
};

/**
 * @public
 */
LS_REVERSI.DemoOverScreen.prototype.exitDemoOverScreen = function() {
	this.displayed = false;
};


/**
 * @public
 */
LS_REVERSI.DemoOverScreen.prototype.processInput = function(inputEvent) {};

/**
 * @public
 */
LS_REVERSI.DemoOverScreen.prototype.updateState = function() {
	
};

/**
 * @public
 * @param {CanvasRenderingContext2D} context2d
 */
LS_REVERSI.DemoOverScreen.prototype.updateGraphics = function(context2d) {
	if(this.displayed) {
		this.drawDemoEndedCover(context2d);
		this.drawDemoEndedMessage(context2d);
	}
};

/**
 * @private
 */
LS_REVERSI.DemoOverScreen.prototype.drawDemoEndedCover = function(context2d) {
	var globalAlphaOrig = context2d.globalAlpha;	
	context2d.globalAlpha = 0.5;
	context2d.fillRect(0, 0, this.canvasSize, this.canvasSize);
	context2d.globalAlpha = globalAlphaOrig;
};

/**
 * @private
 */
LS_REVERSI.DemoOverScreen.prototype.drawDemoEndedMessage = function(context2d) {
	var fillStyleOrig = context2d.fillStyle;
	var fontOrig = context2d.font;
	var textBaselineOrig = context2d.textBaseline;
	var shadowColorOrig = context2d.shadowColor;
	var shadowOffsetXOrig = context2d.shadowOffsetX;
	var shadowOffsetYOrig = context2d.shadowOffsetY;
	var varshadowBlurOrig = context2d.shadowBlur;
		
	var textHeight = (this.canvasSize / 22);	
	context2d.fillStyle = "#CDFEC8";
	context2d.font = textHeight + "px monospace";
	context2d.textBaseline = "top";
	context2d.shadowColor = "green";
	context2d.shadowOffsetX = this.canvasSize / 150;
	context2d.shadowOffsetY = this.canvasSize / 150;
	context2d.shadowBlur = blur;
	
	var text = this.getDemoEndedMessage();
	var blur = this.canvasSize / 150;
	var textWidth = context2d.measureText(text).width;
	context2d.fillText(text, (this.canvasSize - textWidth) / 2, (this.canvasSize - textHeight) / 2);
	
	context2d.fillStyle = fillStyleOrig;
	context2d.font = fontOrig;
	context2d.textBaseline = textBaselineOrig;
	context2d.shadowColor = shadowColorOrig;
	context2d.shadowOffsetX = shadowOffsetXOrig;
	context2d.shadowOffsetY = shadowOffsetYOrig;
	context2d.shadowBlur = varshadowBlurOrig;
};

/**
 * @private
 */
LS_REVERSI.DemoOverScreen.prototype.getDemoEndedMessage = function() {
	var message = "DEMO ENDED. ";
	//TODO
	return message;
};