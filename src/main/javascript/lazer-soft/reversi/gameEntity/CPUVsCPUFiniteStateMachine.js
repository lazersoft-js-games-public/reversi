var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.CPUVsCPUFiniteStateMachine}
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine = function(tokensByPosition, cellsByPosition, gameState, cpuVsCpuRunner, constants, gameStateHistoryInputEventQueue, gameEndedListener, waitForTokensToFlip) {
	LS_GAME_LOOP.Entity.call(0, 0, 0, 0, Number.NEGATIVE_INFINITY);
	
	this.tokensByPosition = tokensByPosition;
	this.cellsByPosition = cellsByPosition;
	this.cpuVsCpuRunner = cpuVsCpuRunner;
	this.constants = constants;
	this.gameStateHistoryInputEventQueue = gameStateHistoryInputEventQueue;
	this.gameEndedListener = gameEndedListener;
	this.waitForTokenToFlip = waitForTokensToFlip === undefined ? true : waitForTokensToFlip;
	
	this.gameStateHistory = new LS_REVERSI.GameStateHistory();
	this.gameStateHistory.advanceFromCurrent(gameState);
	
	this.cellSize = this.cellsByPosition[0][0].width;
	this.nextMove = null;
	this.nextAvailableMoves = new Array();
	
	this.states = ["waitForTokensToFlip", "getAIMove", "waitForDraw", "applyAIMove", "checkGameEnded", "processGameStateHistoryInputEvent"];
	this.state = null;
	
	this.stateHistory = new Array();
	this.stateHistoryMaxSize = 15;
	
	this.drawsSinceWaiting = 0;
	this.maxWaitForDrawCount = 2;
	
	this.moveCountByPlayerNumber = new Array();
	this.moveCountByPlayerNumber[1] = 0;
	this.moveCountByPlayerNumber[2] = 0;
	
	this.totalMoveDurationMsByPlayerNumber = new Array();
	this.totalMoveDurationMsByPlayerNumber[1] = 0;
	this.totalMoveDurationMsByPlayerNumber[2] = 0;
	
	this.gameAdvancementPaused = true;
	
	this.initState();
};

LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype = Object.create(LS_GAME_LOOP.Entity.prototype);
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.constructor = LS_REVERSI.CPUVsCPUFiniteStateMachine;

/**
 * @public
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.processInput = function(inputEvent) {
	if(inputEvent instanceof LS_REVERSI.GameStateHistoryInputEvent)	{
		this.gameStateHistoryInputEvents.push(inputEvent);
	}
};

/**
 * @public
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.updateState = function() {
	//poor man's FSM:
	switch(this.state) {
	case "waitForTokensToFlip": {
		this.onWaitForTokensToFlip();
		break;
	}
	case "getAIMove": {
		this.onGetAIMove();
		break;
	}
	case "applyAIMove": {
		this.onApplyAIMove();
		break;
	}
	case "waitForDraw": {
		this.onWaitForDraw();
		break;
	}
	case "processGameStateHistoryInputEvent": {
		this.onProcessGameStateHistoryInputEvent();
		break;
	}
	}
};

/**
 * @public
 * @param {CanvasRenderingContext2D} context2d
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.updateGraphics = function(context2d) {
	if(this.state === "waitForDraw") {
		this.drawsSinceWaiting++;
	}
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.initState = function() {
	for(var x = 0; x < this.constants.boardSize; x++) {
		for(var y = 0; y < this.constants.boardSize; y++) {
			var tokenType = this.gameStateHistory.getCurrent().getTokenTypeAt(x, y);
			if(tokenType !== this.constants.tokenTypeEmpty) {
				this.tokensByPosition[x][y].placeOnBoard(tokenType);
			}
		}
	}
	this.switchState("processGameStateHistoryInputEvent");
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.onWaitForTokensToFlip = function() {
	if(!this.areTokensFlipping() || !this.waitForTokenToFlip) {
		this.switchState("processGameStateHistoryInputEvent");
	}
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.onGetAIMove = function() {
	if(this.gameStateHistory.getCurrent().isGameEnded()) {
		this.gameEndedListener.onGameEnded();
		this.switchState("processGameStateHistoryInputEvent");
		return;
	}
	var st = new Date().getTime();	
	var newGameState = this.cpuVsCpuRunner.advanceGame();
	this.gameStateHistory.advanceFromCurrent(newGameState);	
	var et = new Date().getTime();
	var durationMs = et - st;	
	var lastMove = this.gameStateHistory.getCurrent().getLastMove();
	var playerNumber = this.gameStateHistory.getCurrent().getLastMove().player;
	this.moveCountByPlayerNumber[playerNumber] = this.moveCountByPlayerNumber[playerNumber] + 1;
	this.totalMoveDurationMsByPlayerNumber[playerNumber] = this.totalMoveDurationMsByPlayerNumber[playerNumber] + durationMs;
	
	this.drawsSinceWaiting = 0;
	this.switchState("waitForDraw");
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.onApplyAIMove = function() {
	var lastMove = this.gameStateHistory.getCurrent().lastMove;
	
	this.updateFlippedPlacedHighlights(lastMove);
	
	var tokenType = lastMove.player == 1 ? this.constants.tokenTypeBlack : this.constants.tokenTypeWhite; 
	
	var placedToken = this.tokensByPosition[lastMove.position.x][lastMove.position.y];	
	placedToken.placeOnBoard(tokenType);
	placedToken.setJustPlaced(true);
	
	for(var i = 0; i < lastMove.flippedTokensPositions.length; i++) {
		var pos = lastMove.flippedTokensPositions[i];
		var flippedToken = this.tokensByPosition[pos.x][pos.y]
		flippedToken.startFlipping();
	}
	this.switchState("waitForTokensToFlip");
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.onCheckGameEnded = function() {
	this.switchState("processGameStateHistoryInputEvent");
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.onWaitForDraw = function() {
	if(this.drawsSinceWaiting >= this.maxWaitForDrawCount) {
		this.drawsSinceWaiting = 0;
		this.switchState("applyAIMove");
	}
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.onProcessGameStateHistoryInputEvent = function() {
	var event = this.gameStateHistoryInputEventQueue.shift();
	if(event !== undefined) {
		var eventType = event.type;
		switch(eventType) {
		case "reset": {
			while(this.gameStateHistory.getCurrentLocation() > 0) {
				this.gameStateHistory.goBackOne();
			}
			this.setHistoricGameState();
			break;
		}
		case "back": {
			this.gameStateHistory.goBackOne();
			this.setHistoricGameState();
			break;
		}
		case "pauseResume": {
			this.gameAdvancementPaused = !this.gameAdvancementPaused;	
			break;
		}
		case "forward": {
			this.gameStateHistory.goForwardOne();
			this.setHistoricGameState();
			break;
		}
		}
	}
	
	if(this.gameAdvancementPaused) {
		this.switchState("processGameStateHistoryInputEvent");		
	} else {
		this.switchState("getAIMove");
	}	
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.setHistoricGameState = function() {
	var gameState = this.gameStateHistory.getCurrent();
	this.updateRunnerGameState(gameState);
	this.clearTokenPlaceholders();
	this.updateTokensStatus(gameState);
	this.updateFlippedPlacedHighlights(gameState.getLastMove());
};


/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.switchState = function(newState) {
	this.state = newState;
	this.updateStateHistory(newState);
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.updateStateHistory = function(currentState) {
	this.stateHistory.push(currentState);
	if(this.stateHistory.length > this.stateHistoryMaxSize) {
		this.stateHistory.shift();
	}
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.areTokensFlipping = function() {
	var tokensFlipping = false;
	var lastMove = this.gameStateHistory.getCurrent().getLastMove();
	for(var i = 0; i < lastMove.flippedTokensPositions.length; i++) {
		var pos = lastMove.flippedTokensPositions[i];
		var token = this.tokensByPosition[pos.x][pos.y];
		if(token.flipping || token.shouldStartFlipping) {
			tokensFlipping = true;
			break;
		}
	}
	return tokensFlipping;
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.clearTokenPlaceholders = function() {
	for(var x = 0; x < this.constants.boardSize; x++) {
		for(var y = 0; y < this.constants.boardSize; y++) {
			this.cellsByPosition[x][y].setTokenPlaceholder(false);
		}
	}
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.updateTokensStatus = function(gameState) {
	if(gameState === undefined) {
		return;
	}
	
	for(var x = 0; x < this.constants.boardSize; x++) {
		for(var y = 0; y < this.constants.boardSize; y++) {
			var token = this.tokensByPosition[x][y];
			var newTokenType = gameState.getTokenTypeAt(x, y);
			if(newTokenType === this.constants.tokenTypeEmpty) {
				token.removeFromBoard();
			} else {
				token.placeOnBoard(newTokenType);
			}
		}
	}
	
	var lastMove = gameState.lastMove;
	if(lastMove !== undefined) {
		var tokenType = lastMove.player == 1 ? this.constants.tokenTypeBlack : this.constants.tokenTypeWhite; 
		this.tokensByPosition[lastMove.position.x][lastMove.position.y].placeOnBoard(tokenType);
	}
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.updateRunnerGameState = function(gameState) {
	this.cpuVsCpuRunner.setStartingGameState(gameState);
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.updateFlippedPlacedHighlights = function(lastMove) {
	this.clearFlippedPlacedHighlights();
	this.setFlippedPlacedHighlights(lastMove);
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.clearFlippedPlacedHighlights = function() {
	for(var x = 0; x < this.constants.boardSize; x++) {
		for(var y  = 0; y < this.constants.boardSize; y++) {
			this.tokensByPosition[x][y].setJustFlipped(false);
			this.tokensByPosition[x][y].setJustPlaced(false);
		}		
	}
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFiniteStateMachine.prototype.setFlippedPlacedHighlights = function(lastMove) {
	if(lastMove === undefined) {
		return;
	}
	
	var placedToken = this.tokensByPosition[lastMove.position.x][lastMove.position.y];	
	placedToken.setJustPlaced(true);
	
	for(var i = 0; i < lastMove.flippedTokensPositions.length; i++) {
		var pos = lastMove.flippedTokensPositions[i];
		var flippedToken = this.tokensByPosition[pos.x][pos.y]
		flippedToken.setJustFlipped(true);
	}
};
