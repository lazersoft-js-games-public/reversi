var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.EntityFactory}
 */
LS_REVERSI.EntityFactory = function(theme, boardWidth, frameRenderer, flipAnimationFramesCount, constants, canvasId, moveHelper) {
	this.theme = theme;
	this.boardWidth = boardWidth;
	this.frameRenderer = frameRenderer;
	this.flipAnimationFramesCount = flipAnimationFramesCount;
	this.constants = constants;
	this.canvasId = canvasId;
	this.moveHelper = moveHelper;
	
	this.cellWidth = boardWidth / 8;	
	this.flipAnimationFramesBlackToWhite = this.createTokenFlipAnimationFramesBlackToWhite();
	this.flipAnimationFramesWhiteToBlack = this.createTokenFlipAnimationFramesWhiteToBlack();
	this.boardCellHighlightFrame = this.createBoardCellHighlightFrame();
	this.boardCellTokenPlaceHolderFrame = this.createBoardCellTokenPlaceHolderFrame();
};

/**
 * @public
 */
LS_REVERSI.EntityFactory.prototype.createSplashScreen = function(splashScreenImage) {
	var drawProperties = new LS_GAME_LOOP.ImageDrawProperties(this.boardWidth, this.boardWidth, 0, 0, 0, 0, splashScreenImage.width, splashScreenImage.height);
	var splashScreenFrame =  new LS_GAME_LOOP.AnimationFrame(splashScreenImage, drawProperties);
	return new LS_REVERSI.SplashScreen(this.boardWidth, splashScreenFrame, this.frameRenderer);
};

/**
 * @public
 */
LS_REVERSI.EntityFactory.prototype.createGameOverScreen = function() {
	return new LS_REVERSI.GameOverScreen(this.boardWidth, this.constants);
};

/**
 * @public
 */
LS_REVERSI.EntityFactory.prototype.createDemoOverScreen = function() {
	return new LS_REVERSI.DemoOverScreen(this.boardWidth, this.constants);
};

/**
 * @public
 */
LS_REVERSI.EntityFactory.prototype.createBoard = function() {
	var board = new LS_REVERSI.Board(this.boardWidth,
			new LS_GAME_LOOP.AnimationFrame(this.theme.boardImage,
					new LS_GAME_LOOP.ImageDrawProperties(this.boardWidth, this.boardWidth, 0, 0, 0, 0, this.theme.boardImage.width, this.theme.boardImage.height)), 
			this.frameRenderer);
	return board;
};

/**
 * @public
 */
LS_REVERSI.EntityFactory.prototype.createBoardCell = function(cellX, cellY) {
	var boardCell = new LS_REVERSI.BoardCell(cellX, cellY, this.cellWidth, 
			this.boardCellHighlightFrame, 
			this.boardCellTokenPlaceHolderFrame,
			this.frameRenderer);
	return boardCell;
};

/**
 * @public
 */
LS_REVERSI.EntityFactory.prototype.createToken = function(cellX, cellY) {
	var token = new LS_REVERSI.Token(cellX, cellY, this.cellWidth, 
			new LS_GAME_LOOP.Animation(this.flipAnimationFramesBlackToWhite, 1), 
			new LS_GAME_LOOP.Animation(this.flipAnimationFramesWhiteToBlack, 1),
			this.frameRenderer,
			this.constants);
	return token;
};

/**
 * @public
 */
LS_REVERSI.EntityFactory.prototype.createTokensByPosition = function() {
	var tokensByPosition = new Array();
	for(var i = 0; i < this.constants.boardSize; i++) {
		tokensByPosition[i] = new Array();
	}
	for(var x = 0; x < this.constants.boardSize; x++) {
		for(var y = 0; y < this.constants.boardSize; y++) {
			var token = this.createToken(x, y);
			tokensByPosition[x][y] = token;
		}
	}
	return tokensByPosition;
};

/**
 * @public
 */
LS_REVERSI.EntityFactory.prototype.createBoardCellsByPosition = function() {
	var boardCellsByPosition = new Array();
	for(var i = 0; i < this.constants.boardSize; i++) {
		boardCellsByPosition[i] = new Array();
	}
	for(var x = 0; x < this.constants.boardSize; x++) {
		for(var y = 0; y < this.constants.boardSize; y++) {
			var token = this.createBoardCell(x, y);
			boardCellsByPosition[x][y] = token;
		}
	}
	return boardCellsByPosition;
};

/**
 * @public
 */
LS_REVERSI.EntityFactory.prototype.createGameFiniteStateMachine = function(tokensByPosition, boardCellsByPosition, cursor, gameMenuInputEventQueue, 
		aiConfigurationToReversiGameEvaluatorConverter, aiConfigurations, debug, canvasSize, splashScreen, gameOverScreen, demoOverScreen) {
	
	if(aiConfigurations.length == 0) {
		alert("No A.I. configurations found");
		throw "No A.I. configurations found";
	}
	
	var gameEvaluator = aiConfigurationToReversiGameEvaluatorConverter.convert(2, aiConfigurations[0]);
	var gameState = new LS_REVERSI.ReversiGameState(1, this.constants, this.moveHelper);
	var minimax = new LS_AI.Minimax(gameEvaluator, gameState, new LS_AI.CallStack(), debug === undefined ? false : debug);
	return new LS_REVERSI.GameFiniteStateMachine(tokensByPosition, boardCellsByPosition, cursor, gameState, minimax, this.constants, gameMenuInputEventQueue, this.moveHelper, 
			aiConfigurationToReversiGameEvaluatorConverter, aiConfigurations, canvasSize, splashScreen, gameOverScreen, demoOverScreen);
};

/**
 * @public
 */
LS_REVERSI.EntityFactory.prototype.createGameFiniteStateMachineForIntermediateGameState = function(tokensByPosition, boardCellsByPosition, cursor, gameMenuInputEventQueue, 
		aiConfigurationToReversiGameEvaluatorConverter, aiConfigurations, debug, canvasSize, splashScreen, gameOverScreen, demoOverScreen, reversiSimpleGameState) {
	
	if(aiConfigurations.length == 0) {
		alert("No A.I. configurations found");
		throw "No A.I. configurations found";
	}
	
	var gameEvaluator = aiConfigurationToReversiGameEvaluatorConverter.convert(2, aiConfigurations[0]);
	var gameState = reversiSimpleGameState.toReversiGameState(this.constants, this.moveHelper);
	var minimax = new LS_AI.Minimax(gameEvaluator, gameState, new LS_AI.CallStack(), debug === undefined ? false : debug);
	return new LS_REVERSI.GameFiniteStateMachine(tokensByPosition, boardCellsByPosition, cursor, gameState, minimax, this.constants, gameMenuInputEventQueue, this.moveHelper, 
			aiConfigurationToReversiGameEvaluatorConverter, aiConfigurations, canvasSize, splashScreen, gameOverScreen, demoOverScreen);
};

/**
 * @public
 */
LS_REVERSI.EntityFactory.prototype.createCursor = function(hardwareCursor) {
	if(hardwareCursor) {
		var pointerCursorStyle = "cursor: url('" + this.theme.cursorActiveImage.src + "'), pointer;";
		var waitCursorStyle = "cursor: url('" + this.theme.cursorWaitImage.src + "'), wait;";
		var stylesById = new Array();
		stylesById["active"] = pointerCursorStyle;
		stylesById["wait"] = waitCursorStyle;				
		return new LS_GAME_LOOP.HardwareCursorEntity(stylesById, "pointer", this.canvasId);
	} else {
		var activeImage = this.theme.cursorActiveImage;
		var waitImage = this.theme.cursorWaitImage;
		var framesById = new Array();
		var activeFrame = new LS_GAME_LOOP.AnimationFrame(activeImage, new LS_GAME_LOOP.ImageDrawProperties(activeImage.width, activeImage.height));
		var waitFrame = new LS_GAME_LOOP.AnimationFrame(waitImage, new LS_GAME_LOOP.ImageDrawProperties(waitImage.width, waitImage.height));
		framesById["active"] = activeFrame;
		framesById["wait"] = waitFrame;		
		var cursor = new LS_GAME_LOOP.CursorEntity(framesById, "active", this.frameRenderer, this.canvasId);
		cursor.x = -50;
		cursor.y = -50;
		return cursor;
	}
};

/**
 * @private
 * @returns {LS_GAME_LOOP.AnimationFrame[]}
 */
LS_REVERSI.EntityFactory.prototype.createTokenFlipAnimationFramesBlackToWhite = function() {
	var tokenBlackImage = this.theme.tokenBlackImage;
	var tokenWhiteImage = this.theme.tokenWhiteImage;
	
	var animationFrames = new Array();
	
	var framesCountFirstHalf = this.flipAnimationFramesCount / 2;
	var widthDelta = this.cellWidth/framesCountFirstHalf;
	var idx = 0;
	for(var i = 0; i < framesCountFirstHalf; i++) {
		var newWidth = this.cellWidth - i * widthDelta;
		var deltaX = (this.cellWidth - newWidth) / 2;
		var drawProperties = new LS_GAME_LOOP.ImageDrawProperties(newWidth, this.cellWidth, deltaX, 0, 0, 0, tokenBlackImage.width, tokenBlackImage.height);
		var animationFrame = new LS_GAME_LOOP.AnimationFrame(tokenBlackImage, drawProperties);
		animationFrames[idx] = animationFrame;
		idx++;
	}	
	
	for(var i = 0; i < this.flipAnimationFramesCount - framesCountFirstHalf; i++) {
		var newWidth = this.cellWidth - (this.flipAnimationFramesCount - framesCountFirstHalf - i - 1) * widthDelta;
		var deltaX = (this.cellWidth - newWidth) / 2;
		var drawProperties = new LS_GAME_LOOP.ImageDrawProperties(newWidth, this.cellWidth, deltaX, 0, 0, 0, tokenWhiteImage.width, tokenWhiteImage.height);
		var animationFrame = new LS_GAME_LOOP.AnimationFrame(tokenWhiteImage, drawProperties);
		animationFrames[idx] = animationFrame;
		idx++;
	}
	return animationFrames;
};

/**
 * @private
 * @returns {LS_GAME_LOOP.AnimationFrame[]}
 */
LS_REVERSI.EntityFactory.prototype.createTokenFlipAnimationFramesWhiteToBlack = function() {
	var frames = new Array();
	for(var i = 0; i < this.flipAnimationFramesBlackToWhite.length; i++) {
		frames[i] = this.flipAnimationFramesBlackToWhite[this.flipAnimationFramesBlackToWhite.length - 1 - i];
	}
	return frames;
};

/**
 * @private
 */
LS_REVERSI.EntityFactory.prototype.createBoardCellHighlightFrame = function() {
	var image = this.theme.boardCellHighlightImage;
	var drawProperties = new LS_GAME_LOOP.ImageDrawProperties(this.cellWidth, this.cellWidth, 0, 0, 0, 0, image.width, image.height);
	return new LS_GAME_LOOP.AnimationFrame(image, drawProperties);
};

/**
 * @private
 */
LS_REVERSI.EntityFactory.prototype.createBoardCellTokenPlaceHolderFrame = function() {
	var image = this.theme.tokenPlaceholderImage;
	var drawProperties = new LS_GAME_LOOP.ImageDrawProperties(this.cellWidth, this.cellWidth, 0, 0, 0, 0, image.width, image.height);
	return new LS_GAME_LOOP.AnimationFrame(image, drawProperties);
};