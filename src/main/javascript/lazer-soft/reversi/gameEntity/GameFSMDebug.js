var LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.GameFSMDebug}
 */
LS_REVERSI.GameFSMDebug = function(gameFiniteStateMachine) {
	LS_GAME_LOOP.Entity.call(this, 
			10, 10, 110, 100, 1000);
	
	this.gameFSM = gameFiniteStateMachine;
};

LS_REVERSI.GameFSMDebug.prototype = Object.create(LS_GAME_LOOP.Entity.prototype);
LS_REVERSI.GameFSMDebug.prototype.constructor = LS_REVERSI.GameFSMDebug;


/**
 * @public
 */
LS_REVERSI.GameFSMDebug.prototype.processInput = function(inputEvent) {};

/**
 * @public
 */
LS_REVERSI.GameFSMDebug.prototype.updateState = function() {};

/**
 * @public
 * @param {CanvasRenderingContext2D} context2d
 */
LS_REVERSI.GameFSMDebug.prototype.updateGraphics = function(context2d) {
	var y = this.y + 12;
	context2d.font="10px Arial";
	for(var i = 0; i < this.gameFSM.stateHistory.length - 1; i++) {
		var state = this.gameFSM.stateHistory[i];
		if(state === "waitForHumanToMove") {
			context2d.fillStyle = "yellow";
		} else if(state === "waitForDraw" && i > 0 && this.gameFSM.stateHistory[i - 1] === "getAIMove") {
			context2d.fillStyle = "white";
			context2d.fillRect(this.x - 1, y - 10, 160, 12)
			if(this.gameFSM.lastGetAIMoveDurationMs < 500) {
				context2d.fillStyle = "green";
			} else if(this.gameFSM.lastGetAIMoveDurationMs < 1500) {
				context2d.fillStyle = "orange";
			} else {
				context2d.fillStyle = "red";
			}
			state = state + " (A.I. took " + this.gameFSM.lastGetAIMoveDurationMs + " ms.)";
		} else {
			context2d.fillStyle = "blue";
		}
		context2d.fillText(state, this.x, y);
		y += 12;
	}
	var state = this.gameFSM.stateHistory[this.gameFSM.stateHistory.length - 1];
	if(state === "waitForHumanToMove") {
		context2d.fillStyle = "yellow";
	} else {
		context2d.fillStyle = "blue";
	}
	context2d.fillText(" > " + state, this.x, y);
};
