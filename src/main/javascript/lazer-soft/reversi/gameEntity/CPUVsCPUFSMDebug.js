var LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.CPUVsCPUFSMDebug}
 */
LS_REVERSI.CPUVsCPUFSMDebug = function(cpuVsCpuFiniteStateMachine, startingText, constants) {
	LS_GAME_LOOP.Entity.call(this, 
			10, 10, 110, 100, Number.POSITIVE_INFINITY);
	
	this.fontSize = 16;
	
	this.fsm = cpuVsCpuFiniteStateMachine;
	this.startingText = startingText;
	this.constants = constants;
};

LS_REVERSI.CPUVsCPUFSMDebug.prototype = Object.create(LS_GAME_LOOP.Entity.prototype);
LS_REVERSI.CPUVsCPUFSMDebug.prototype.constructor = LS_REVERSI.CPUVsCPUFSMDebug;


/**
 * @public
 */
LS_REVERSI.CPUVsCPUFSMDebug.prototype.processInput = function(inputEvent) {};

/**
 * @public
 */
LS_REVERSI.CPUVsCPUFSMDebug.prototype.updateState = function() {};

/**
 * @public
 * @param {CanvasRenderingContext2D} context2d
 */
LS_REVERSI.CPUVsCPUFSMDebug.prototype.updateGraphics = function(context2d) {
	var y = this.y + this.fontSize + 2;
	context2d.fillStyle = "yellow";
	context2d.font = this.fontSize + "px Arial";
	context2d.fillText(this.startingText, this.x, y);
	var gameState = this.fsm.gameStateHistory.getCurrent();
	if(gameState !== undefined && gameState.isGameEnded()) {
		context2d.fillText(this.getGameOutcomeMessage(), this.x, y + this.fontSize + 2);
	}
};

/**
 * @private
 */
LS_REVERSI.CPUVsCPUFSMDebug.prototype.getGameOutcomeMessage = function() {
	var whiteCount = 0;
	var blackCount = 0;
	var gameState = this.fsm.gameStateHistory.getCurrent();
	for(var x = 0; x < this.constants.boardSize; x++) {
		for(var y = 0; y < this.constants.boardSize; y++) {
			var tokenType = gameState.getTokenTypeAt(x, y);
			if(tokenType === this.constants.tokenTypeWhite) {
				whiteCount++;
			} else if(tokenType === this.constants.tokenTypeBlack) {
				blackCount++;
			}
		}
	}
	if(whiteCount > blackCount) {
		return "White has won " + whiteCount + "/" + blackCount;
	} else if(blackCount > whiteCount) {
		return "Black has won " + blackCount + "/" + whiteCount;
	} else if(blackCount > whiteCount) {
		return "Draw " + blackCount + " = " + whiteCount;
	}
	return "???";
};
