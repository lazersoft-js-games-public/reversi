var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.Board}
 */
LS_REVERSI.Board = function(width, backgroundFrame, frameRenderer) {
	LS_GAME_LOOP.Entity.call(this, 0, 0, width, width, 10);
	this.backgroundFrame = backgroundFrame;
	this.frameRenderer = frameRenderer;
};

LS_REVERSI.Board.prototype = Object.create(LS_GAME_LOOP.Entity.prototype);
LS_REVERSI.Board.prototype.constructor = LS_REVERSI.Board;

/**
 * @public
 */
LS_REVERSI.Board.prototype.processInput = function(inputEvent) {};

/**
 * @public
 */
LS_REVERSI.Board.prototype.updateState = function() {};

/**
 * @public
 * @param {CanvasRenderingContext2D} context2d
 */
LS_REVERSI.Board.prototype.updateGraphics = function(context2d) {
	this.frameRenderer.draw(context2d, 0, 0, this.backgroundFrame);
};