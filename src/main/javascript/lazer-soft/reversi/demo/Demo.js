var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * set to "true" to run game in Demo Mode. 
 * This is automatically set to true when using LS_REVERSI.launchGameDemo(args) or LS_REVERSI.launchAIEditorDemo(args)
 */
LS_REVERSI.DM = false;