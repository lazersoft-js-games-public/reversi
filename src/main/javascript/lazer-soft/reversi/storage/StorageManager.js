var LS_REVERSI = LS_REVERSI = LS_REVERSI || {};

/**
 * @public
 * @class
 * @returns {LS_REVERSI.StorageManager}
 */
LS_REVERSI.StorageManager = function(keyPrefix) {
	if(typeof(Storage) === "undefined") {
		throw "Local storage not supported";
	}
	this.prefix = keyPrefix;
};

/**
 * @public
 */
LS_REVERSI.StorageManager.prototype.store = function(key, value) {
	localStorage.setItem(this.prefix + key, value);
};

/**
 * @public
 */
LS_REVERSI.StorageManager.prototype.get = function(key) {
	return localStorage[this.prefix + key];
};

/**
 * @public
 */
LS_REVERSI.StorageManager.prototype.remove = function(key) {
	localStorage.removeItem(this.prefix + key);
};

/**
 * @public
 */
LS_REVERSI.StorageManager.prototype.listKeys = function() {
	var keys = new Array();
	for(var i = 0; i < localStorage.length; i++) {
		var prefixedKey = localStorage.key(i);
		keys[i] = prefixedKey.substring(this.prefix.length);
	}
	return keys;
};