# Reversi

This is a **vanilla JavaScript implementation** of the game of **Reversi**. It's a single player game, allowing you to play agains A.I. oponents of varying difficulty.

The implementation contains a Reversi A.I. implemented with **Minimax with Alpha-Beta pruning**. There's also an implementation of a **fixed-update game loop**, for handling animations.

If you want to further explore how the game's A.I. works, there's also an **A.I. editor/debugger**, that allows to see how 2 A.I. instances behave when playing one agains the other, and where you can also change the A.I. configuration and see the modifications live in action.

## Try it out 
You can play here:

https://lazersoft.neocities.org/reversi/Game.html

Or play around with the A.I. configuration here:

https://lazersoft.neocities.org/reversi/AI-editor.html

## Building and running the project
The project in managed using [Maven](https://maven.apache.org/)

To quickly start the project locally (without running any tests):

    mvn  clean  yuicompressor:compress  assembly:directory  -DskipTests  tomcat7:run
This will make the game available at:
http://localhost:8765/ls-reversi/Game-embed-example.html
Or, with an alternate theme, at:
http://localhost:8765/ls-reversi/Game-embed-example-alternate-look.html
The A.I. editor will be available at:
http://localhost:8765/ls-reversi/AI-editor-embed-example.html

To run the Jasmine tests:

    mvn clean test
    
To create the project assembly:

    mvn  clean  yuicompressor:compress  assembly:assembly
This will create the file target/reversi-1.0-game
To try out the game locally, simply unzip, and load any of the html files in your browser (Chrome strongly recommended) to see the game, the game with an alternate look (theme), or the A.I. editor.
